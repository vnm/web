/**
 * Created by Alexey on 01.09.2015.
 */
Router.configure({
    layoutTemplate: 'layout',
    //loadingTemplate: 'loading',
    //notFoundTemplate: 'notFound',
    //waitOn: function() {
    //    return [Meteor.subscribe('notifications')]
    //}
});

Router.onBeforeAction(function() {
    //closeMap();
    if (Meteor.isClient && !Meteor.userId()) {
        this.render('login');
    } else {
        this.next();
    }
});

Router.route('login', {
    path: '/login',
});

Router.route('homeRedirect',{
    path: '/',
    action: function(){
        this.redirect('mainTableView', {lang: 'ru', page: '0'})
    }
});

Router.route('statisticRedirect',{
    path: '/statistics',
    action: function(){
        this.redirect('statistics', {lang: 'ru', page: '0'})
    }
});

Router.route('logout', {
    path: '/logout',
    action: function(){
        this.render('login');
        var redirect = this.redirect;

        Meteor.logout(function(err){
        if(err)
            console.log('Logout failed, reason: ' + err);
        else {
            console.log('Logout ok');
            Router.go('login');
        }
    });
}
});

Router.route('controlPanel',{
    path: '/controlPanel',
    subscriptions: function(){
       return [Meteor.subscribe('clients'), Meteor.subscribe('renters'), Meteor.subscribe('futureTasks'), Meteor.subscribe('userdata')];
    },

    action: function(){
        if (this.ready()) {
            this.render();
        } else {
            this.render('loading');
        }
    }
});

Router.route('statistics',{
    path: '/statistics/:lang/:page',
    subscriptions: function(){
        //var page = Router.current().params.page;
        //var skip = page*50;

        var searchQuery = {};

        var from = new Date().getTime()-1000*3600;
        var to = new Date().getTime();

        var statSearchQuery = Session.get('statSearchQuery');

        if (!statSearchQuery){
            var ssq = {};
            ssq.from = from;
            ssq.to = to;
            Session.set('statSearchQuery', ssq);
        }

        statSearchQuery = Session.get('statSearchQuery');

        if (statSearchQuery){
            var selectedTerminal = statSearchQuery.selectedTerminal;
            if (selectedTerminal){
                if(selectedTerminal !== 'all')
                    searchQuery.client = selectedTerminal;
            }

            var  fromDate = statSearchQuery.from;

            if (fromDate){
                if (searchQuery.Timestamp === undefined)
                    searchQuery.Timestamp = {};

                searchQuery.Timestamp.$gte = fromDate;
            }

            var  toDate = statSearchQuery.to;
            if (toDate){
                if (searchQuery.Timestamp === undefined)
                    searchQuery.Timestamp = {};

                searchQuery.Timestamp.$lt = toDate;
            }
        }

        //var statistic = Statistic.find(searchQuery);
        //var pagesNum = statistic.count();
        //
        //Session.set('pagesNum', pagesNum);

        return [
            Meteor.subscribe('clients'),
            Meteor.subscribe('renters'),
            Meteor.subscribe('statistic', searchQuery)
        ];
    },

    action: function(){
        if (this.ready()) {
            this.render();
        } else {
            this.render('loading');
        }
    }
});

Router.route('adminLists', {
    path: '/adminlists',
    subscriptions: function(){
        return [Meteor.subscribe('types'), Meteor.subscribe('categories'), Meteor.subscribe('divisions')];
    },

    data: function(){
        var categories = Categories.find();
        var types = Types.find();
        var divisions = Divisions.find();
        var d = {};
        d.categories = categories;
        d.types = types;
        d.divisions = divisions;
        return d;
    },
    action: function(){
        if (this.ready()) {
            this.render();
        } else {
            this.render('loading');
        }
    }
})

Router.route('mainTableView',{
    path: '/main/:lang/:page',
    subscriptions: function(){
        return [Meteor.subscribe('types'), Meteor.subscribe('categories'), Meteor.subscribe('renters'), Meteor.subscribe('divisions')];

    },
    data: function(){
        var renters = Renters.find();
        var d = {};
        d.renters = renters;
        return d;
    },
    action: function () {
        if (this.ready()) {
            this.render();
        } else {
            this.render('loading');
        }
    }
});

Router.route('serverFile', {
    where: 'server',
    path: '/file/:imageid',
    action: function() {
        var fs = Npm.require('fs');
        var filePath = Files.findOne({_id: this.params.imageid}).path;
        var data = fs.readFileSync(filePath);
        this.response.writeHead(200, {
            'Content-Type': 'image'
        });
        this.response.write(data);
        this.response.end();
    }
});
