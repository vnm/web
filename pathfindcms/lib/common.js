/**
 * Created by Alexey on 01.09.2015.
 */

//text render width

width = 430;
ClientsPoolDelay = 1000*60*1;

defaultRenter = {
    isActive: false,
    isLogoActive: false,
    keywords: "",
    logo: {},
    includeInSearch: true,
    en:{
        title: "Renter title",
        description: {
            text: "Renter description",
            html: "Renter description"
        }
    },
    ru:{
        title: "Название арендатора",
        description: {
            text: "Описание арендатора",
            html: "Описание арендатора"
        }
    }
}

jobList = [
    {
        job: "refreshAll",
        name: "Обновить все"
    }
]

jobRepeat = [
    {
        repeat: "notRepeat",
        name: "не повторять"
    },
    {
        repeat: "everyDay",
        name: "каждый день"
    },
    {
        repeat: "everyWeek",
        name: "каждую неделю"
    },
    {
        repeat: "everyMonth",
        name: "каждый месяц"
    }


]

sortValues = [
    {
        name: "floor",
        title: "По этажам"
    },
    {
        name: "alphabet",
        title: "По алфавиту"
    },
    {
        name: "category",
        title: "По категориям"
    },
    {
        name: "type",
        title: "По типу"
    }
]

