/**
 * Created by Alexey on 01.09.2015.
 */

 //https://atmospherejs.com/matb33/collection-hooks
Renters = new Meteor.Collection('renters');

Renters.before.insert(function(userId, doc){
    doc.modifiedAt = Date.now();
});

Renters.before.update(function (userId, doc, fieldNames, modifier, options) {
  modifier.$set = modifier.$set || {};
  modifier.$set.modifiedAt = Date.now();
});

Meteor.methods({
    clearRenter: function(renterId){

        //TODO: Delte logotype file and text-render;

        var renter = defaultRenter;
        //var cellNumber = Renters.findOne(renterId).cellNumber;
        var category = Categories.findOne({default: true})._id;
        var type = Types.findOne({default: true})._id;
        var division = Divisions.findOne({default: true})._id;

        //renter.cellNumber = cellNumber;
        renter.category = category;
        renter.type = type;
        renter.division = division;

        Renters.update(renterId, {$set: renter});
        //Console.log(Renters.findOne(renterId));
    }
})