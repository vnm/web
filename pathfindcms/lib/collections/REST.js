/**
 * Created by Alexey on 09.09.2015.
 */
if(Meteor.isServer){
    Meteor.startup(function(){
        collectionApi = new CollectionAPI({apiPath: 'api'});
        collectionApi.addCollection(Renters, 'renters');
        collectionApi.addCollection(Categories, 'categories');
        collectionApi.addCollection(Types, 'types');
        collectionApi.addCollection(Divisions, 'divisions');
        collectionApi.start();
    });
}