/**
 * Created by Alexey on 01.09.2015.
 */
var defaultCategory;
var defaultType;
var defaultDivision;

if(Meteor.users.find().count() === 0){

}

if (Clients.find().count() === 0){
    Clients.insert({
        ip: "192.168.0.101"
    });
    Clients.insert({
        ip: "192.168.0.222"
    });
    Clients.insert({
        ip: "192.168.0.3"
    });
}

if(Divisions.find().count() === 0){
    defaultDivision = Divisions.insert({
        default: true,
        title: "Магазины"
    });

    Divisions.insert({
        title: "Киоски"
    });

    Divisions.insert({
        title: "Автоматы"
    });

    Divisions.insert({
        title: "Эскалаторы и лифты"
    });

    Divisions.insert({
        title: "Прочее"
    });

}
if(Categories.find().count() === 0){
    defaultCategory = Categories.insert({
        default: true,
        ru:{
            title: "Прочие товары"
        },
        en:{
            title: "Other goods"
        }
    });

    Categories.insert({
        ru:{
            title: "Аксессуары, Ювелирные изделия и подарки"
        },
        en:{
            title: "Accessories, Jewellery &gifts"
        }
    })
}
if(Types.find().count() === 0){
    defaultType = Types.insert({
        default: true,
        title: "Секция",
        name: "section"
    });

    Types.insert({
        title: "Киоск",
        name: "kiosk"
    });

}
if(Renters.find().count() < 600){
    for (var i=Renters.find().count(); i<600; i++){
        var renter = defaultRenter;
        renter.cellNumber = i.toString();
        renter.category = defaultCategory;
        renter.type = defaultType;
        renter.division = defaultDivision;
        Renters.insert(renter);
    }
}