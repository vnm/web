/**
 * Created by Alexey on 02.09.2015.
 */
Meteor.publish('types', function(){
    return Types.find();
});

Meteor.publish('categories', function(){
    return Categories.find();
});

Meteor.publish('divisions', function(){
    return Divisions.find();
});

Meteor.publish('files', function(){
    return Files.find();
});

Meteor.publish('renters', function(){
    return Renters.find();
});

Meteor.publish('clients', function(){
    return Clients.find();
});

Meteor.publish('futureTasks', function(){
    return FutureTasks.find();
});

Meteor.publish('statistic', function(searchQuery){
	return Statistic.find(searchQuery, {limit:1000});
})

Meteor.publish('userdata', function() {
    if (!this.userId) return null;
    var group = Meteor.users.findOne({_id: this.userId}).profile.group;
    if (group!== "admins")
        return this.ready();
    return Meteor.users.find({}, {fields: {'profile':1, 'username':1}});
});