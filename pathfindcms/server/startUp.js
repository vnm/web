if (Meteor.isServer) {
  
  Meteor.startup(function () {
	  rootpath = '../../../../../.public/';
	  console.log(' p e r ' + process.env.rp);
	  console.log('rp after start: ' + rootpath);
	PoolClients();

	FutureTasks.find().forEach(function(taskDetail) {
		addSheduleJob(taskDetail._id, taskDetail);
	});

	SyncedCron.start();

  });

}

function PoolClients(){
	var ping = Meteor.npmRequire('ping');
	var Fiber = Npm.require('fibers');
	var lock = false;

	Meteor.setInterval(function(){

		if (lock)
			return;

		lock = true;
		var clients = Clients.find().fetch();
		var clientsCount = clients.length;
		
		var total = 0;
		var message = "";

		 function checkAll()
		 {
		 	total++;
		 	if (total === clientsCount){
				lock = false;
		 		if (message.length > 0)
		 			Meteor.call('sendEmail', message);
		 	}
		 }
		
		clients.forEach(function(client){

			var id = client._id;
			var host = client.ip;
			var lastStatus = Clients.findOne(id).status;

			HTTP.get("http://"+host+":82/log", {}, function(e,r){
				if (e){
					ping.sys.probe(host, function(isAlive){
				        if (!isAlive)
				        {
				        	Fiber(function () {
                   				Clients.update(id, {$set: {status: 'offline'}})
                   				if (lastStatus === 'online' || lastStatus === 'softwareisdown'){
                   					message = message + "terminal " + host + " is offline\n";
                   				}
                   				checkAll();
                			}).run();
				        	
				        }
				        else
				        {
				        	Fiber(function () {
				        		Clients.update(id, {$set: {status: 'softwareisdown'}})
				        		if (lastStatus === 'online'){
                   					message = message + "terminal " + host + " software is down\n";
                   				}
				        		checkAll();
				        	}).run();
				        }
				    });
				}

				if(!e)
				{
					try {
						HTTP.get("http://" + host + ":8081/state", {}, function (e, r) {
							if (!e) {
								Clients.update(id, {$set: {status: 'online', updatedAt: r.content}})
							}
						})

						var lastStatRecord = Statistic.findOne({client: host}, {sort: {Timestamp: -1}}) &&
							Statistic.findOne({client: host}, {sort: {Timestamp: -1}}).Timestamp;

						if (lastStatRecord === undefined)
							lastStatRecord = 0;

						var statArray = JSON.parse(r.content);

						if (statArray) {
							for (var i = 0; i < statArray.length; i++) {
								if (statArray[i].Timestamp > lastStatRecord) {
									statArray[i].client = host;
									Statistic.insert(statArray[i]);
								}
							}
						}

						lastStatRecord = Statistic.findOne({client: host}, {sort: {Timestamp: -1}}) &&
							Statistic.findOne({client: host}, {sort: {Timestamp: -1}}).Timestamp;

						if (lastStatRecord === undefined)
							lastStatRecord = 0;

						HTTP.post("http://" + host + ":8080/log", {params: {'timestamp': lastStatRecord}});
					}
					catch (err){
						console.log(err);
					}

					checkAll();
				}
			})
		})
		
	}, ClientsPoolDelay);
}