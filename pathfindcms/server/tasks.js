//http://richsilv.github.io/meteor/scheduling-events-in-the-future-with-meteor/
//https://github.com/percolatestudio/meteor-synced-cron
//https://atmospherejs.com/orphan/synced-cron

function refreshOne(details){
	console.log('refresh' + details.ip);
}

function refreshAll(){
	console.log('refresh all');
	Meteor.call('refreshAllClients');
}

addSheduleJob = function (id, details){

	var start = moment(details.taskStart);
	if (details.jobName === 'refreshAll')
	{
		SyncedCron.add({
			name: id,
			schedule: function(parser){

				console.log('taskRepeat: ' + details.taskRepeat);

				if (details.taskRepeat === "notRepeat")
					return parser.recur().on(start.toDate()).fullDate();
				
				if (details.taskRepeat === "everyDay")
					return parser.recur().on(start.format("HH:mm:ss")).time().after(start.toDate()).fullDate();

				if (details.taskRepeat === "everyWeek"){
					var dow = parseInt(start.format("e"))+1;
					if (dow > 7)
						dow = 0;
					return parser.recur().on(dow).dayOfWeek().on(start.format("HH:mm:ss")).time().after(start.toDate()).fullDate();
				}

				if (details.taskRepeat === "everyMonth"){
					var dom = parseInt(start.format("D"));
					return parser.recur().on(dom).dayOfMonth().on(start.format("HH:mm:ss")).time().after(start.toDate()).fullDate();
				}
			},
			job: function(){
				refreshAll(details);
			}
		})
	}
	
}