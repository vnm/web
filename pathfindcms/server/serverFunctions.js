/**
 * Created by Alexey on 04.09.2015.
 */



isItImage = function(name){
    var a = name.split(".");
    if (a[a.length-1] === "jpg")
        return true;
    if (a[a.length-1] === "jpeg")
        return true;
    if (a[a.length-1] === "png")
        return true;
    if (a[a.length-1] === "bmp")
        return true;
    return false;
}

cleanPath = function (str) {
    if (str) {
        return str.replace(/\.\./g,'').replace(/\/+/g,'').
            replace(/^\/+/,'').replace(/\/+$/,'');
    }
}

cleanName = function (str) {
    return str.replace(/\.\./g,'').replace(/\//g,'');
}

checkAndCreatePaths = function (rootpath, relpath){

    var fs = Npm.require('fs');
    var a = relpath.split("/");

    console.log('rootpath: ' + rootpath);

    if (!fs.existsSync(rootpath)){
        fs.mkdirSync(rootpath);
        console.log("create path: " + rootpath);
    }

    for (var i=0; i< a.length; i++)
    {
        var p = (a.slice(0, i+1)).join("/");

        if (!fs.existsSync(rootpath + p))
        {
            //console.log("create path: " + rootpath+p);
            fs.mkdirSync(rootpath+p);
        }
    }

    return rootpath + relpath;
}

PhotoFromVideoName = function(name){
    var a = name.split(".");
    a[a.length-1] = "jpg";

    return a.join(".");
}

PhotoFromSoundName = function(name){
    var a = name.split(".");
    a[a.length-1] = "jpg";

    return a.join(".");
}