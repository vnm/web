/**
 * Created by Alexey on 04.09.2015.
 */

 var md5 = Meteor.npmRequire('md5');  
 var fs = Npm.require('fs');
 var Fiber = Npm.require('fibers');

Meteor.methods({
    saveURL: function (url, id, contentName, lang) {

        //var fs = Npm.require('fs');
        //console.log('url: ' + url);
        //console.log('id: ' + id);
        //console.log('contentName: ' + contentName);
        //console.log('lang: ' + lang);
        

        var base64Data = url.replace(/^data:image\/png;base64,/, "");

        var path = checkAndCreatePaths(rootpath, id + "/" + lang + "/" + contentName + "/text/");
        var filepath = path + "text.png";
        var relpath = id + "/" + lang + "/" + contentName + "/text.png";

        //console.log('filepath: ' + filepath);

        fs.writeFileSync(filepath, base64Data, 'base64');

        var file = Files.insert({path: filepath});
        
        fs.readFile(filepath, function(err, buf) {
            Fiber(function () {
                var hash = md5(buf);
                console.log('hash: '+hash);
                var o = {};
                
                o[lang+".description.path"] = relpath;
                o[lang+".description.link"] = file;
                o[lang+".description.md5"] = hash;

                Renters.update(id, {$set: o}, function(err){
                    if(err){
                        console.log('Ошибка изменения description.path: ' + err);
                    }
                    else{
                        console.log('description.path изменен path: ' + relpath);
                        }
                    });
                }).run();
            });
    },

    saveLargeFile: function (blob, encoding, name, id, contentName, first, last, lang) {

        //var Fiber = Npm.require('fibers');
        name = cleanName(name || 'file');
        encoding = encoding || 'binary';

        var path = checkAndCreatePaths(rootpath, id + "/" + lang + "/" + contentName + "/");
        var filepath = path + name;
        var relpath = id + "/" + lang + "/" + contentName + "/" + name;

        if (first) {
            if (fs.existsSync(path + name)) {
                fs.unlinkSync(path + name);
            }
        }

        fs.appendFileSync(path + name, blob, encoding);

        if (last) {
            console.log('File has been written: ' + path + name);
            var file = Files.insert({path: filepath});

            fs.readFile(path + name, function(err, buf) {
                Fiber(function(){
                    var hash = md5(buf);
                    if (contentName === 'logo') {

                        var addr = lang + ".logo";
                        var date = new Date();

                        var o = {};
                        o[lang + ".logo.fileId"] = file;
                        o[lang + ".logo.filePath"] = relpath;
                        o[lang + ".logo.lastUpdate"] = date;
                        o[lang + ".logo.md5"] = hash;

                        Renters.update(id, {$set: o});
                    }
            
                }).run();
            });  
        }
    },

    clearClients: function(){
         Clients.remove({});
    },

    refreshClient: function(id){
        var ip = Clients.findOne(id).ip;
        d = {'command': 'refresh'};
        HTTP.post("http://"+ip+":8080/api/managment", {params: d}, function(e,r){
            console.log('refresh client : '+ ip);
        })
    },

    rebootClient: function(id){
        var ip = Clients.findOne(id).ip;
        d = {'command': 'reboot'};
        HTTP.post("http://"+ip+":8080/api/managment", {params: d}, function(e,r){
            console.log('reboot client fail: '+ ip);
        })
    },

    turnoffClient: function(id){
        var ip = Clients.findOne(id).ip;
        d = {'command': 'shutdown'};
        HTTP.post("http://"+ip+":8080/api/managment", {params: d}, function(e,r){
            console.log('turnoff client : ' + ip);
        })
    },

    refreshAllClients: function(){
        var c = Clients.find();
        c.forEach(function(d){
            console.log(d.ip);
            Meteor.call('refreshClient', d._id);
        })
    },

    rebootAllClients: function(){
        var c = Clients.find();
        c.forEach(function(d){
            console.log(d.ip);
            Meteor.call('rebootClient', d._id);
        })
    },

    turnoffAllClients: function(){
        var c = Clients.find();
        c.forEach(function(d){
            console.log(d.ip);
            Meteor.call('turnoffClient', d._id);
        })
    },

   'remoteGet' : function(url,options){
        this.unblock();
        return HTTP.get(url,options);
    },

   'sheduleTask': function(taskName, jobName, taskStart, taskRepeat) {

        var details = {};
        details.taskName = taskName;
        details.taskStart = taskStart;
        details.taskRepeat = taskRepeat;
        details.jobName = jobName;


        var id = FutureTasks.insert(details);
        addSheduleJob(id, details);
    },

    'removeTask': function(id){
        var jobName = FutureTasks.findOne(id).name;
        SyncedCron.remove(jobName);
        FutureTasks.remove(id);

    },

    'sendEmail' :function(message){
        var mail = Meteor.users.findOne({username: 'superuser'}).profile.mail;
        if (mail.length>0){
            Email.send({
                to: mail,
                from: 'svc_totem@galeria.spb.ru',
                subject: 'gallery warning',
                text: message
            });
        }
    },

    'changePwd' : function(user, pwd){
        if (pwd.length >= 6){
            if (this.userId){
                var group = Meteor.users.findOne(this.userId).profile.group;
                if (group === 'admins'){
                    var userId = Meteor.users.findOne({username: user})._id;
                    Accounts.setPassword(userId, pwd);
                }
            }
        }
    },

    'changeMail' : function(user, mail){
        if (this.userId){
            var group = Meteor.users.findOne(this.userId).profile.group;
            if (group === 'admins'){
                var userId = Meteor.users.findOne({username: user})._id;
                Meteor.users.update(userId, {$set: {'profile.mail': mail}});
            }
        }

    },

    download: function(statSearchQuery) {

        var searchQuery = {};

        if (statSearchQuery){
            var selectedTerminal = statSearchQuery.selectedTerminal;
            if (selectedTerminal){
                if(selectedTerminal !== 'all')
                    searchQuery.client = selectedTerminal;
            }

            var  fromDate = statSearchQuery.from;

            if (fromDate){
                if (searchQuery.Timestamp === undefined)
                    searchQuery.Timestamp = {};

                searchQuery.Timestamp.$gte = fromDate;
            }

            var  toDate = statSearchQuery.to;
            if (toDate){
                if (searchQuery.Timestamp === undefined)
                    searchQuery.Timestamp = {};

                searchQuery.Timestamp.$lt = toDate;
            }
        }

        var collection = Statistic.find(searchQuery, {fields: {'Timestamp':1, 'client':1, "SectionName":1, "Section": 1, "Type": 1, "Msg": 1, "Language": 1, "_id":0}}).fetch();

        for (var i=0; i<collection.length; i++){
            collection[i].Timestamp = moment(collection[i].Timestamp).format("DD-MM-YY HH:mm")
        }

        return exportcsv.exportToCSV(collection, true, ";");
    },

    clearStatDB: function(ts){
        Statistic.remove({Timestamp: {$lte: ts}});
    },

    statisticBDCount: function(){
        return Statistic.find().count();
    }
});

   