/**
 * Created by Alexey on 04.09.2015.
 */
Meteor.saveFile = function(file, name, id, contentName, Lang, progress) {

    var fileReader = new FileReader();
    var method = 'readAsBinaryString';
    var encoding = 'binary';
    var pos = 0;
    var chunk = 1024*256;

    FileChunk();

    function FileChunk(){
        progress(pos/file.size);

        if(pos<file.size){
            var LastPart = false;
            var firstPart = false;

            if (chunk >= file.size-(pos+chunk)) {
                chunk = file.size-pos;
                LastPart = true;
            }

            if (pos == 0)
                firstPart = true;

            var blob = file.slice(pos, pos+chunk);
            pos += chunk;

            fileReader.onloadend = function(e){
                 Meteor.call('saveLargeFile', e.srcElement.result, encoding, name, id, contentName, firstPart, LastPart, Lang, function(err){if(err) console.log(err); else FileChunk()});
            }

            fileReader[method](blob);

        }
    }

}