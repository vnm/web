Template.statistics.helpers({
	stat: function(){
		var page = Router.current().params.page;
		var skip = page*50;

		//var searchQuery = {};
        //
		//var statSearchQuery = Session.get('statSearchQuery');
        //
		//if (statSearchQuery){
		//	var selectedTerminal = statSearchQuery.selectedTerminal;
		//		if (selectedTerminal){
		//			if(selectedTerminal !== 'all')
		//				searchQuery.client = selectedTerminal;
		//		}
        //
		//	var  fromDate = statSearchQuery.from;
        //
		//		if (fromDate){
		//			if (searchQuery.Timestamp === undefined)
		//				searchQuery.Timestamp = {};
        //
		//			searchQuery.Timestamp.$gte = fromDate;
		//		}
        //
		//	var  toDate = statSearchQuery.to;
		//		if (toDate){
		//			if (searchQuery.Timestamp === undefined)
		//				searchQuery.Timestamp = {};
        //
		//			searchQuery.Timestamp.$lt = toDate;
		//		}
		//}

		//var statistic = Statistic.find();


		var pagesNum = Statistic.find().count();
		Session.set('pagesNum', pagesNum);

		var statistic = Statistic.find({}, {sort:{cellNumber: 1}, skip:skip, limit: 50});
		return statistic;
	},

	time: function(){
		return moment(this.Timestamp).format('DD-MM-YY;HH:mm');
	},

	pages: function(){

		var pagesNum = Session.get('pagesNum');

		var a = [];
		for(var i=0; i<Math.ceil(pagesNum/50); i++){
			a.push(i);
		}
		return a;
	},

	zero: function() {
		if (Statistic.find().count() === 0)
			return true;
	}
})