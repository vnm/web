Template.selectStatistics.helpers({
	Clients: function(){
		return Clients.find();
	},

	selectedTerminal: function(){
		return Session.get('selectedTerminal');
	},

	selected: function(value){
		var ssq = Session.get('statSearchQuery');



		if (ssq && ssq.selectedTerminal === value)
			return 'selected';
		else
			return "";
	},

	from: function(){
		var q = Session.get('statSearchQuery');
		if (q){
			if (q.from)
				return moment(q.from).format("HH:mm DD.MM.YYYY");
		}
	},

	to: function(){
		var q = Session.get('statSearchQuery');
		if (q){
			if (q.to)
				return moment(q.to).format("HH:mm DD.MM.YYYY");
		}
	}
});

Template.selectStatistics.events({
	'click button.showStat': function(){
		var statSearchQuery = {};

		statSearchQuery.selectedTerminal = $('.terminalSelect').val();

		if ($('#statFromdate').data("DateTimePicker").date()!== null)
			statSearchQuery.from = $('#statFromdate').data("DateTimePicker").date().unix()*1000;

		if ($('#statTodate').data("DateTimePicker").date()!==null)
			statSearchQuery.to = $('#statTodate').data("DateTimePicker").date().unix()*1000;

		Session.set('statSearchQuery', statSearchQuery);
		var cr = Router.current().route.getName();
		var lang = Router.current().params.lang;
		Router.go(cr, {page: 0, lang: lang});
	},

	'click button.showAll': function(){
		Session.set('statSearchQuery', undefined);
	},

	'click button.downloadSelected': function(e,t){
		//https://atmospherejs.com/lfergon/exportcsv

		var nameFile = 'statistic(' + moment().format("DD.MM HH:mm") + ').csv';
		var statSearchQuery = Session.get('statSearchQuery');
		Meteor.call('download', statSearchQuery, function(err, fileContent) {
			if(fileContent){
				var blob = new Blob([fileContent], {type: "text/plain;charset=utf-8"});
				saveAs(blob, nameFile);
			}
		});
	}
});

Template.selectStatistics.rendered = function(){
	this.$('#statFromdate').datetimepicker({format: 'HH:mm DD.MM.YYYY'});
	this.$('#statTodate').datetimepicker({format: 'HH:mm DD.MM.YYYY'});
};

