Template.statProgress.helpers({
	termCount: function(){
		return Clients.find().count();
	},

	termOk: function() {
		return Session.get('statok');
	},

	termNR: function(){
		return Session.get('statnr');
	}
})