Template.statTableRow.helpers({
	time: function(){
		return moment(this.Timestamp).format('DD-MM-YY;HH:mm');
	}
});