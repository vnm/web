/**
 * Created by Alexey on 03.09.2015.
 */
Template.descriptionModal.helpers({
});

Template.descriptionModal.events({
    "click .textSaveBtn": function(e, t){

        var html =  $(e.target.parentElement.parentElement).find('#summernote').summernote('code');
        var text = $(e.target.parentElement.parentElement).find('#summernote').summernote('code')
            .replace(/<\/p>/gi, "\n")
            .replace(/<br\/?>/gi, "\n")
            .replace(/<\/?[^>]+(>|$)/g, "");

        var id = Session.get('clickedRenterId');
        var lang = Router.current().params.lang;

        var o = {};
        o[lang+".description.text"] = text;
        o[lang+".description.html"] = html;

        Renters.update(id, {$set: o}, function(err){
            if(err){
                console.log('Ошибка изменения description: ' + err);
            }
            else{
                console.log('Description изменен text: ' + text);
                console.log('Description изменен html: ' + html);
            }
        });

        RenderAndSendText();

        function RenderAndSendText(){

            var div = document.createElement("div");
            $(div).width(width);

            div.innerHTML  = html;

            console.log(div);

            document.body.appendChild(div);
            // var div = $('div.editable');

            var scrollPos = document.body.scrollTop;

            html2canvas(div,{
                onrendered: function(canvas) {
                    Meteor.call('saveURL', canvas.toDataURL(), id, "textRender", lang);
                    document.body.removeChild(div);
                    $('#descModal').modal('hide');
                    window.scrollTo(0,scrollPos);
                    //$(e.target.parentElement).slideUp();
                }
            });
        }

        
    }
});

Template.descriptionModal.rendered = function(){
    //https://atmospherejs.com/summernote/standalone
    $('#summernote').summernote({
        height: 200,
        minHeight: 200,
        width: width+10,
        toolbar: [
            //[groupname, [button list]]

            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            //['color', ['color']],
            ['para', ['paragraph']],
            ['height', ['height']],
        ]
    });

   // console.log('sn init');

    var d = $('#summernote').next();
    d.addClass("col-center");


}