/**
 * Created by Alexey on 03.09.2015.
 */
Template.categoryModalString.helpers({
    title: function(){
        return this.ru.title;
    }
})

Template.categoryModalString.events({
    'click a': function(e,t){
        var id = Session.get('clickedRenterId');
        var catId = e.target.getAttribute("myid");
        var oldId = Renters.findOne(id).category;
        if (catId == oldId)
            return;
        
        Renters.update(id, {$set: {category: catId}}, function(err){
            if(err){
                console.log('Ошибка изменения category: ' + err);
            }
            else{
                console.log('category изменен: ' + catId);
            }
        });
        $('#catModal').modal('hide');
    }
})