/**
 * Created by Alexey on 03.09.2015.
 */
Template.categoryModal.helpers({
    'categories': function(){
    	var lang = Router.current().params.lang;
    	var a = {};
    	var path = lang+".title";
    	a[path]= 1;
        return Categories.find({},{sort: a});
    }
})