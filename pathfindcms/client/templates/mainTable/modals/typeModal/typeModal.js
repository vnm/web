/**
 * Created by Alexey on 03.09.2015.
 */
Template.typeModal.helpers({
    'types': function(){
        return Types.find({},{sort: {title: 1}});
    }
})