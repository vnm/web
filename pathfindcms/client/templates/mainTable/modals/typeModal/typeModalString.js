/**
 * Created by Alexey on 03.09.2015.
 */
Template.typeModalString.helpers({
    title: function(){
        return this.title;
    }
})

Template.typeModalString.events({
    'click a': function(e,t){
        var id = Session.get('clickedRenterId');
        var typeId = e.target.getAttribute("myid");
        var oldId = Renters.findOne(id).type;

        if (typeId == oldId){
            $('#typeModal').modal('hide');
            return;
        }

        
        Renters.update(id, {$set: {type: typeId}}, function(err){
            if(err){
                console.log('Ошибка изменения type: ' + err);
            }
            else{
                console.log('Type изменен: ' + typeId);
            }
        });
        $('#typeModal').modal('hide');
    }
})