/**
 * Created by Alexey on 03.09.2015.
 */
Template.clearRenterModal.events({
    'click .clearRenterBtn': function(e,t){
        var renterId = Session.get('clickedRenterId');
        Meteor.call('clearRenter', renterId);
        $('#clearRenterModal').modal('hide');
    }
})