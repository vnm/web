/**
 * Created by Alexey on 03.09.2015.
 */
Template.divisionModalString.events({
    'click a': function(e,t){
        var id = Session.get('clickedRenterId');

        var divId = e.target.getAttribute("data-id");
        var oldId = Renters.findOne(id).division;
        if (divId == oldId)
            return;

        Renters.update(id, {$set: {division: divId}}, function(err){
            if(err){
                console.log('Ошибка изменения division: ' + err);
            }
            else{
                console.log('Division изменен: ' + divId);
            }
        });
        $('#divisionModal').modal('hide');
    }
})