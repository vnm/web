/**
 * Created by Alexey on 03.09.2015.
 */
Template.divisionModal.helpers({
    'divisions': function(){
        return Divisions.find({}, {sort: {title: 1}});
    }
})