/**
 * Created by Alexey on 01.09.2015.
 */
Template.tableRow.helpers({
    title: function(){
        var lang = Router.current().params.lang;
        return this[lang].title;
    },
    modelBinding: function(){
        if (this.modelBinding)
            return this.modelBinding;
        else
            return "не задана";
    },
    category: function(){
        var lang = Router.current().params.lang;
        var catId = this.category;
        //console.log('catId: ' + catId);
        //console.log('Id: ' + this._id);
        //console.log('cat: ' + Categories.findOne(catId)[lang].title)
        return Categories.findOne(catId)[lang].title;
    },
    description: function(){
        var lang = Router.current().params.lang;
        return this[lang].description.text;
    },
    logo: function(){
        var lang = Router.current().params.lang;

        if  (this[lang].logo && this[lang].logo.filePath){
            var a = this[lang].logo.filePath.split("/");
            return a[a.length-1];
        }
        else{
            return "не загружен";
        }
    },
    type: function(){
        var typeID = this.type;
        var type = Types.findOne(typeID);
        return type.title;
    },
    isActive: function(){
        if (this.isActive === true)
            return 'checked';
        else
            return "";
    },
    isAdmin: function(){

        if(Meteor.user().username === 'admin')
            return true;
        else
            return false;
    },
    logoFileId: function(){
        var lang = Router.current().params.lang;
        if (this[lang] && this[lang].logo && this[lang].logo.fileId)
            return this[lang].logo.fileId;
        else
            return false;
    },
    isLogoActive: function(){
        if (this.isLogoActive === true)
            return 'checked';
        else
            return "";
    },
    popoverContent: function(){
        var lang = Router.current().params.lang;
        if (this[lang] && this[lang].logo && this[lang].logo.fileId)
            return "<img src='/file/" + this[lang].logo.fileId + "'/>";
        else
            return "логотип не загружен";
    },
    keywords: function(){

        if(this.keywords)
            return this.keywords;
        else
            return "";
    },
    division: function(){
        if (this.division)
            return Divisions.findOne(this.division).title;
        else
            return false;
    },
    modifiedAt: function(){
        if (this.modifiedAt)
            return moment(this.modifiedAt).format('DD-MM-YY;HH:mm');
        else
            return "";
    },
    isInSearch: function(){
        if (this.includeInSearch)
            return "checked"
        else
            return "";
    }
})

Template.tableRow.events({

    'keydown td.cellNumber': function(e,t){
        if(e.keyCode === 13){
            e.preventDefault();
            $(e.target).blur();
        }

        if (e.keyCode === 27) {
            e.preventDefault();
            e.target.textContent = this.cellNumber;
            $(e.target).blur();
        }
    },

    'blur td.cellNumber': function(e,t){
        var newCellNumber = e.target.textContent;

        if (newCellNumber == this.cellNumber)
            return;

        e.target.textContent = "";
        var id = this._id;

        Renters.update(id, {$set: {'cellNumber': newCellNumber}}, function(err){
            if(err){
                console.log('Ошибка изменения cellNumber: ' + err);
            }
            else{
                console.log('cellNumber изменен: ' + newCellNumber);
            }
        });
    },

    'keydown td.tableTitleEditable': function(e,t){
        if(e.keyCode === 13){
            e.preventDefault();
            $(e.target).blur();
        }

        if (e.keyCode === 27) {
            e.preventDefault();
            var lang = Router.current().params.lang;
            e.target.textContent = this[lang].title;
            $(e.target).blur();
        }
    },

    'blur td.tableTitleEditable': function(e,t){
        var newTitle = e.target.textContent;
        var lang = Router.current().params.lang;

        if (newTitle ===this[lang].title)
            return;

        e.target.textContent = "";
        var id = this._id;


        var o = {};
        o[lang+".title"] = newTitle;

        Renters.update(id, {$set: o}, function(err){
            if(err){
                console.log('Ошибка изменения title: ' + err);
            }
            else{
                console.log('Title изменен: ' + newTitle);
            }
        });
    },

    'change .activeCheckBox': function(e,t){
        if (e.target.checked === this.isActive)
            return;
        var id = this._id;
        Renters.update(id, {$set: {"isActive": e.target.checked}}, function(err){
            if(err){
                console.log('Ошибка изменения isActive: ' + err);
            }
            else{
                console.log('isActive изменен: ' + e.target.checked);
            }
        })
    },

    'change .isInSearchCheckBox': function(e,t){
        if (e.target.checked === this.includeInSearch)
            return;

        var id = this._id;

        Renters.update(id, {$set: {"includeInSearch": e.target.checked}}, function(err){
            if(err){
                console.log('Ошибка изменения includeInSearch: ' + err);
            }
            else{
                console.log('includeInSearch изменен: ' + e.target.checked);
            }
        })
    },

    'click td.tableCategory' :function(e,t){
        Session.set('idForCategory', this._id);
        Session.set('clickedRenterId', this._id);
        $('#catModal').modal();
    },

    'click td.tableDescription': function(e,t){
        Session.set('clickedRenterId', this._id);
        var id = this._id;
        var lang = Router.current().params.lang;

        var html = Renters.findOne(id)[lang].description.html;
        //console.log(html);
        $('#summernote').summernote('code', html);
        $('#descModal').modal();
    },

    'keydown td.tableKeyWords': function(e,t){
        if(e.keyCode === 13){
            e.preventDefault();
            $(e.target).blur();
        }

        if (e.keyCode === 27) {
            e.preventDefault();
            if (this.keywords)
                e.target.textContent = this.keywords;
            else
                e.target.textContent = "";
            $(e.target).blur();
        }
    },

    'blur td.tableKeyWords': function(e,t){

        var newKeyWords = e.target.textContent;
        var oldKeyWords = this.keywords;

        if (newKeyWords === oldKeyWords)
            return;

        //if (!oldKeyWords)
        //    return;

        if (newKeyWords.length === 0 && oldKeyWords.length === 0)
            return;

        e.target.textContent = "";
        var id = this._id;



        Renters.update(id, {$set: {keywords: newKeyWords}}, function(err){
            if(err){
                console.log('Ошибка изменения keywords: ' + err);
            }
            else{
                console.log('keywords изменен: ' + newKeyWords);
            }
        });
    },

    'click td.tableLogo': function(e,t){
        Session.set('clickedRenterId', this._id);
        $('#logoselect').click();
    },

    'change .activeLogoCheckBox': function(e,t){
        if (e.target.checked === this.isLogoActive)
            return;

        var id = this._id;
        Renters.update(id, {$set: {"isLogoActive": e.target.checked}}, function(err){
            if(err){
                console.log('Ошибка изменения isLogoActive: ' + err);
            }
            else{
                console.log('isLogoActive изменен: ' + e.target.checked);
            }
        })
    },

    'click td.tableType' :function(e,t){
        Session.set('clickedRenterId', this._id);
        $('#typeModal').modal();
    },

    'click td.tableClearRenter' : function(e,t){
        Session.set('clickedRenterId', this._id);
        $('#clearRenterModal').modal();
    },

    'keydown td.tableBinding': function(e,t){
        if(e.keyCode === 13){
            e.preventDefault();
            $(e.target).blur();
        }

        if (e.keyCode === 27) {
            e.preventDefault();
            e.target.textContent = this.modelBinding;
            $(e.target).blur();
        }
    },

    'blur td.tableBinding': function(e,t){
        var newModelBinding = e.target.textContent;

        if (newModelBinding == this.modelBinding)
            return;

        e.target.textContent = "";
        var id = this._id;

        Renters.update(id, {$set: {'modelBinding': newModelBinding}}, function(err){
            if(err){
                console.log('Ошибка изменения modelBinding: ' + err);
            }
            else{
                console.log('modelBinding изменен: ' + newModelBinding);
            }
        });
    },

    'click td.tableDivision': function(e,t){
        Session.set('clickedRenterId', this._id);
        $('#divisionModal').modal()
    }

})

Template.tableRow.rendered = function(){

    $(this.firstNode).find('.popoverAncor').popover();

}

