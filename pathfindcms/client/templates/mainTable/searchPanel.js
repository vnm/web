Template.searchPanel.helpers({
	searchNo : function(){
		return Session.get('searchNo');
	},
	searchName: function(){
		return Session.get('searchName');
	}
});

Template.searchPanel.events({
	'change #searchNo': function(e,t){

		Session.set("searchNo", $(e.target).val());

		var cr = Router.current().route.getName();
        var lang = Router.current().params.lang;
        Router.go(cr, {page: 0, lang: lang});
	},

	'change #searchName': function(e,t){

		Session.set("searchName", $(e.target).val());

		var cr = Router.current().route.getName();
        var lang = Router.current().params.lang;
        Router.go(cr, {page: 0, lang: lang});
	}
})