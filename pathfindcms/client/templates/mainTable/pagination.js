/**
 * Created by Alexey on 02.09.2015.
 */
Template.pagination.helpers({
    pageNum: function(){
        return this+1;
    },
    currentRoute: function(){
        return Router.current().route.getName();
    },
    lang: function(){
        return Router.current().params.lang;
    },
    isActive: function(){
        if (Router.current().params.page == this)
            return "active";
        else
            return "";
    }
})