/**
 * Created by Alexey on 02.09.2015.
 */
Template.sortPanelButton.helpers({
    isActive: function(){
        var sort = Session.get('sort');

        if (sort == this.name)
            return "active";
        else
            return "";
        }
})
