/**
 * Created by Alexey on 02.09.2015.
 */
Template.divisionPanel.helpers({
    divisions: function(){
        return Divisions.find();
    }
})

Template.divisionPanel.events({
    'click a': function(e, t){
        e.preventDefault();
        var division = e.target.getAttribute("data");
        var div = Session.get('division');
        if(division == div)
            Session.set('division', undefined);
        else
            Session.set('division', division);

        var cr = Router.current().route.getName();
        var lang = Router.current().params.lang;
        Router.go(cr, {page: 0, lang: lang});
    }
})