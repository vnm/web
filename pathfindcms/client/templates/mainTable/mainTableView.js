/**
 * Created by Alexey on 01.09.2015.
 */
Template.mainTableView.helpers({
    renters: function(){

        var page = Router.current().params.page;
        var skip = page*20;

        var division = Session.get('division');
        var cellNumber = Session.get('searchNo');
        var renter = Session.get('searchName');

        var searchQuery = {};
        if (division)
            searchQuery.division = division;
        if (cellNumber)
            searchQuery.cellNumber = {'$regex': cellNumber};
        if (renter){
            searchQuery['ru.title'] = {'$regex': renter, '$options': 'i'};
        }

        //console.log(searchQuery);

        var renters = Renters.find(searchQuery);
        var pagesNum = renters.count();
        Session.set('pagesNum', pagesNum);

        renters = Renters.find(searchQuery, {sort:{cellNumber: 1}, skip:skip, limit: 20});
        return renters;
    },
    pages: function(){

        var pagesNum = Session.get('pagesNum');

        var a = [];
        for(var i=0; i<Math.ceil(pagesNum/20); i++){
            a.push(i);
        }
        return a;
    },
    //currentRoute: function(){
    //    return Router.current().route.getName();
    //},
    lang: function(){
        return Router.current().params.lang;
    },
    maxpage: function(){
        var pagesNum = this.renters.count();
        return Math.ceil(pagesNum/20) - 1;
    },
    isAdmin: function(){

        if(Meteor.user().username === 'admin')
            return true;
        else
            return false;
    },
});

Template.mainTableView.events({
    'change #logoselect': function(e,t){

        var id = Session.get('clickedRenterId');
        var lang = Router.current().params.lang;
        var file = e.currentTarget.files[0];
        if (file.size> 100000){
            alert('Логотип не более 100Кб')
            return;
        }

        var contentName= 'logo';

        //console.log(lang);
        Meteor.saveFile(file, file.name, id, contentName, "ru", function(p){
            if(p>=1){
                //e.currentTarget.value = "";
            }
        });

        Meteor.saveFile(file, file.name, id, contentName, "en", function(p){
            if(p>=1){
                e.currentTarget.value = "";
            }
        });
    }
})

Template.mainTableView.rendered = function(){
    var division = Divisions.findOne({default: true})._id;
    Session.set('division', division);
}