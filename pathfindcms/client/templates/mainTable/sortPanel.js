/**
 * Created by Alexey on 02.09.2015.
 */
Template.sortPanel.helpers({
    sortArray: function(){
      return sortValues;
    }
})

Template.sortPanel.events({
    'click a': function(e, t){

        var sort = e.target.getAttribute("data");
        var s = Session.get('sort');

        if (s === undefined)
        {
            Session.set('sort', sort);
        }

        if(sort == s)
            Session.set('sort', undefined);
        else
            Session.set('sort', sort);

    }
})