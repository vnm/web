/**
 * Created by Alexey on 02.09.2015.
 */
Template.divisionPanelButton.helpers({
    isActive: function(){
        var div = Session.get('division');
        if (div === undefined)
            return "";

        if (div == this._id)
            return "active"
        else
            return ""
    }
})