/**
 * Created by Alexey on 07.09.2015.
 */
Template.navbar.helpers({
    'lang': function(){
        return Router.current().params.lang;
    },
    'page': function(){
        return Router.current().params.page;
    },
    currentRoute: function(){
        return Router.current().route.getName();
    },
    isAdmin: function(){
        if(Meteor.user().username === 'admin')
            return true;
        else
            return false;
    },
    mainTable: function(){
        if (Router.current().route.getName() === 'mainTableView')
            return true;
        else
            return false;
    }
})