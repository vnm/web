/**
 * Created by Alexey on 01.09.2015.
 */
Template.login.events({
    'submit .form-signin': function(e ,t){
        e.preventDefault();

        var name = t.find('#login').value;
        var password = t.find('#inputPassword').value;

        Meteor.loginWithPassword(name, password, function(err){
            if (err)
                console.log('Login failed, reason: ' + err);
            else {
                console.log("Login ok");
                Router.go('homeRedirect');
            }
        })
    }
})