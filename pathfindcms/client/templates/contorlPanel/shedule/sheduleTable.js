Template.sheduleTable.helpers({
	'tasks': function(){
		return FutureTasks.find();
	},

	'jobList': function(){
		return jobList;
	},

	'jobRepeat' : function(){
		return jobRepeat;
	}
})

Template.sheduleTable.events({
	'click td.addSheduleTask': function(e, t){
		var taskName = $(e.target.parentElement.parentElement).find('.taskName').val();
		var jobName = $(e.target.parentElement.parentElement).find('.taskJob').val();

		var taskStart = $('#taskStart').data("DateTimePicker").date().format();
		var taskRepeat = $(e.target.parentElement.parentElement).find('.taskRepeat').val();

		Meteor.call('sheduleTask', taskName, jobName, taskStart, taskRepeat);

		$(e.target.parentElement.parentElement).find('.taskName').val("");
		$(e.target.parentElement.parentElement).find('.taskJob :first').attr("selected", "selected");
		$(e.target.parentElement.parentElement).find(".taskStart").val("");
		$(e.target.parentElement.parentElement).find('.taskRepeat :first').attr("selected", "selected");
	}
})

Template.sheduleTable.rendered = function(){
	$('#taskStart').datetimepicker({format: 'HH:mm DD.MM.YYYY'});
}

Template.sheduleTableRow.helpers({
	'jobName': function(){
		for (var i=0; i< jobList.length; i++)
		{
			if (jobList[i].job === this.jobName)
				return jobList[i].name;
		}
	},

	'taskStart': function(){
		return moment(this.taskStart).format("HH:mm DD.MM.YYYY");
	},

	'taskRepeat': function(){
		for (var i=0; i< jobRepeat.length; i++)
		{
			if (jobRepeat[i].repeat === this.taskRepeat)
				return jobRepeat[i].name;
		}
	},
});

Template.sheduleTableRow.events({
	'click td.removeSheduleTask' : function(){
		Meteor.call('removeTask', this._id);
	}

})