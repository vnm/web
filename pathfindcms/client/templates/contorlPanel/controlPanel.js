Template.controlPanel.helpers({
	'Clients': function(){
		return Clients.find();
	},

	'lastUpdate': function(){
		var ts = Renters.findOne({}, {sort: {modifiedAt:-1}}).modifiedAt;
		if (ts)
			return moment(ts).format('DD-MM-YY;HH:mm');
	},

	'adminsGroup': function(){
		var group = Meteor.users.findOne(Meteor.userId()).profile.group;
		return group === "admins"
	},

	'suMail': function(){
		return Meteor.users.findOne({username: 'superuser'}).profile.mail;
	},

	'statisticBDCount': function(){
		return ReactiveMethod.call('statisticBDCount');
	}

});

Template.controlPanel.events({
	'click a.addTerminal': function(){
		var ip = prompt("Please enter IP");
		if(ip.length > 8){
			Clients.insert({ip: ip});
		}
	},
	'click button.refreshAll': function(){
		Meteor.call('refreshAllClients');
	},

	'click button.rebootAll': function(){
		Meteor.call('rebootAllClients');
	},

	'click button.turnoffAll': function(){
		Meteor.call('turnoffAllClients');
	},

	'click button.addJob': function(){
		Meteor.call('sheduleRefreshOne');
	},

	'click button.changeupwd': function(){
		var pwd = $('#userpwd').val();
		Meteor.call('changePwd', 'user', pwd);
		$('#userpwd').val('');
	},

	'click button.changesupwd': function(){
		var pwd = $('#supwd').val();
		Meteor.call('changePwd', 'superuser', pwd);
		$('#supwd').val('');
	},

	'click button.changesumail': function(){
		var mail = $('#sumail').val();
		Meteor.call('changeMail', 'superuser', mail);
		$('#sumail').val('');
	},

	'click button.deleteDb': function(){
		var ts = $('#removeBefore').data("DateTimePicker").date().unix()*1000;
		Meteor.call('clearStatDB', ts);
	}
});

Template.controlPanel.rendered = function(){
	$('#removeBefore').datetimepicker({format: 'HH:mm DD.MM.YYYY'});
}