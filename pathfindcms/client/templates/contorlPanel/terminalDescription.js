Template.terminalDescription.helpers({
	'panelClass': function(){
		if (this.status === 'online')
			return 'panel-success';
		if (this.status === 'offline')
			return 'panel-warning';
		if (this.status === 'softwareisdown')
			return 'panel-danger';

		return 'panel-default';
	},

	'updatedAt' : function(){
		var ts = Clients.findOne(this._id).updatedAt;
		if (ts)
			return moment(parseInt(ts)).format('DD-MM-YY;HH:mm');
		else
			return "нет данных"
	},

	disabled: function(){
		if (this.status !=='online')
			return 'disabled';
		else
			return "";
	},
	'adminsGroup': function(){
		var group = Meteor.users.findOne(Meteor.userId()).profile.group;
		return group === "admins"
	},
})

Template.terminalDescription.events({
	'click button.refresh': function(){
		var id = this._id;
		Meteor.call('refreshClient', id);
	},

	'click button.reboot': function(){
		var id = this._id;
		Meteor.call('rebootClient', id);
	},

	'click button.turnoff': function(){
		var id = this._id;
		Meteor.call('turnoffClient', id);
	},

	'click button.deleteTerminal': function(){
		Clients.remove(this._id);
	}
})
