/**
 * Created by Alexey on 07.09.2015.
 */
Template.divisionTableRow.helpers({
    'isDefault': function(){
        if (this.default === true)
            return "defaultRow";
        else
            return false;
    }
});

Template.divisionTableRow.events({
    'click td.tableClearDivision' : function(){
        Divisions.remove(this._id, function(){
            console.log('Удален раздел');
        });
    },
    'keydown td.title': function(e,t){
        if(e.keyCode === 13){
            e.preventDefault();
            $(e.target).blur();
        }

        if (e.keyCode === 27) {
            e.preventDefault();
            e.target.textContent = this.title;
            $(e.target).blur();
        }
    },
    'blur td.title': function(e,t){
        var newTitle = e.target.textContent;

        if (newTitle == this.title)
            return;

        e.target.textContent = "";
        var id = this._id;

        Divisions.update(id, {$set: {'title': newTitle}}, function(err){
            if(err){
                console.log('Ошибка изменения title: ' + err);
            }
            else{
                console.log('title изменен: ' + newTitle);
            }
        });
    }
})