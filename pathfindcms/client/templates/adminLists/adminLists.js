/**
 * Created by Alexey on 08.09.2015.
 */
Template.adminLists.helpers({
    'division' : function(){
        var div = Session.get('adminListsClickedDivision');
        if (div)
            return Divisions.findOne(div).title;
        else
            return "не задан"
    }
})
Template.adminLists.events({
    'click td.listsAddCategory': function(e,t) {
        var obj = {};
        obj.ru = {};
        obj.en = {};
        obj.ru.title = $('.catRuTitle')[0].textContent;
        obj.en.title = $('.catEnTitle')[0].textContent;

        Categories.insert(obj, function(){
            console.log('Категория добавлена, ' +  obj);
        });

        $('.catRuTitle')[0].textContent = "";
        $('.catEnTitle')[0].textContent = "";
    },

    'click td.listsAddType': function(e,t) {

        var obj = {};

        obj.name = $('.typeName')[0].textContent;
        obj.title = $('.typeTitle')[0].textContent;

        Types.insert(obj, function(){
            console.log('Категория добавлена, ' +  obj);
        });

        $('.typeName')[0].textContent = "";
        $('.typeTitle')[0].textContent = "";
    },

    'click td.divisionAdd': function(e,t) {

        var obj = {};

        obj.title = $('.divisionTitle')[0].textContent;

        Divisions.insert(obj, function(){
            console.log('Раздел добавлен, ' +  obj);
        });

        $('.divisionTitle')[0].textContent = "";
    },
})