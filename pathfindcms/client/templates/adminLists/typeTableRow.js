/**
 * Created by Alexey on 07.09.2015.
 */
Template.typeTableRow.helpers({
    'isDefault': function(){
        if (this.default === true)
            return "defaultRow";
        else
            return false;
    }
});

Template.typeTableRow.events({
    'click td.typeClear' : function(){
        Types.remove(this._id, function(){
            console.log('Удален тип');
        });
    },
    'keydown td.title': function(e,t){
        if(e.keyCode === 13){
            e.preventDefault();
            $(e.target).blur();
        }

        if (e.keyCode === 27) {
            e.preventDefault();
            e.target.textContent = this.title;
            $(e.target).blur();
        }
    },
    'blur td.title': function(e,t){
        var newTitle = e.target.textContent;

        if (newTitle == this.title)
            return;

        e.target.textContent = "";
        var id = this._id;

        Types.update(id, {$set: {'title': newTitle}}, function(err){
            if(err){
                console.log('Ошибка изменения title: ' + err);
            }
            else{
                console.log('title изменен: ' + newTitle);
            }
        });
    },
    'keydown td.name': function(e,t){
        if(e.keyCode === 13){
            e.preventDefault();
            $(e.target).blur();
        }

        if (e.keyCode === 27) {
            e.preventDefault();
            e.target.textContent = this.name;
            $(e.target).blur();
        }
    },
    'blur td.name': function(e,t){
        var newName = e.target.textContent;

        if (newName == this.name)
            return;

        e.target.textContent = "";
        var id = this._id;

        Categories.update(id, {$set: {'name': newName}}, function(err){
            if(err){
                console.log('Ошибка изменения name: ' + err);
            }
            else{
                console.log('name изменен: ' + newName);
            }
        });
    },
})