/**
 * Created by Alexey on 07.09.2015.
 */
Template.categoryTableRow.helpers({
    'ruTitle': function(){
        return this.ru.title;
    },
    'enTitle': function(){
        return this.en.title;
    },
    'isDefault': function(){
        if (this.default === true)
            return "defaultRow";
        else
            return false;
    }
});

Template.categoryTableRow.events({
    'click td.tableClearRenter' : function(){
        Categories.remove(this._id, function(){
            console.log('Удалена категория');
        });
    },
    'keydown td.ruTitle': function(e,t){
        if(e.keyCode === 13){
            e.preventDefault();
            $(e.target).blur();
        }

        if (e.keyCode === 27) {
            e.preventDefault();
            e.target.textContent = this.ru.title;
            $(e.target).blur();
        }
    },
    'blur td.ruTitle': function(e,t){
        var newRuTitle = e.target.textContent;

        if (newRuTitle == this.ru.title)
            return;

        e.target.textContent = "";
        var id = this._id;

        Categories.update(id, {$set: {'ru.title': newRuTitle}}, function(err){
            if(err){
                console.log('Ошибка изменения ru.title: ' + err);
            }
            else{
                console.log('ru.title изменен: ' + newRuTitle);
            }
        });
    },
    'keydown td.enTitle': function(e,t){
        if(e.keyCode === 13){
            e.preventDefault();
            $(e.target).blur();
        }

        if (e.keyCode === 27) {
            e.preventDefault();
            e.target.textContent = this.en.title;
            $(e.target).blur();
        }
    },
    'blur td.enTitle': function(e,t){
        var newEnTitle = e.target.textContent;

        if (newEnTitle == this.en.title)
            return;

        e.target.textContent = "";
        var id = this._id;

        Categories.update(id, {$set: {'en.title': newEnTitle}}, function(err){
            if(err){
                console.log('Ошибка изменения en.title: ' + err);
            }
            else{
                console.log('en.title изменен: ' + newEnTitle);
            }
        });
    },
})