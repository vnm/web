Guests = new Meteor.Collection("guests");

Guests.allow({
    update: function(userId){
        if (userId)
            return true;
    }
});

if (Meteor.isServer){
    Meteor.publish("guests", function(){
        return Guests.find();
    });
}