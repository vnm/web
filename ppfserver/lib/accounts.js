AccountsTemplates.configure({
    // Behavior
    confirmPassword: true,
    enablePasswordChange: true,
    forbidClientAccountCreation: true,
    overrideLoginErrors: true,
    sendVerificationEmail: true,
    lowercaseUsername: false,
    focusFirstInput: true,

    // Appearance
    showAddRemoveServices: false,
    showForgotPasswordLink: false,
    showLabels: true,
    showPlaceholders: true,
    showResendVerificationEmailLink: false,

    // Client-side Validation
    continuousValidation: false,
    negativeFeedback: true,
    negativeValidation: true,
    positiveValidation: true,
    positiveFeedback: true,
    showValidating: true,

    // Privacy Policy and Terms of Use
    privacyUrl: 'privacy',
    termsUrl: 'terms-of-use',

    // Redirects
    homeRoutePath: '/',
    redirectTimeout: 4000,

    // Hooks
    //  onLogoutHook: onLogoutHook,
    //  onSubmitHook: onSubmitHook,
    //  preSignUpHook: myPreSubmitFunc,
    //  postSignUpHook: myPostSubmitFunc,

    // Texts
    texts: {
        button: {
            signIn: "Sign In",
            forgotPwd: "Wrong password",
        },
        title: {
            signIn: "PPF",
            verifyEmail: "Confirm password",
            forgotPwd: "Forgot password"
        },
        errors: {
            accountsCreationDisabled: "Account creation is disabled",
            cannotRemoveService: "",
            loginForbidden: "Entrance error",
            mustBeLoggedIn: "You must be logged in",
            pwdMismatch: "Password mismatch",
            validationErrors: "Validation errors"
        }
    }
});


AccountsTemplates.removeField('password');
AccountsTemplates.removeField('email');

AccountsTemplates.addFields([
    {
        _id: "username",
        type: "text",
        required: true,
        minLength: 5,
        hasIcon: true,
        iconClass: "user",
        placeholder: "Login",
        errStr: 'Enter login',
        displayName: "Login"
    },
    {
        _id: 'password',
        type: 'password',
        required: true,
        minLength: 6,
        hasIcon: true,
        iconClass: "lock",
        placeholder: "Password",
        displayName: "Password"
    }
]);

AccountsTemplates.configureRoute('signIn', {
    name: 'login',
    path: '/login',
    template: 'login',
    layoutTemplate: 'loginLayout',
    redirect: '/',
});
