Router.route('guests', {
    path: 'guests',
    layoutTemplate: 'mainLayout',
    template: 'guests',

    waitOn: function(){
        return [Meteor.subscribe('guests')];
    },
    data: function(){
        var data = {};
        data.params = {};
        data.guests = Guests.find();
        return data;
    }
});