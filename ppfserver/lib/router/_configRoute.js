Router.configure({
    loadingTemplate: 'loading',
    // waitOn: function(){ return [Meteor.subscribe("guests"), Meteor.subscribe("terminals")]}
});

Router.onBeforeAction(function() {
    if (Meteor.isClient) {
        if (!Meteor.userId()) {
            this.redirect('login');
            if (this.ready) {
                this.next();
            }
            else
                this.render('loading');
        } else {
            if (this.ready) {
                this.next();
            }
            else
                this.render('loading');
        }
    }
    if (Meteor.isServer){
        this.next();
    }
});

Router.route('home',{
    path: '/',
    action: function(){
        this.redirect('guests');
    }
});

Router.route('logout',{
    path: '/logout',
    action: function() {
        Meteor.logout(function (err) {
            if (err)
                console.log('Logout failed, reason: ' + err);
            else {
                console.log('Logout ok');
            }
        });
    }
});