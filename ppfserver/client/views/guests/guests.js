Template.guests.helpers({
    guestsTotal: function(){
        return Guests.find().count();
    },
    zone1T: function(){
        return Guests.find({zone1: true}).count()
    },
    zone1F: function() {
        return Guests.find({zone1: false}).count()
    },
    mzoneT: function(){
        return Guests.find({mzone: true}).count()
    },
    mzoneF: function(){
        return Guests.find({mzone: false}).count()
    },
    drinkT: function(){
        return Guests.find({drink: true}).count()
    },
    drinkF: function(){
        return Guests.find({drink: false}).count()
    },
    raincoat: function(){
        return Guests.find({raincoat: 'got'}).count()
    },
    hoodie: function(){
        return Guests.find({hoodie: 'got'}).count()
    }
});