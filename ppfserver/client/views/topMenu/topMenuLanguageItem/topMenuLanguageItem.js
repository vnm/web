Template.topMenuLanguageItem.helpers({
    //add you helpers here
});

Template.topMenuLanguageItem.events({
    "click a": function(e,t){
        e.preventDefault();

        let p = Template.parentData(1).params;
        p.language = this.shortEngTitle;
        let route = Router.current().route.getName();

        Router.go(route, p);
    }
});

Template.topMenuLanguageItem.onCreated(function () {
    //add your statement here
});

Template.topMenuLanguageItem.onRendered(function () {
    //add your statement here
});

Template.topMenuLanguageItem.onDestroyed(function () {
    //add your statement here
});

