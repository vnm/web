Template.topMenuTerminalItem.helpers({
    //add you helpers here
});

Template.topMenuTerminalItem.events({
    "click a": function(e,t){
        e.preventDefault();

        Meteor.call('refreshClient', this._id, 'refreshAll', function(e,r){
            if (r) {
                if (r.result === "error") {
                    toastr.error("Cannot refresh " + r.ip, "Error");
                }
                if (r.result === "success") {
                    toastr.success(r.ip + " successfully refreshed", "Success");
                }
            }
        });
    }
});