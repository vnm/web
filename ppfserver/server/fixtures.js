let users = [
    {
        name        : "admin",
        email       : "admin@email.com",
        pass        : "aaaa",
        roles       : ["admin"]
    }

];

//roles : admin, register, bar
_.each(users, function (user) {
    let isStored = Meteor.users.findOne({username: user.name});
    if (!isStored) {
        Accounts.createUser({
            username   : user.name,
            email      : user.email,
            password   : user.pass,
            profile     : {role: user.roles[0]}
        });
    }
});

