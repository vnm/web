Router.route('/api/guests', { where: 'server' })
    .get(function () {
        let settings ={};

        settings.currentLanguage = Settings.findOne({name: "languages"}).currentLanguage;
        settings.languages = Settings.findOne({name: "languages"}).languages;

        this.response.writeHead(200, {'Content-Type': 'application/json'});
        this.response.end(JSON.stringify(settings));
    });
