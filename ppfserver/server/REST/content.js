Router.route('/api/sections', { where: 'server' })
    .get(function () {

        let secs = [];


        let _secs = []

        _secs.push({_id: "interface", title: "interface", enable: true});
        _secs.push({_id: "photo", title: "photo", enable: true});
        _secs.push({_id: "sport", title: "sport", enable: true});
        _secs.push({_id: "text", title: "text", enable: true});
        _secs.push({_id: "theme", title: "theme", enable: true});
        _secs.push({_id: "cartcheckout", title: "cartcheckout", enable: true});

        for (let i = 0; i< _secs.length; i++) {
            let v = new ApiSection(_secs[i]);
            FillSections(v);
            secs.push(v);
        }


        this.response.writeHead(200, {'Content-Type': 'application/json'});
        this.response.end(JSON.stringify(secs));
    });

var FillSections = function(section){
    if (Sections.find({owner: section._id}).count()>0){
        let _secs = Sections.find({owner: section._id}).fetch();
        section.sections = [];
        for (let i=0; i<_secs.length; i++){
            let v = new ApiSection(_secs[i]);
            FillSections(v);
            section.sections.push(v);
        }
    }
}

function ApiSection(section) {
    this.name = section.data && section.data.name;
    this._id = section._id;
    this.position = section.position;

    // console.log(interface);

    if (section.title)
        this.title = section.title;
    else
        this.title = "";

    if (section.position)
        this.position = section.position;
    else
        this.position = 0;

    if (section.enable)
        this.enable = true;
    else
        this.enable = false;

    // if (section && section.image){
    //     this.image = Files.link(section.image);
    // }
    // else {
    //     this.image = "";
    // }

    this.content = [];
    let _cont = Items.find({owner: section._id}).fetch();

    for (let i=0; i< _cont.length; i++){
        let c = {};
        c.type = _cont[i].type;
        c._id = _cont[i]._id;
        c.position = _cont[i].position;
        c.title = _cont[i].title;
        c.enable = false;


        if (c.type === ContentTypes.VIDEO)
        {
            c.fileList = [];
            if (_cont[i] && _cont[i].data && _cont[i].data.file)
                c.fileList.push(Files.link(_cont[i].data.file));

            c.thumb = "";
            if (_cont[i] && _cont[i].data && _cont[i].data.thumb)
                c.thumb = _cont[i].data.thumb.url()

            c.title = _cont[i].title;
            c.text = _cont[i].data.text;
        }

        if (c.type === ContentTypes.IMAGE)
        {
            if (_cont[i].enable)
                c.enable=true;

            c.fileList = [];
            if (_cont[i] && _cont[i].data && _cont[i].data.file)
                c.fileList.push(Files.link(_cont[i].data.file));
            if (_cont[i] && _cont[i].data && _cont[i].data.text)
                c.text = _cont[i].data.text;
        }

        if (c.type === ContentTypes.TEXT)
        {
            c.title = _cont[i].title;



            c.render = {};
            c.text = {};

            if (_cont[i] && _cont[i].data){
                for (var key in _cont[i].data){
                    if (_cont[i].data[key].image && _cont[i].data[key].image.file)
                        c.render[key] = Files.link(_cont[i].data[key].image.file);
                    c.text[key] = _cont[i].data[key].text;
                }
            }
        }

        if (c.type === ContentTypes.PACK)
        {
            c.title = _cont[i].title;
            c.files = {};

            if (_cont[i] && _cont[i].data){
                for (var key in _cont[i].data){
                    c.files[key] = [];

                    for (let j=0;j<_cont[i].data[key].files.length; j++){
                        let file = {};
                        file.name = _cont[i].data[key].files[j].name;
                        file.link = Files.link(_cont[i].data[key].files[j]);

                        c.files[key].push(file);
                    }
                }
            }
        }

        if (c.type === ContentTypes.SETTINGSITEM)
        {
            c.title = _cont[i].title;
            c.settings = [];
            if (_cont[i] && _cont[i].data && _cont[i].data.questions) {
                c.settings = _cont[i].data.questions;
            }
        }

        this.content.push(c);
    }
}