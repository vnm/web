Router.route('/api/addguest', { where: 'server' })
    .post(function (e) {
        console.log(this.request.body);
        let g = this.request.body;
        if (g.rfid.length>6 && g.name.length>2){
            let g1 = {
                rfid: g.rfid,
                name: g.name
            };

let e = Guests.find({rfid: g1.rfid}).count();
if (e>0){
    this.response.writeHead(200, {'Content-Type': 'application/json'});
    this.response.end(JSON.stringify({status: "DOUBLE", reason: "Браслет уже зарегистрирован"}));
}
else{

    g1.zone1 = true;
    g1.drink = true;
    g1.mzone = true;
    g1.raincoat = 'waiting'; //waiting //prize //got
    g1.hoodie = 'waiting';

    let id = Guests.insert(g1);
    if (id){
        this.response.writeHead(200, {'Content-Type': 'application/json'});
        this.response.end(JSON.stringify({status: "OK", name: g1.name}));
    }
    else{
        this.response.writeHead(200, {'Content-Type': 'application/json'});
        this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка базы данных"}));

    }
}

}
else {
    this.response.writeHead(200, {'Content-Type': 'application/json'});
    this.response.end(JSON.stringify({status: "ERROR", reason: "Не правильно считана метка или слишком короткое имя"}));
}
});

Router.route('/api/guestinfo', { where: 'server' })
    .post(function (e) {
        if (this.request.body && this.request.body.rfid) {
            let rfid = this.request.body.rfid;
            if (rfid.length > 6) {
                let g = Guests.findOne({rfid: rfid});
                if (g) {
                    this.response.writeHead(200, {'Content-Type': 'application/json'});
                    this.response.end(JSON.stringify({status: "OK", guest: g}));
                }
                else{
                    this.response.writeHead(200, {'Content-Type': 'application/json'});
                    this.response.end(JSON.stringify({status: "NOTFOUND", reason: "Метка не зарегистрирована"}));
                }
            }
            else{
                this.response.writeHead(200, {'Content-Type': 'application/json'});
                this.response.end(JSON.stringify({status: "ERROR", reason: "Некорректная метка"}));
            }
        }
        else{
            this.response.writeHead(200, {'Content-Type': 'application/json'});
            this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса"}));
        }

    });

Router.route('/api/removecoctail', { where: 'server' })
    .post(function (e) {
        if (this.request.body && this.request.body.rfid) {
            let rfid = this.request.body.rfid;
            if (rfid.length > 6) {
                let g = Guests.findOne({rfid: rfid});
                if (g) {
                    if (g.drink){
                        let a = Guests.update(g._id, {$set: {drink: false}});
                        console.log(a);
                        if (a){
                            this.response.writeHead(200, {'Content-Type': 'application/json'});
                            this.response.end(JSON.stringify({status: "OK"}));
                        }
                        else{
                            this.response.writeHead(200, {'Content-Type': 'application/json'});
                            this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса"}));
                        }
                    }
                    else{
                        this.response.writeHead(200, {'Content-Type': 'application/json'});
                        this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса"}));
                    }

                }
                else{
                    this.response.writeHead(200, {'Content-Type': 'application/json'});
                    this.response.end(JSON.stringify({status: "NOTFOUND", reason: "Метка не зарегистрирована"}));
                }
            }
            else{
                this.response.writeHead(200, {'Content-Type': 'application/json'});
                this.response.end(JSON.stringify({status: "ERROR", reason: "Некорректная метка"}));
            }
        }
        else{
            this.response.writeHead(200, {'Content-Type': 'application/json'});
            this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса"}));
        }

    });

Router.route('/api/addhoodie', { where: 'server' })
    .post(function (e) {
        if (this.request.body && this.request.body.rfid) {
            let rfid = this.request.body.rfid;
            if (rfid.length > 6) {
                let g = Guests.findOne({rfid: rfid});
                if (g) {
                    if (g.hoodie==='waiting'){
                        let a = Guests.update(g._id, {$set: {hoodie: 'prize'}});
                        console.log(a);
                        if (a){
                            this.response.writeHead(200, {'Content-Type': 'application/json'});
                            this.response.end(JSON.stringify({status: "OK"}));
                        }
                        else{
                            this.response.writeHead(200, {'Content-Type': 'application/json'});
                            this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса"}));
                        }
                    }
                    else{
                        this.response.writeHead(200, {'Content-Type': 'application/json'});
                        this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса"}));
                    }

                }
                else{
                    this.response.writeHead(200, {'Content-Type': 'application/json'});
                    this.response.end(JSON.stringify({status: "NOTFOUND", reason: "Метка не зарегистрирована"}));
                }
            }
            else{
                this.response.writeHead(200, {'Content-Type': 'application/json'});
                this.response.end(JSON.stringify({status: "ERROR", reason: "Некорректная метка"}));
            }
        }
        else{
            this.response.writeHead(200, {'Content-Type': 'application/json'});
            this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса"}));
        }

    });

Router.route('/api/removehoodie', { where: 'server' })
    .post(function (e) {
        if (this.request.body && this.request.body.rfid) {
            let rfid = this.request.body.rfid;
            if (rfid.length > 6) {
                let g = Guests.findOne({rfid: rfid});
                if (g) {
                    if (g.hoodie==='prize'){
                        let a = Guests.update(g._id, {$set: {hoodie: 'got'}});
                        console.log(a);
                        if (a){
                            this.response.writeHead(200, {'Content-Type': 'application/json'});
                            this.response.end(JSON.stringify({status: "OK"}));
                        }
                        else{
                            this.response.writeHead(200, {'Content-Type': 'application/json'});
                            this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса"}));
                        }
                    }
                    else{
                        this.response.writeHead(200, {'Content-Type': 'application/json'});
                        this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса"}));
                    }

                }
                else{
                    this.response.writeHead(200, {'Content-Type': 'application/json'});
                    this.response.end(JSON.stringify({status: "NOTFOUND", reason: "Метка не зарегистрирована"}));
                }
            }
            else{
                this.response.writeHead(200, {'Content-Type': 'application/json'});
                this.response.end(JSON.stringify({status: "ERROR", reason: "Некорректная метка"}));
            }
        }
        else{
            this.response.writeHead(200, {'Content-Type': 'application/json'});
            this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса"}));
        }

    });

Router.route('/api/removeraincoat', { where: 'server' })
    .post(function (e) {
        if (this.request.body && this.request.body.rfid) {
            let rfid = this.request.body.rfid;
            if (rfid.length > 6) {
                let g = Guests.findOne({rfid: rfid});
                if (g) {
                    if (g.raincoat==='prize'){
                        let a = Guests.update(g._id, {$set: {raincoat: 'got'}});
                        console.log(a);
                        if (a){
                            this.response.writeHead(200, {'Content-Type': 'application/json'});
                            this.response.end(JSON.stringify({status: "OK"}));
                        }
                        else{
                            this.response.writeHead(200, {'Content-Type': 'application/json'});
                            this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса1"}));
                        }
                    }
                    else{
                        this.response.writeHead(200, {'Content-Type': 'application/json'});
                        this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса2"}));
                    }

                }
                else{
                    this.response.writeHead(200, {'Content-Type': 'application/json'});
                    this.response.end(JSON.stringify({status: "NOTFOUND", reason: "Метка не зарегистрирована"}));
                }
            }
            else{
                this.response.writeHead(200, {'Content-Type': 'application/json'});
                this.response.end(JSON.stringify({status: "ERROR", reason: "Некорректная метка"}));
            }
        }
        else{
            this.response.writeHead(200, {'Content-Type': 'application/json'});
            this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса3"}));
        }

    });

Router.route('/api/resetzones', { where: 'server' })
    .post(function (e) {
        if (this.request.body && this.request.body.rfid) {
            let rfid = this.request.body.rfid;
            if (rfid.length > 6) {
                let g = Guests.findOne({rfid: rfid});
                if (g) {
                    let a = Guests.update(g._id, {$set: {zone1: true, mzone: true}});
                    if (a){
                        this.response.writeHead(200, {'Content-Type': 'application/json'});
                        this.response.end(JSON.stringify({status: "OK"}));
                    }
                    else{
                        this.response.writeHead(200, {'Content-Type': 'application/json'});
                        this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса1"}));
                    }

                }
                else{
                    this.response.writeHead(200, {'Content-Type': 'application/json'});
                    this.response.end(JSON.stringify({status: "NOTFOUND", reason: "Метка не зарегистрирована"}));
                }
            }
            else{
                this.response.writeHead(200, {'Content-Type': 'application/json'});
                this.response.end(JSON.stringify({status: "ERROR", reason: "Некорректная метка"}));
            }
        }
        else{
            this.response.writeHead(200, {'Content-Type': 'application/json'});
            this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса3"}));
        }

    });

Router.route('/api/entermzone', { where: 'server' })
    .post(function (e) {

        console.log(this.request.body.nfc);

        if (this.request.body && this.request.body.nfc) {
            let rfid = this.request.body.nfc;
            if (rfid.length > 6) {
                let g = Guests.findOne({rfid: rfid});
                if (g){
                    if (g.mzone){
                        let u = {};
                        u.mzone = false;
                        if (g.raincoat === 'waiting')
                            u.raincoat = 'prize';

                        Guests.update(g._id, {$set: u});

                        let g1 = Guests.findOne(g._id);

                        this.response.writeHead(200, {'Content-Type': 'application/json'});
                        this.response.end(JSON.stringify({status: "OK", guest: g1}));
                    }
                    else{
                        this.response.writeHead(200, {'Content-Type': 'application/json'});
                        this.response.end(JSON.stringify({status: "ERROR", reason: "Гость уже посещал зону"}));
                    }
                }
                else{
                    this.response.writeHead(200, {'Content-Type': 'application/json'});
                    this.response.end(JSON.stringify({status: "ERROR", reason: "Браслет не зарегистрирован"}));
                }
            }
            else{
                this.response.writeHead(200, {'Content-Type': 'application/json'});
                this.response.end(JSON.stringify({status: "ERROR", reason: "Некорректная метка"}));
            }
        }
        else{
            this.response.writeHead(200, {'Content-Type': 'application/json'});
            this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса3"}));
        }

    });

Router.route('/api/enterzone1', { where: 'server' })
    .post(function (e) {
        if (this.request.body && this.request.body.nfc) {
            let rfid = this.request.body.nfc;
            if (rfid.length > 6) {
                let g = Guests.findOne({rfid: rfid});
                if (g){
                    if (g.zone1){
                        let u = {};
                        u.zone1 = false;

                        Guests.update(g._id, {$set: u});

                        let g1 = Guests.findOne(g._id);

                        this.response.writeHead(200, {'Content-Type': 'application/json'});
                        this.response.end(JSON.stringify({status: "OK", guest: g1}));
                    }
                    else{
                        this.response.writeHead(200, {'Content-Type': 'application/json'});
                        this.response.end(JSON.stringify({status: "ERROR", reason: "Гость уже посещал зону"}));
                    }
                }
                else{
                    this.response.writeHead(200, {'Content-Type': 'application/json'});
                    this.response.end(JSON.stringify({status: "ERROR", reason: "Браслет не зарегистрирован"}));
                }
            }
            else{
                this.response.writeHead(200, {'Content-Type': 'application/json'});
                this.response.end(JSON.stringify({status: "ERROR", reason: "Некорректная метка"}));
            }
        }
        else{
            this.response.writeHead(200, {'Content-Type': 'application/json'});
            this.response.end(JSON.stringify({status: "ERROR", reason: "Ошибка запроса3"}));
        }

    });