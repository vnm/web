Meteor.methods({
    'getRole': function(){
      let u = Meteor.users.findOne(this.userId);
      console.log(u);
    },

    //https://gist.github.com/possibilities/3443021
    'refreshClients': function(cmd){

        let _terminals = Terminals.find().fetch();

        var futures = _.map(_terminals, function(terminal){
            let future = new Future();
            var onComplete = future.resolver();

            Meteor.call('refreshClient', terminal._id, cmd, function(e,r){
                onComplete(e,r);
            });

            return future;
        });

        Future.wait(futures);
        return _.invoke(futures, 'get');

    },

    'refreshClient': function(id, cmd){
        let ip = Terminals.findOne(id).ip;

        d = {'command': cmd};
        this.unblock();
        try {
            HTTP.post("http://" + ip + ":8088/api/management", {params: d});
        }
        catch(err) {
            return {result: "error", ip: ip}
        }
        return {result: "success", ip: ip}
    },
    'removeTerminal': function(id){
        if (this.userId) {
            Terminals.remove(id);
        }
    },
    'removeLang': function(el){
        if (this.userId) {
            let a = Settings.findOne({name: "languages"});

            for (var i=0; i<a.languages.length; i++){
                if (a.languages[i].shortEngTitle === el.shortEngTitle){
                    a.languages.splice(i,1);
                    let id = a._id;
                    delete a._id;

                    Settings.update(id, a);

                    break;
                }
            }
        }
    },
    'addLang': function(l){
        if (this.userId) {
            let a = Settings.findOne({name: "languages"});
            a.languages.push(l);
            let id = a._id;
            delete a._id;

            Settings.update(id, a);
        }
    },
    setCMSdefault: function(el){
        if (this.userId) {
            let a = Settings.findOne({name: "languages"});

            for (var i=0; i<a.languages.length; i++){
                if (a.languages[i].shortEngTitle === el.shortEngTitle){
                    a.languages[i].CMSdefault = true;
                }
                else
                    a.languages[i].CMSdefault = false;
            }

            let id = a._id;
            delete a._id;

            Settings.update(id, a);
        }
    }
});