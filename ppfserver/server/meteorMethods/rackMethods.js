Meteor.methods({
    removeRackItem: function(id){
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        Rack.remove(id);
    }
})