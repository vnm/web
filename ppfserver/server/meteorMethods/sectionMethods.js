Meteor.methods({
    removeSection: function (id) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        var sections = Sections.find({owner: id}).fetch();
        for (var i = 0; i < sections.length; i++) {
            Meteor.call('removeSection', sections[i]._id);
        }

        var items = Items.find({owner: id}).fetch();
        for (var i = 0; i < items.length; i++) {
            Items.remove(items[i]._id);
        }



        Sections.remove(id);
    },
});