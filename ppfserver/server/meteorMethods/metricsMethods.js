Meteor.methods({
    calculateMetrics: function(){

        this.unblock();

        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        ////////////////////////
        //pass 1 avg values by sessionType
        //console.time("avg values by sessionType");

        let pass1 = Metrics.aggregate([
            {
                $group: {
                    _id: "$sessionType",
                    count: {$sum: 1},
                    averageDuration: {$avg: "$sessionDuration" },
                    averageDesignsCount: {$avg: "$designsCount"},
                    moneyIncome: {$sum: "$cartTotal"},
                    totalCups: {$sum: "$totalCupsCount"}
                }
            }
        ]);

        //console.timeEnd("avg values by sessionType");

        let pass1obj = {};
        for (var i =0; i<pass1.length; i++){
            pass1obj[pass1[i]._id] = {
                count: pass1[i].count,
                avgDuration: pass1[i].averageDuration,
                averageDesignsCount: pass1[i].averageDesignsCount,
                moneyIncome:pass1[i].moneyIncome,
                totalCupsCount:pass1[i].totalCups

            };
        }

        let c = 0;
        if (pass1obj.unfinished!=undefined)
            c++;
        if (pass1obj.intro!=undefined)
            c++;
        if (pass1obj.receipt!=undefined)
            c++;

        pass1obj.total = {
            count: (pass1obj.unfinished && pass1obj.unfinished.count||0) + (pass1obj.intro && pass1obj.intro.count||0) + (pass1obj.receipt && pass1obj.receipt.count||0),
            avgDuration: (pass1obj.unfinished && pass1obj.unfinished.avgDuration||0 + pass1obj.intro && pass1obj.intro.avgDuration||0 +  pass1obj.receipt && pass1obj.receipt.avgDuration||0)/c,
            avgDesigns: (pass1obj.unfinished && pass1obj.unfinished.averageDesignsCount||0 + pass1obj.intro && pass1obj.intro.averageDesignsCount||0 + pass1obj.receipt && pass1obj.receipt.averageDesignsCount||0)/c,
            totalMoney: pass1obj.unfinished && pass1obj.unfinished.moneyIncome||0 + pass1obj.intro && pass1obj.intro.moneyIncome||0 + pass1obj.receipt && pass1obj.receipt.moneyIncome||0,
            totalCupsCount: pass1obj.unfinished && pass1obj.unfinished.totalCupsCount||0 + pass1obj.intro && pass1obj.intro.totalCupsCount||0 + pass1obj.receipt && pass1obj.receipt.totalCupsCount||0,
        };

        let m = {};
        m.allSessions = {};
        m.allSessions.amount={};
        m.allSessions.duration={};

        m.allSessions.amount.total = pass1obj.total && pass1obj.total.count || 0;
        m.allSessions.amount.unfinished = pass1obj.unfinished && pass1obj.unfinished.count || 0;
        m.allSessions.amount.intro = pass1obj.intro && pass1obj.intro.count || 0;
        m.allSessions.amount.receipt = pass1obj.receipt && pass1obj.receipt.count || 0;

        m.allSessions.duration.averageAll = Meteor.call("toHMS", pass1obj.total.avgDuration);
        m.allSessions.duration.unfinished = Meteor.call("toHMS", pass1obj.unfinished && pass1obj.unfinished.avgDuration ||0);
        m.allSessions.duration.intro = Meteor.call("toHMS", pass1obj.intro && pass1obj.intro.avgDuration||0);
        m.allSessions.duration.receipt = Meteor.call("toHMS", pass1obj.receipt && pass1obj.receipt.avgDuration||0);

        m.allSessions.amount.designsPerSession = pass1obj.total && pass1obj.total.avgDesigns.toFixed(2);
        m.allSessions.amount.totalSoldCups = pass1obj.total && pass1obj.total.totalCupsCount;
        m.allSessions.amount.totalIncome = pass1obj.total && pass1obj.total.totalMoney.toFixed(2);

        return m;

    },

    toHMS: function(ms){
        let date = new Date(null);
        date.setSeconds(ms/1000); // specify value for SECONDS here
        return date.toISOString().substr(11, 8);
    },

    SessionTypesChart: function(){
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        this.unblock();
        ////////////////////////
        //pass 1 avg values by sessionType
        //console.time("sessionType chart");

        let pass1 = Metrics.aggregate([
            {
                $group: {
                    _id: "$sessionType",
                    count: {$sum: 1},
                },

            }
        ]);

        //console.timeEnd("sessionType chart");

        let m = [];

        for (var i =0; i<pass1.length; i++){
            m.push([pass1[i]._id, pass1[i].count]);
        }

        return m;
    },

    DesignsPerSession: function(){
        this.unblock();

        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }


        ////////////////////////
        //pass 1 designs per session
        //console.time("DesignsPerSession chart");

        let pass1 = Metrics.aggregate([
            {
                $group: {
                    _id: "$designsCount",
                    dps: {
                        $sum: 1
                    },
                }
            },
            {
                $sort: {
                    _id: 1
                }
            }
        ]);

        //console.timeEnd("DesignsPerSession chart");

        let m = [];

        for (var i =0; i<pass1.length; i++){
            m.push([(pass1[i]._id).toString(), pass1[i].dps]);
        }

        return m;
    },

    CompareDesignsByType: function(){
        this.unblock();

        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        ////////////////////////
        //pass 1 designs per session
        //console.time("CompareDesignsByType chart");

        let pass1 = Metrics.aggregate([
            {
                $match: {designsCount: {$gt: 0}}
            },
            {
                $unwind: "$designType"
            },
            {
                $group: {
                    _id: "$designType",
                    count: {
                        $sum: 1
                    },
                }
            }
        ]);

        //console.timeEnd("CompareDesignsByType chart");

        let m = [];

        for (var i =0; i<pass1.length; i++){
            m.push([pass1[i]._id, pass1[i].count]);
        }

        return m;
    },

    HourlySessionsPerPeriod: function(){
        this.unblock();

        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        ////////////////////////
        //pass 1 designs per session

        let d = new Date();
        d.setDate(d.getDate()-30);

        //console.time("HourlySessionsPerPeriod chart");

        let pass1 = Metrics.aggregate([
            {
                $match: {startSession: {$gte: d}}
            },
            // {
            //     $sort: {startSession: 1}
            // },
            {
                $project: {
                    "y": {"$year": "$startSession"},
                    "m": {"$month": "$startSession"},
                    "d": {"$dayOfMonth": "$startSession"},
                    "h": {"$hour": "$startSession"},
                }
            },
            {
                $group: {
                    _id: {year: "$y", month: "$m", day: "$d", hour: "$h"},
                    count: {
                        $sum: 1
                    },
                }
            },
            {
                $sort: {
                    _id: 1
                }
            },

        ]);

        let m = [];

        for (var i =0; i<pass1.length; i++){
            let d1 = new Date(Date.UTC(pass1[i]._id.year, pass1[i]._id.month-1, pass1[i]._id.day, pass1[i]._id.hour));
             m.push([d1.getTime(), pass1[i].count]);
        }
        return m;
    },

    EverySessionDuration: function(){
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        this.unblock();
        ////////////////////////
        //pass 1 designs per session

        //console.time("HourlySessionsPerPeriod chart");

        let pass1 = Metrics.aggregate([
            {
                $project: {
                    "minute": {"$minute": "$sessionDuration2"}
                }
            },
            {
                $group: {
                    _id: "$minute",
                    count: {
                        $sum: 1
                    },
                }
            },
            {
                $sort: {
                    _id: 1
                }
            }
        ]);

        //console.timeEnd("HourlySessionsPerPeriod chart");

        let m = [];

        for (var i =0; i<pass1.length; i++){
            m.push([pass1[i]._id.toString()+"-"+(pass1[i]._id+1).toString()+" min", pass1[i].count]);
        }

        return m;
    },

    CupTypes: function(){

        this.unblock();

        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }


        ////////////////////////
        //pass 1 designs per session

        //console.time("CupTypes chart");

        let pass1 = Metrics.aggregate([
            {
                $unwind: {
                    path: "$cupsCount",
                    includeArrayIndex: "arrayIndex"
                }

            },
            {
                $group: {
                    _id: "$arrayIndex",
                    count: {
                        $sum: "$cupsCount"
                    },
                }
            }
        ]);

        //console.timeEnd("CupTypes chart");

        let m = [];


        for (var i =0; i<pass1.length; i++){
            m.push([CupTypes[pass1[i]._id], pass1[i].count]);
        }


        return m;
    },

    everySessionIncome: function(){

        this.unblock();

        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        ////////////////////////
        //pass 1 designs per session

        let d = new Date();
        d.setDate(d.getDate()-1);

        //console.time("CupTypes chart");

        let pass1 = Metrics.find({startSession: {$gte: d}}, {fields: {startSession: 1, cartTotal: 1, _id: 0}}).fetch();

        //console.timeEnd("CupTypes chart");

        // console.log(pass1);

        let m = [];

        for (var i =0; i<pass1.length; i++){
            m.push([new Date(pass1[i].startSession).getTime(), pass1[i].cartTotal]);
        }

        return m;
    },

    Themes: function(){

        this.unblock();

        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        //console.time("Themes");

        let pass1 = Metrics.aggregate([
            {
                $match: {
                    sessionType: {$eq: "receipt"}
                }
            },
            {
                $unwind: "$designDescription"
            },
            {
                $match: {
                    "designDescription.type": {$eq: "theme"}
                }
            },
            {
                $group: {
                    _id: "$designDescription.theme",
                    count: {
                        $sum: 1
                    },
                }
            }
        ]);

        //console.timeEnd("Themes");

        let m = [];

        for (var i =0; i<pass1.length; i++){
            m.push([pass1[i]._id, pass1[i].count]);
        }

        return m;
    },

    Sports: function(){

        this.unblock();

        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        ///console.time("Sports");

        let pass1 = Metrics.aggregate([
            {
                $match: {
                    sessionType: {$eq: "receipt"}
                }
            },
            {
                $unwind: "$designDescription"
            },
            {
                $match: {
                    "designDescription.type": {$eq: "sport"}
                }
            },
            {
                $group: {
                    _id: "$designDescription.theme",
                    count: {
                        $sum: 1
                    },
                }
            }
        ]);

        //console.timeEnd("Sports");

        let m = [];

        for (var i =0; i<pass1.length; i++){
            m.push([pass1[i]._id, pass1[i].count]);
        }

        return m;
    },

    Languages: function(){

        this.unblock();

        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        //console.time("Languages");

        let pass1 = Metrics.aggregate([
            {
                $group: {
                    _id: "$language",
                    count: {
                        $sum: 1
                    },
                }
            }
        ]);

        //console.timeEnd("Languages");

        let m = [];

        for (var i =0; i<pass1.length; i++){
            m.push([pass1[i]._id, pass1[i].count]);
        }

        return m;
    },

});