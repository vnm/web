Meteor.methods({
    removeItem: function(id){
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        Items.remove(id);
    },
    uploadTextImage: function(b64str, item, language){
        if (item && item.data[language] && item.data[language].image && item.data[language].image.file){
            Files.remove(item.data[language].image.file)
            //todo: move it to collection update hook
        }

        let id;
        if (item && item._id)
            id = item._id;

        var base64Data = b64str.replace(/^data:image\/png;base64,/, "");
        Files.write(new Buffer(base64Data, 'base64'), {
            fileName: 'text.png',
            type: 'image/png'
        }, function(e,fileRef){
            if (fileRef) {
                item.data[language].image.file = fileRef;
                if (id) {
                    delete item._id;
                    item["title"] = item.title;
                    delete item.title;
                    Items.update(id, {$set: item});
                }
                else {
                    Items.insert(item);
                }
            }
        });
    },
})