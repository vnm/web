import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';

class MyCard extends React.Component{
	render(){
		return(
			<div className="col-lg-3 col-sm-6">
				<div className="card m-b-5">
					<div className="card-header">
						<div className="card-title">
							{this.props.title}
						</div>
					</div>
					<div className="card-body">
						{this.props.content}
					</div>
				</div>
			</div>
        );
	}
}

class HomeHeatingCard extends React.Component{
	constructor(props){
		super(props);
		this.state=props.state;
	}
	render(){
		let s1, s2;
		if (this.state.home.heating)
			s1 = <span className="label label-important m-r-5">НАГРЕВ</span>   
		else
			s1 = <span className="label label-info m-r-5">ОХЛАЖДЕНИЕ</span>  

		if (this.state.home.heating)
			s2 = <span className="label label-important m-r-5">ТЕПЛЫЙ ПОЛ</span>   
		else
			s2 = <span className="label label-info m-r-5">ТЕПЛЫЙ ПОЛ</span>  

		let content = 			
			<>
                <h1 className="semi-bold">{this.state.home.temp.toFixed(1)}°</h1>
                {s1}{s2}           
                <p className="hint-text m-t-10 m-b-0">{FromNow(this.state.home.changedAt)}</p>
          	</>
		return (
			<MyCard title="ОТОПЛЕНИЕ" content={content}/>
		);
	}
}

class WaterHeatingCard extends React.Component{
	constructor(props){
		super(props);
		this.state=props.state;
	}
	render(){
		let s1;
		if (this.state.water.heating)
			s1 = <span className="label label-important m-r-5">НАГРЕВ</span>   
		else
			s1 = <span className="label label-info m-r-5">ОХЛАЖДЕНИЕ</span>   

		let content = 			
			<>
                <h1 className="semi-bold">{this.state.water.temp.toFixed(1)}°</h1>
				{s1}
                <p className="hint-text m-t-10 m-b-0">{FromNow(this.state.water.changedAt)}</p>
          	</>
		return (
			<MyCard title="ГОРЯЧАЯ ВОДА" content={content}/>
		);
	}
}

class OutsideCard extends React.Component{
	constructor(props){
		super(props);
		this.state=props.state;
	}
	render(){
		let content = 			
			<>
                <h1 className="semi-bold">{this.state.outside.temp.toFixed(1)}°</h1>
				<p>&nbsp;</p>
                <p className="hint-text m-t-10 m-b-0">&nbsp;</p>
          	</>
		return (
			<MyCard title="УЛИЦА" content={content}/>
		);
	}
}

Number.prototype.pad = function(size) {
	var s = String(this);
	while (s.length < (size || 2)) {s = "0" + s;}
	return s;
}

function FromNow(date){
	var delta = new Date()-new Date(date);
	var days = Math.floor(delta/(1000*60*60*24))
	var hours = Math.floor((delta - days*1000*60*60*24)/(1000*60*60));
	var minutes = Math.floor((delta - days*(1000*60*60*24) - hours*(1000*60*60))/(1000*60));
	var secounds = Math.floor((delta - days*(1000*60*60*24) - hours*(1000*60*60) - minutes*(1000*60))/(1000));

	return 	days.pad(2) + "д "+
			hours.pad(2) + "ч "+
			minutes.pad(2) + "м "+
			secounds.pad(2) + "c";
}
function Render(state){
	ReactDOM.render(
		<>
		<HomeHeatingCard state={state} />
		<WaterHeatingCard state={state} />
		<OutsideCard state={state} />
		</>
		, document.getElementById('boilerRow'));
}

function startWs(){
    let ws = new WebSocket("ws://192.168.1.11:9001");
    ws.onmessage=function(e){
    	Render(JSON.parse(e.data));
	}
	ws.onclose = function(event) {
		console.log('reconnect in 5 sec');
		setTimeout(startWs, 5000); 
	  };
}

startWs();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

const requestNotificationPermission = async () => {
	const permission = await window.Notification.requestPermission();
}
const main = async () => {
	serviceWorker.register();
    const permission =  await requestNotificationPermission();
}
main();

