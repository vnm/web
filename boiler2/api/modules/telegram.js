const TOKEN = "";
const PROXY_SERVER = "http://www.freeproxy-list.ru/api/proxy?anonymity=false&token=demo";
const TELEGRAM_ADDR =  "https://api.telegram.org/";
const MAX_POLL_ERROR_COUNT = 5;
const ERROR_COOLDOWN_TIME=5*60*1000;

const request = require('request');
process.env["NTBA_FIX_319"] = 1;
const TelegramBot = require('node-telegram-bot-api');

function getProxies(){
    return new Promise((resolve, reject)=>{
        console.log("get proxy list");

        request(PROXY_SERVER, (e,r,b)=>{
            if(e) reject(e);
            else{
                let a = b.split("\n");
                resolve(a);
            } 
        })
    });
}

function checkProxySequental(proxyList){
    return new Promise((resolve, reject)=>{
        let i=0;
        _checkProxy();
        function _checkProxy(){
            if (i>proxyList.length-1){
                reject('no valid proxy found')
                return;
            }
            let p = "http://" + proxyList[i];
            console.log(i);

            console.log("proxy to check: " + p);
            request({url: TELEGRAM_ADDR, proxy: p}, (e)=>{
                if(e) {
                    i++;
                    _checkProxy();
                }
                else{
                    console.log("valid proxy: " + p);
                    resolve(p)
                }
            });
        }
    })
}

function getBestProxyParallel(proxyList){
    console.log("get proxy parralel");

    let proxyCheck = [];

    return new Promise((resolve, reject)=>{
        let pa = [];
        for (let i=0; i<proxyList.length; i++){
            let p = new Promise((resolve, reject)=>{
                let proxy = "http://"+ proxyList[i];
                // console.log('check proxy :' + proxy);
                request({url: TELEGRAM_ADDR, proxy: proxy, time: true, timeout: 5000}, (e,r)=>{
                    if(e) {
                        resolve();    
                    }
                    else{
                        let item = {proxy: proxy, time: r.timings.response};
                        proxyCheck.push(item);
                        resolve();
                    }
                });
            });
            pa.push(p);
        }

        Promise.all(pa).then(()=>{
            if (proxyCheck.length == 0){
                reject('no valid proxy')
            }
            else{
                let proxy = proxyCheck[0]

                for (let i in proxyCheck){
                    let _proxy = proxyCheck[i];
                    if (_proxy.time<proxy.time)
                        proxy = _proxy;
                }

                console.log("best proxy: " + JSON.stringify(proxy));
                resolve(proxy.proxy)
            }
        })
    })
}

function startTelegram(){
    let pollErrorCount = 0;
    let cooldownTimerId;

    getProxies()
        .then(getBestProxyParallel)
        .then(proxyAddress=>{
            console.log('start bot with proxy: ' + proxyAddress);

            bot = new TelegramBot(TOKEN, {polling: true, request:{proxy: proxyAddress}});

            bot.on('message', (msg) => {
                const chatId = msg.chat.id;
                bot.sendMessage(chatId, 'Received your message');
            });

            bot.on('polling_error', (error) => {
                console.log('bot error');
                console.log(error);  // => 'EFATAL'
                onErrorHandler();

                if (pollErrorCount>MAX_POLL_ERROR_COUNT-1){
                    console.log('error count exceeded');
                    console.log('wait 60 sec and restart bot');
                    bot.stopPolling().then((res, rej)=>{
                        setTimeout(startTelegram, 60000)
                    });
                }
            });
        })
        .catch(e=>{
            console.log('error');
            console.log(e);
            console.log('wait 60 sec and try again');
            setTimeout(startTelegram, 60000)
        });
    
    function onErrorHandler(){
        if(cooldownTimerId){
            clearTimeout(cooldownTimerId);
        }
        pollErrorCount++;
        cooldownTimerId = setTimeout(()=>{pollErrorCount = 0;}, ERROR_COOLDOWN_TIME);
    }
}


startTelegram();
