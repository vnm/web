const INTERVAL              =5000;
const RESTART_TIMEOUT       =60000;
const WATER_SENSOR_ID       ="28-0115620877ff";
const OUTSIDE_SENSOR_ID     ="28-03176218d3ff";

const setHomeTempCallBack   =require('./homeTemp').setHomeTempCallBack;
// const telegram              =require('./modules/telegram');
const ds18                  =require('./ds18b20');
const state                 =require('./stateobject');
const settings              =require('./settingsobject');
const relay                 =require('./relay');

let tempSensorIDs = [];
let ds18b20Sensors = {};

let OnBoilerLoopTick = null;

function controlBoiler(){

    console.log(state);
    console.log("relays are inverted")
    console.log(relay.GPIOState);

    if (typeof(OnBoilerLoopTick)=="function")
        OnBoilerLoopTick(state);

	let needwaterheat=state.water.heating;

    if(state.water.heating){
        if(state.water.temp>settings.water.max){
            needwaterheat = false;
        }
    }
    else{
        if(state.water.temp<settings.water.min){
            needwaterheat = true
        }
    }
	
	let needhomeheat=state.home.heating;
	
	if (state.home.heating){
        if (state.home.temp>settings.home.max){
            needhomeheat = false;
        }
    }
    else{
        if (state.home.temp<settings.home.min){
            needhomeheat = true;
        }
    }

	let waterheat = needwaterheat && settings.water.enable;
	let homeheat = needhomeheat && settings.home.enable;

    setWaterHeating(waterheat);
    setHomeHeating(homeheat);
    setMainSwitch();

    controlBoilerLoop();
}

function controlBoilerLoop(){
    setTimeout(()=>{controlBoiler();}, INTERVAL)
}

function setWaterHeating(v){
    if (state.water.heating!=v){
        relay.setWaterPump(v, function(){
            state.water.heating=v;
            state.water.changedAt=new Date();	
        })
    }
}

function setHomeHeating(v){
    if (state.home.heating!=v){
        relay.setHomePump(v, function(){
            state.home.heating=v;
            state.home.changedAt = new Date();	
        })
        relay.setFloorPump(v, function(){

        })
    }
}

function setMainSwitch(){
    relay.setMainSwitch(state.water.heating || state.home.heating, function(){});
}

function setds18b20Temperatures(v){
    //TODO: timout warning
    if (WATER_SENSOR_ID in v)
        state.water.temp = v[WATER_SENSOR_ID];
    if (OUTSIDE_SENSOR_ID in v)
        state.outside.temp = v[OUTSIDE_SENSOR_ID];
}

function setHomeTemp(temp){
    //TODO: timeout warning
    state.home.temp = temp;
}

function start(OnBoilerLoopTick_Callback){

    OnBoilerLoopTick = OnBoilerLoopTick_Callback;

    ds18.getIDs((e, IDs)=>{
        if (e){
            console.log("get temperature sensor id's error");
            console.log(`wait ${RESTART_TIMEOUT/1000} seconds and restart control part`)
            setTimeout(()=>{start();}, RESTART_TIMEOUT);
        }
        else{
            tempSensorIDs = IDs;
            ds18.startPoll(setds18b20Temperatures);
            setHomeTempCallBack(setHomeTemp);
            controlBoilerLoop();
        }
    })
}

exports.start = start;
exports.state = state;