const Loki= require('lokijs');
const SAVEINTERVAL = 60*60*1000;
const DB_NAME= 'db.json';
const COLLECTION_NAME= 'statelog';

const OnDBLoad = async function(){
    logcollection = await loadCollection(COLLECTION_NAME, db);
}

const db = new Loki(`${DB_NAME}`, {
    autoload: true,
    autoloadCallback : OnDBLoad,
    autosave: true, 
    autosaveInterval: SAVEINTERVAL});

const LOGINTERVAL = 60*1000;
let stateref = null;
let logcollection = null;

const loadCollection = function (colName, db){
    return new Promise(resolve => {
        db.loadDatabase({}, () => {
            const _collection = db.getCollection(colName) || db.addCollection(colName, {ttl: 3*365*24*60*60*1000, ttlInterval: 0});
            resolve(_collection);
        })
    });
}

const addToLog = function(){
    if (stateref!=null && logcollection!=null){
        let o = Object.assign({}, stateref);
        logcollection.insert(o);
    }
}

const start = function(_stateref){
    stateref = _stateref;

    setInterval(addToLog, LOGINTERVAL);
}

module.exports.start = start;