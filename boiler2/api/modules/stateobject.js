module.exports = {
    home: {
        temp: 100,
        heating: false,
        changedAt: new Date()
    },
    water: {
        temp: 100,
        heating: false,
        changedAt: new Date()
    },
    outside: {
        temp: null
    },
    pellets: {
        quantity: null
    }
};
