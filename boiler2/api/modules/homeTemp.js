const PORT = 80

const http = require('http')

let cb;

exports.setHomeTempCallBack =  function(homeTempCallBack){
    cb = homeTempCallBack;
}

function setHomeTemp(homeTemp){
    if (cb)
        cb(homeTemp)
}

const requestHandler = (request, response) => {
    let body = [];
    request.on('data', (chunk) => {
        body.push(chunk);
    }).on('end', () => {
        console.log('http');
        body = Buffer.concat(body).toString();
        if (request.method === "POST" && request.url === "/api/sethometemp"){
            try{
                let o = JSON.parse(body);
                let homeTemp = o.homeTemp;
                setHomeTemp(homeTemp);
            }
            catch(e){
                console.log(e);
            }
        }
        response.end('OK')
    }); 

}
const server = http.createServer(requestHandler)

server.listen(PORT, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${PORT}`)
})