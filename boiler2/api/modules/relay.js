const MAIN_PUMP     = 36;
const MAIN_SWITCH   = 40;
const WATER_PUMP    = 38;
const FLOOR_PUMP    = 37;

//https://www.npmjs.com/package/rpi-gpio
const GPIO = require('rpi-gpio');

let GPIOState = {
    mainPump:       false,
    mainSwitch:     false,
    waterPump:      false,
    floorPump:    false
}

exports.GPIOState = GPIOState;

GPIO.setup(MAIN_PUMP, GPIO.DIR_OUT, 
    function(){
        GPIO.write(MAIN_PUMP, true, function(e){
            if (e){
                console.log("GPIO WRITE ERROR (MAIN_PUMP)")
                console.log(e)
            }
            else{
                GPIOState.mainPump = true;
            }
        })
    }
);

GPIO.setup(MAIN_SWITCH, GPIO.DIR_OUT, 
    function(){
        GPIO.write(MAIN_SWITCH, true, function(e){
            if (e){
                console.log("GPIO WRITE ERROR (MAIN_SWITCH)")
                console.log(e)
            }
            else{
                GPIOState.mainSwitch = true;
            }
        })
    }
);

GPIO.setup(WATER_PUMP, GPIO.DIR_OUT, 
    function(){
        GPIO.write(WATER_PUMP, true, function(e){
            if (e){
                console.log("GPIO WRITE ERROR (WATER_PUMP)")
                console.log(e)
            }
            else{
                GPIOState.waterPump = true;
            }
        })
    }
);

GPIO.setup(FLOOR_PUMP, GPIO.DIR_OUT, 
    function(){
        GPIO.write(FLOOR_PUMP, true, function(e){
            if (e){
                console.log("GPIO WRITE ERROR (FLOOR_PUMP)")
                console.log(e)
            }
            else{
                GPIOState.floorPump = true;
            }
        })
    }
);

exports.setWaterPump = function(v, cb){
    GPIO.write(WATER_PUMP, !v, function(e){
        if (e){
            console.log(`GPIO write ${WATER_PUMP} (water pump relay) error`);
            console.log(e);
        }
        else{
            GPIOState.waterPump = !v;
            cb(v)
        }
    })
}

exports.setHomePump = function(v, cb){
    GPIO.write(MAIN_PUMP, !v, function(e){
        if (e){
            console.log(`GPIO write ${MAIN_PUMP} (home pump relay) error`);
            console.log(e);
        }
        else{
            GPIOState.mainPump = !v;
            cb(v)
        }
    })
}

exports.setMainSwitch = function(v, cb){
    GPIO.write(MAIN_SWITCH, !v, function(e){
        if (e){
            console.log(`GPIO write ${MAIN_SWITCH} (main switch relay) error`);
            console.log(e);
        }
        else{
            GPIOState.mainSwitch = !v;
            cb(v)
        }
    })
}

exports.setFloorPump = function(v, cb){
    GPIO.write(FLOOR_PUMP, !v, function(e){
        if (e){
            console.log(`GPIO write ${FLOOR_PUMP} (floor pump relay) error`);
            console.log(e);
        }
        else{
            GPIOState.floorPump = !v;
            cb(v)
        }
    })
}

