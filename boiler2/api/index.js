const ws = require ('ws');
const express = require('express');
const app = express();
const boiler = require('./modules/boiler');
const logger = require('./modules/logger');


const wss = new ws.Server({
	port: 9001
});

wss.on('connection', function(ws){
	ws.send(JSON.stringify(boiler.state));
})

function OnBoilerLoopTickHandler(state){
	wss.clients.forEach(function(client){
		client.send(JSON.stringify(state));
	})
}

boiler.start(OnBoilerLoopTickHandler);
logger.start(boiler.state);



