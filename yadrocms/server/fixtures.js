let users = [{
    name        : "admin",
    email       : "admin@email.com",
    pass        : "aaaa",
    roles       : ["admin"]
}];

_.each(users, function (user) {
    let isStored = Meteor.users.findOne({username: user.name});
    if (!isStored) {
        Accounts.createUser({
            username   : user.name,
            email      : user.email,
            password   : user.pass,
        });
    }
});

let languages = ["ru", "eng"];

if (!Settings.findOne({name: "languages"})){
       Settings.insert({name: "languages", languages: languages, currentLanguage: languages[0]});
}

if (!Settings.findOne({name: "text"})){
    Settings.insert({   name: "text",
                        title: "Рэк",
                        description: "управление контентом"});
}

if (!Settings.findOne({name: "IRBranding"})){
    Settings.insert({
        name: "IRBranding",
        enableIRLogo: false
    });
}

if (!Settings.findOne({name: "oldSettings"})){
    Settings.insert({
        name: "oldSettings",
        value: {
            "title": "Инсталляция Результаты",
            "description": "Система управления контентом",
            "languages": ["ru"],
            "media": {
                "acceptableImageTypes"  : ".jpg, .png, .jpeg, .tif, .tiff|images/*",
                "acceptableVideoTypes"  : "video/*",
                "imageMaxWidth"         : 1080,
                "imageMaxHeight"        : 1920
            },
            "fonts": {
                "google": { "families": [ "Open+Sans:700italic:latin,cyrillic", "Montserrat:400,700:latin" ] },
                "regular": [
                    {
                        "fontName" : "Proxima Nova Regular",
                        "fontPath" : "ProximaNova-Reg.otf",
                        "fontWeight" : "300"
                    }
                ]
            },
            "textContentDefaultBackground" : "#f1f1f1",
            "text": {
                "default_render_width"     : 1080,
                "default_render_padding"   : 0,
                "default_render_scale"     : 1,
                "default_render_lineheight": 20,
                "default_render_fontname"  : "Proxima Nova Regular",
                "default_render_weight"    : "300",
                "fontsize_formats"         : "14px 16px 18px 20px 24px 28px 32px 38px 46px 54px",
                "font_formats"             : "Proxima Nova Black=Proxima Nova Black;Proxima Nova Bold = Proxima Nova Bold;Proxima Nova Regular = Proxima Nova Regular;",
                "style_formats": [
                    { "title": "Заголовки", "items": [
                        {
                            "title": "Основной заголовок", "selector": "p",
                            "styles": {
                                "text-align"         : "center",
                                "font-size"          : "32px",
                                "margin"             : "0px",
                                "line-height"        : "40px",
                                "font-family"        : "Proxima Nova Regular",
                                "text-transform"     : "uppercase"
                            }
                        }
                    ]},
                    { "title": "Контент", "items": [
                        {
                            "title": "Название описания", "selector": "p",
                            "styles": {
                                "font-size"          : "24px",
                                "margin"             : "0px",
                                "line-height"        : "24px",
                                "font-family"        : "Proxima Nova Regular"
                            }
                        },
                        {
                            "title": "Основной текст", "selector": "p",
                            "styles": {
                                "font-size"          : "16px",
                                "font-family"        : "Proxima Nova Regular",
                                "margin"             : "10px 0",
                                "line-height"        : "18px"
                            }
                        }
                    ]}
                ]
            },
            "section": {
                "default_render_width"     : 540,
                "default_render_padding"   : 0,
                "default_render_lineheight": 24,
                "default_render_fontname"  : "Proxima Nova Black",
                "default_render_fontsize"  : 24,
                "default_render_weight"    : "400"
            },
            "shortcuts": {
                "reRenderText" : "L",
                "reRenderSection" : "K"
            }
        }
    });
}