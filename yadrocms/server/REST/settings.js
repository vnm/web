Router.route('/api/settings', { where: 'server' })
    .get(function () {
        let settings ={};

        settings.currentLanguage = Settings.findOne({name: "languages"}).currentLanguage;

        this.response.writeHead(200, {'Content-Type': 'application/json'});
        this.response.end(JSON.stringify(settings));
    });
