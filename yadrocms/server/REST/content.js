Router.route('/api/rack', { where: 'server' })
    .get(function () {
        let rack = Rack.find().fetch();
        let secs = [];

        for (let z=0; z<rack.length; z++) {
            let _secs = Sections.find(rack[z].device, {
                fields: {
                    _id: 1,
                    position: 1,
                    active: 1,
                    image: 1,
                    title: 1,
                    deviceHeight:1,
                    owner: 1,
                    model: 1,
                    point: 1,
                    type: 1
                }
            }).fetch();

            for (let i = 0; i< _secs.length; i++) {
                let v = new ApiSection(_secs[i]);
                FillSections(v);
                v.position = rack[z].position || 0;
                secs.push(v);
            }
        }

        this.response.writeHead(200, {'Content-Type': 'application/json'});
        this.response.end(JSON.stringify(secs));
    });

var FillSections = function(section){
    if (Sections.find({owner: section._id}).count()>0){
        let _secs = Sections.find({owner: section._id}).fetch();
        section.sections = [];
        for (let i=0; i<_secs.length; i++){
            let v = new ApiSection(_secs[i]);
            FillSections(v);
            section.sections.push(v);
        }
    }
}

function ApiSection(section) {
    this.name = section.data && section.data.name;
    this._id = section._id;
    this.position = section.position;

    // console.log(section);

    if (section.title)
        this.title = section.title;
    else
        this.title = "";

    if (section.point)
        this.point = section.point;
    else
        this.point = {x:0, y:0};

    if (section.type)
        this.type = section.type;
    else
        this.type = "rootSection";

    if (section.position)
        this.position = section.position;
    else
        this.position = 0;

    if (section.model)
        this.model = section.model;
    else
        this.model = "";

    if (section.owner === "root"){
        this.deviceHeight = section.deviceHeight;
    }

    if (section && section.image){
        this.image = Files.link(section.image);
    }
    else {
        this.image = "";
    }

    this.content = [];
    let _cont = Items.find({owner: section._id}).fetch();

    for (let i=0; i< _cont.length; i++){
        let c = {};
        c.type = _cont[i].type;
        c._id = _cont[i]._id;
        c.position = _cont[i].position;
        c.title = _cont[i].title;

        if (c.type === ContentTypes.VIDEO)
        {
            c.fileList = [];
            if (_cont[i] && _cont[i].data && _cont[i].data.file)
                c.fileList.push(Files.link(_cont[i].data.file));

            c.thumb = "";
            if (_cont[i] && _cont[i].data && _cont[i].data.thumb)
                c.thumb = _cont[i].data.thumb.url()

            c.title = _cont[i].title;
            c.text = _cont[i].data.text;
        }

        if (c.type === ContentTypes.IMAGE)
        {
            c.fileList = [];
            if (_cont[i] && _cont[i].data && _cont[i].data.file)
                c.fileList.push(Files.link(_cont[i].data.file));
            if (_cont[i] && _cont[i].data && _cont[i].data.text)
                c.text = _cont[i].data.text;
        }

        if (c.type === ContentTypes.TEXT)
        {
            c.title = _cont[i].title;

            c.render = {};
            c.text = {};

            if (_cont[i] && _cont[i].data){
                for (var key in _cont[i].data){
                    if (_cont[i].data[key].image && _cont[i].data[key].image.file)
                        c.render[key] = Files.link(_cont[i].data[key].image.file);
                    c.text[key] = _cont[i].data[key].text;
                }
            }
        }

        if (c.type === ContentTypes.PACK)
        {
            c.title = _cont[i].title;
            c.files = {};

            if (_cont[i] && _cont[i].data){
                for (var key in _cont[i].data){
                    c.files[key] = [];

                    for (let j=0;j<_cont[i].data[key].files.length; j++){
                        let file = {};
                        file.name = _cont[i].data[key].files[j].name;
                        file.link = Files.link(_cont[i].data[key].files[j]);

                        c.files[key].push(file);
                    }
                }
            }
        }

        if (c.type === ContentTypes.VIEWS)
        {
            c.fileList = [];
            if (_cont[i] && _cont[i].data){
                let f = "";
                // if (_cont[i].data.frontViewImage){
                //     f = Files.link(_cont[i].data.frontViewImage)
                // }
                // c.fileList.push(f);
                //
                // f="";
                if (_cont[i].data.rightViewImage){
                    f = Files.link(_cont[i].data.rightViewImage)
                }
                c.fileList.push(f);

                f="";
                if (_cont[i].data.leftViewImage){
                    f = Files.link(_cont[i].data.leftViewImage)
                }
                c.fileList.push(f);

                f="";
                if (_cont[i].data.backViewImage){
                    f = Files.link(_cont[i].data.backViewImage)
                }
                c.fileList.push(f);

                f="";
                if (_cont[i].data.topViewImage){
                    f = Files.link(_cont[i].data.topViewImage)
                }
                c.fileList.push(f);
            }
        }

        this.content.push(c);
    }
}