Meteor.methods({
    //https://gist.github.com/possibilities/3443021
    'refreshClients': function(cmd){

        let _terminals = Terminals.find().fetch();

        var futures = _.map(_terminals, function(terminal){
            let future = new Future();
            var onComplete = future.resolver();

            Meteor.call('refreshClient', terminal._id, cmd, function(e,r){
                onComplete(e,r);
            });

            return future;
        });

        Future.wait(futures);
        return _.invoke(futures, 'get');

    },

    'refreshClient': function(id, cmd){
        let ip = Terminals.findOne(id).ip;

        d = {'command': cmd};
        this.unblock();
        try {
            HTTP.post("http://" + ip + ":8088/api/management", {params: d});
        }
        catch(err) {
            return {result: "error", ip: ip}
        }
        return {result: "success", ip: ip}
    },
    'removeTerminal': function(id){
        if (this.userId) {
            Terminals.remove(id);
        }
    }
});