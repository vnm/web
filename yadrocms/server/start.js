Meteor.startup(function () {
    Future = Npm.require('fibers/future');

    Files.find({}).forEach((file)=> {
        // This updates the file URL using the current value of ROOT_URL
        let path = process.env.PATH_TO_CONTENT + "\\"+file._id+"."+file.extension;
        Files.update({_id: file._id}, {$set: {_storagePath: process.env.PATH_TO_CONTENT, path: path, "versions.original.path": path}});
    });
});