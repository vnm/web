Router.route('sections', {
    path: '/language/:language/sections/:owner?',
    layoutTemplate: 'mainLayout',
    waitOn: function(){
        let p = {};
        p.owner = this.params.owner;
        return [Meteor.subscribe('sections'), Meteor.subscribe('items')];
    },

    action: function(){
        if (!this.params.language){
            this.params.language = Settings.findOne({name: "languages"}).languages[0] || "ru";
        }
        if (!this.params.owner){
            this.params.owner = "root";
        }

        this.render();
    },

    data: function(){

        var data = {};
        data.params = {};
        data.params.language = this.params.language;

        data.terminals = Terminals.find();

        data.params.owner = this.params.owner?this.params.owner:"root";
        data.sections = Sections.find({owner: this.params.owner}, {sort: {position: 1}});
        data.sectionList = Sections.find({owner: this.params.owner}, {sort: {position: 1}});
        data.itemList = Items.find({owner: this.params.owner});

        if (data.sectionList.count()>0){
            data.activeZoneList = Sections.find({owner: data.sectionList.fetch()[0]._id});
        }

        if (this.params.owner !== "root"){
            data.section = Sections.findOne(this.params.owner);
        }

        data.devicesCount = Sections.find({owner: "root"}).count();

        return data;
    }
});

Router.route('createSection', {
    path: "/language/:language/sections/:owner/create",
    action: function(){
        this.params.isNew = true;
        this.redirect('editSection', this.params);
    }
});

Router.route('editSection', {
    path: '/language/:language/sections/:owner/edit/:sectionId?',
    layoutTemplate: 'mainLayout',
    yieldTemplates: {
        'sectionModal': { to: 'modal' }
    },
    template: 'sections',
    waitOn: function() {
        return [Meteor.subscribe('sections')];
    },
    data: function(){

        var data = {};

        data.params = this.params;
        data.terminals = Terminals.find();
        data.params.owner = this.params.owner?this.params.owner:"root";
        data.params.language = this.params.language;

        data.sectionList = Sections.find({owner: this.params.owner}, {sort: {position: 1}});
        data.itemList = Items.find({owner: this.params.owner});

        if (data.sectionList.count()>0){

            data.activeZoneList = Sections.find({owner: data.sectionList.fetch()[0]._id});
        }

        if (this.params.sectionId) {
            data.section = Sections.findOne(this.params.sectionId);
        }
        else {
            data.section = {};
            data.section.owner = data.params.owner;
        }

        data.devicesCount = Sections.find({owner: "root"}).count();

        return data;
    }

});