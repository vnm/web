Router.route('createFourViews', {
    path: '/language/:language/owner/:owner/createfourviews',
    action: function(){
        this.redirect('editFourViews', this.params);
    }
});

Router.route('editFourViews', {
    path: '/language/:language/owner/:owner/editfourviews/:id?',
    layoutTemplate: 'mainLayout',
    template: 'sections',
    yieldTemplates: {
        'fourViewsModal': { to: 'modal' }
    },
    waitOn: function(){
        return [Meteor.subscribe('sections'), Meteor.subscribe('items')];
    },
    data: function(){
        var data = {};
        data.terminals = Terminals.find();
        data.params = this.params;
        data.params.owner = this.params.owner?this.params.owner:"root";
        data.sections = Sections.find({owner: this.params.owner}, {sort: {position: 1}});
        data.sectionList = Sections.find({owner: this.params.owner}, {sort: {position: 1}});


        if (this.params.id) {
            data.item = Items.findOne(this.params.id);
        }
        else {
            data.item = {};
            data.item.owner = data.params.owner;
            data.item.type = "fourViews";
            data.item.data={};
        }

        if (this.params.owner !== "root"){
            data.section = Sections.findOne(this.params.owner);
        }

        return data;
    }
});