Router.route('createActiveImage', {
    path: '/language/:language/owner/:owner/createActiveImage',
    action: function(){
        this.redirect('editActiveImage', this.params);
    }
});

Router.route('editActiveImage', {
    path: '/language/:language/owner/:owner/editActiveImage/:id?',
    layoutTemplate: 'mainLayout',
    template: 'sections',
    yieldTemplates: {
        'activeImageModal': { to: 'modal' }
    },
    waitOn: function(){
        return [Meteor.subscribe('sections')];
    },
    data: function(){
        var data = {};
        data.terminals = Terminals.find();
        data.params = this.params;
        data.params.owner = this.params.owner?this.params.owner:"root";
        data.sections = Sections.find({owner: this.params.owner}, {sort: {position: 1}});
        data.sectionList = Sections.find({owner: this.params.owner}, {sort: {position: 1}});


        if (this.params.id) {
            data.section = Sections.findOne(this.params.id);
        }
        else {
            data.section = {};
            data.section.owner = data.params.owner;
            data.section.type = "activeImageSection";
        }


        return data;
    }
});