Router.route('addToRack', {
    path: 'language/:language/addToRack',
    layoutTemplate: 'mainLayout',
    template: 'rack',

    yieldTemplates: {
        'addToRackModal': { to: 'modal' }
    },
    waitOn: function(){
        return [Meteor.subscribe('rack'), Meteor.subscribe('sections')];
    },
    data: function(){
        var data = {};
        data.params = {};
        data.terminals = Terminals.find();
        data.params.language = this.params.language;

        data.devices = Sections.find({owner: "root"});

        data.devicesList = Rack.find().fetch();
        data.devicesCount = Sections.find({owner: "root"}).count();

        return data;
    }
});