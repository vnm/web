Router.route('rack', {
    path: 'language/:language?/rack',
    layoutTemplate: 'mainLayout',
    template: 'rack',

    waitOn: function(){
        return [Meteor.subscribe('rack'), Meteor.subscribe('sections')];
    },
    data: function(){
        var data = {};
        data.params = {};
        data.terminals = Terminals.find();
        data.params.language = this.params.language;

        data.devicesList = Rack.find().fetch();
        data.devicesCount = Sections.find({owner: "root"}).count();

        return data;
    }
});