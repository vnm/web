Router.route('settings', {
    path: 'language/:language/settings',
    layoutTemplate: 'mainLayout',
    template: 'settings',

    waitOn: function(){
        return [Meteor.subscribe('terminals')];
    },
    data: function(){
        var data = {};
        data.params = {};

        data.params.language = this.params.language;
        data.terminals = Terminals.find();
        // data.devicesList = Rack.find();
        // data.devicesCount = Sections.find({owner: "root"}).count();

        return data;
    }
});