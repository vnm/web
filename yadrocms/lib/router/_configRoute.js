Router.configure({
    loadingTemplate: 'loading',
    waitOn: function(){ return [Meteor.subscribe("settings"), Meteor.subscribe("terminals")]}
});

// Router.onBeforeAction('loading');

Router.onBeforeAction(function() {
    if (Meteor.isClient) {
        if (!Meteor.userId()) {
            this.redirect('login');
            if (this.ready)
                this.next();
            else
                this.render('loading');
        } else {
            if (this.ready)
                this.next();
            else
                this.render('loading');
        }
    }
    if (Meteor.isServer){
        this.next();
    }
});

// https://github.com/iron-meteor/iron-router/blob/dev/README.md#controlling-subscriptions

Router.route('home',{
    path: '/',
    action: function(){
        let l = Settings.findOne({name: "languages"}).languages[0] || "ru";
        this.redirect('rack', {language: l});
    }
});

Router.route('logout',{
    path: '/logout',
    action: function() {
        Meteor.logout(function (err) {
            if (err)
                console.log('Logout failed, reason: ' + err);
            else {
                console.log('Logout ok');
            }
        });
    }
});