TextImageDataVO = function () {
    this.file    = null;
    this.height  = 0; // Задается после рендера html в картинку
    // Параменты необходимые для генерации картинки
    this.width   = 1080;
    this.padding = 0;
    this.linehgt = 20; // Line height
    this.scale   = 1;
    this.center  = false;
    this.font    = "";
    this.weight  = 0;
}
