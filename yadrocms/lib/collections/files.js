import { FilesCollection } from 'meteor/ostrio:files';

Files = new FilesCollection({
    storagePath: process.env.PATH_TO_CONTENT,
    collectionName: 'files',
    //allowClientCode: false, // Disallow remove files from Client
    //onBeforeUpload: function (file) {
    //    // Allow upload files under 10MB, and only in png/jpg/jpeg formats
    //    if (file.size <= 10485760 && /png|jpg|jpeg/i.test(file.ext)) {
    //        return true;
    //    } else {
    //        return 'Please upload image, with size equal or less than 10MB';
    //    }
    //}
});

if (Meteor.isServer) {
    Meteor.publish('files', function () {
        return Files.find().cursor;
    });
}