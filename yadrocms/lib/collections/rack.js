Rack = new Meteor.Collection('rack');

if (Meteor.isServer){
    Rack.allow({
        'insert': function(){
            if (Meteor.userId())
                return true;
        },
        'update': function () {
            if (Meteor.userId())
                return true;
        },
        'remove': function(){
            if (Meteor.userId())
                return true;
        },
    });

    Meteor.publish('rack', function(){
        return Rack.find();
    });
}