Items = new Meteor.Collection('items');

//https://atmospherejs.com/matb33/collection-hooks
Items.before.insert(function(userId, doc){
    doc.createdAt = Date.now();
    doc.modifiedAt = Date.now();

    let owner = doc.owner;

    doc.position = Items.find({owner: owner}).count();
});

Items.before.update(function (userId, doc, fieldNames, modifier, options) {
    if (modifier.$set && modifier.$set.data){

        let newdata = modifier.$set.data;
        let olddata = doc.data;

        if (newdata.frontViewImage && newdata.frontViewImage._id != (olddata.frontViewImage && olddata.frontViewImage._id)){

            if (olddata.frontViewImage)
                Files.remove(olddata.frontViewImage)
        }
        if (newdata.backViewImage && newdata.backViewImage._id != (olddata.backViewImage && olddata.backViewImage._id)){

            if (olddata.backViewImage)
                Files.remove(olddata.backViewImage)
        }
        if (newdata.leftViewImage && newdata.leftViewImage._id != (olddata.leftViewImage && olddata.leftViewImage._id)){

            if (olddata.leftViewImage)
                Files.remove(olddata.leftViewImage)
        }
        if (newdata.rightViewImage && newdata.rightViewImage._id != (olddata.rightViewImage && olddata.rightViewImage._id)){

            if (olddata.rightViewImage)
                Files.remove(olddata.rightViewImage)
        }

        if (newdata.topViewImage && newdata.topViewImage._id != (olddata.topViewImage && olddata.topViewImage._id)){

            if (olddata.topViewImage)
                Files.remove(olddata.topViewImage)
        }

        if (newdata.file && newdata.file._id != olddata.file._id){
            Files.remove(olddata.file)
        }
    }

    if (modifier.$set) {
        for (var key in modifier.$set) {
            if (key.includes('data.file')) {
                if (doc.data && doc.data.file &&doc.data.file._id!=modifier.$set[key]&&modifier.$set[key]._id){
                    Files.remove(doc.data.file);
                }
            }
        }
    }


    if (doc.type === ContentTypes.PACK) {
        for (var key in modifier.$set) {
            if (key.includes('data') && key.includes('files')) {

                let oldFiles = key.split('.')
                    .reduce(function(object, property){
                        return object && object[property];
                    }, doc);

                if (oldFiles) {
                    oldFiles.forEach(function (item) {
                        Files.remove(item);
                    })
                }

            }
        }
    }

    if (doc.type === ContentTypes.TEXT){
        for (var key in modifier.$set) {
            if (key.includes('data') && key.includes('file')) {
                if (modifier.$set[key]==null){
                    let a = key.split(".");
                    if (doc.data[a[1]].image.file)
                        Files.remove(doc.data[a[1]].image.file);
                }
            }
        }
    }

    modifier.$set = modifier.$set || {};
    modifier.$set.modifiedAt = Date.now();
});

Items.after.remove(function(userId, doc){
    let owner = doc.owner;
    let position = doc.position;
    let type = doc.type;

    var itms = Items.find({type: type, owner: owner, position: {$gt: position}}).fetch();
    for (var i=0; i< itms.length; i++){
        Items.update(itms[i], {$set:{position: itms[i].position-1}});
        }
});

Items.before.remove(function(userId, doc){
    let data = doc.data;
    if (data && userId) {
        if (data.frontViewImage) {
            Files.remove(data.frontViewImage)
        }
        if (data.backViewImage) {
            Files.remove(data.backViewImage)
        }
        if (data.leftViewImage) {
            Files.remove(data.leftViewImage)
        }
        if (data.rightViewImage) {
            Files.remove(data.rightViewImage)
        }
        if (data.topViewImage) {
            Files.remove(data.topViewImage)
        }
        if (data.file){
            Files.remove(data.file);
        }
        if (doc.type === "pack") {
            for (let l in doc.data) {
                for (let fl in doc.data[l]){
                    doc.data[l][fl].forEach(function(file){
                        Files.remove(file);
                    })
                }
            }
        }

        if (doc.type==="text"){
            for (let l in doc.data) {
                if (doc.data[l].image.file) {
                    Files.remove(doc.data[l].image.file);
                }
            }

        }
    }
});

if (Meteor.isServer){
    Items.allow({
        'insert': function(){
            if (Meteor.userId())
                return true;
        },
        'update': function () {
            if (Meteor.userId())
                return true;
        },
        'remove': function(){
            if (Meteor.userId())
                return true;
        },
    });

    Meteor.publish('items', function(){
        return Items.find();
    });
}