Settings = new Meteor.Collection("settings");

Settings.allow({
    update: function(userId){
        if (userId)
            return true;
    }
});

if (Meteor.isServer){
    Meteor.publish("settings", function(){
        return Settings.find();
    });
}