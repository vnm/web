Sections = new Meteor.Collection('sections');

//https://atmospherejs.com/matb33/collection-hooks

Sections.before.insert(function(userId, doc){
    doc.createdAt = Date.now();
    doc.modifiedAt = Date.now();

    let owner = doc.owner;

    doc.position = Sections.find({owner: owner}).count();
});

Sections.before.update(function (userId, doc, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};
    modifier.$set.modifiedAt = Date.now();

    if (modifier && modifier.$set && modifier.$set.image){
        if (doc.image) {
            Files.remove(doc.image);
        }
    }

    if (modifier && modifier.$unset && modifier.$unset.image){
        if (doc.image) {
            Files.remove(doc.image);
        }
    }
});

Sections.before.remove(function(userId, doc){
    if (Rack.findOne({device: doc._id})){
        Rack.remove({device: doc._id});
    }

    if (userId && doc.image){
        Files.remove(doc.image);
    }
});


if (Meteor.isServer){

    Sections.allow({
        'insert': function(){
            if (Meteor.userId())
                return true;
        },
        'update': function () {
            if (Meteor.userId())
                return true;
        },
        'remove': function(){
            if (Meteor.userId())
                return true;
        },
    });

    Meteor.publish('sections', function(params){
        return Sections.find();
    });
}