
Terminals = new Meteor.Collection("terminals");

Terminals.allow({
    insert: function(userId) {
        if (userId)
            return true;
    },
    update: function(userId){
        if (userId)
            return true;
    },
    remove: function(userId){
        if (userId)
            return true;
    }
});



if (Meteor. isServer){
    Meteor.publish("terminals", function(){
            return Terminals.find();
    });
}
/**
 * Created by Alexey on 01.08.2016.
 */
