Template.removeItemModal.onRendered(function(){
    var params = this.data.params;

    $('.ui.modal')
        .modal('show')
        .modal({
            onApprove: function(){
                Meteor.call('deleteItem', params.id, function(e,r){
                    if (e)
                        console.log("delete item error" + e);
                    else
                        console.log("delete item ok");
                });
            },
            onHide: function(){
                Router.go('sections', params);
            }
        })
})