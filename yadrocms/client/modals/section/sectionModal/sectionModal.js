Template.sectionModal.helpers({
    "title": function(){
        let lang = this.params.language;
        if (this.section && this.section.title) {
            return this.section
                && this.section.title
                && this.section.title[lang];
        }
        else{
            return false;

        }

    },
});

Template.sectionModal.onRendered(function(){

    let params = this.data.params;
    let file = null;
    let section = this.data.section;
    let language = this.data.params.language;

    $('#secionForm').form({
        fields:{
            title: {
                identifier: 'title',
                rules: [{
                    type: 'minCount[1]',
                    prompt: 'укажите название'
                }
                ]
            },
            model: {
                identifier: 'model',
                rules: [{
                    type: 'minCount[1]',
                    prompt: 'укажите модель'
                }
                ]
            },
            deviceHeight: {
                identifier: 'deviceHeight',
                rules: [{
                    type   : 'integer[1..20]',
                    prompt: 'неверно указана высота'
                }
                ]
            }
        }
    });

    $('#secionForm').on('submit', function(){
        if ($('#secionForm').form('is valid')){

            section.title = {};
            section.title[language] = ($('#title').val());
            section.model = $('#model').val();
            section.deviceHeight = parseInt($('#deviceHeight').val());

            function writeSection(fileObj){

                if (section.image!== "")
                    section.image = null;


                if (fileObj) {
                    section.image = fileObj;
                }

                let query = {};
                query.$set = {};

                query.$set["title."+language] = section.title[language];
                query.$set["model"] = section.model;
                query.$set["deviceHeight"] = section.deviceHeight;

                if (section.image)
                    query.$set["image"] = section.image;
                if (section.image === "") {
                    query.$unset = {};
                    query.$unset["image"] = 1;
                }

                let id = section._id;


                if (section._id) {
                    Sections.update(id, query, function(e,r){
                        if (!e){
                            toastr.success(section.title[language] + " успешно обновлен", "Успех");
                            $('#sectionModal').modal('hide');
                        }
                        else{
                            toastr.error("Не удалось обновить " + section.title[language], "Ошибка");
                        }
                    });
                }
                else {
                    Sections.insert(section, function(e,r){
                        if (!e){
                            toastr.success(section.title[language] + " успешно создан", "Успех");
                            $('#sectionModal').modal('hide');
                        }
                        else{
                            toastr.error("Не удалось создать " + section.title[language], "Ошибка");
                        }
                    });
                }

            }

            if (file) {
                var upload = Files.insert({
                    file: file,
                    streams: 'dynamic',
                    chunkSize: 'dynamic'
                }, true);

                upload.on('end', function (error, fileObj) {
                    if (error) {
                        alert('Error during upload: ' + error);
                    } else {
                        writeSection(fileObj);
                    }
                });
            }
            else
                writeSection();
        }
    });

    $('#sectionModal')
        .modal({
            onVisible: function () {
                $("#sectionModal").modal("refresh");
            },
            onApprove: function(e){
                $('#secionForm').submit();
                return false;
            },
            onHide: function(){
                Router.go('sections', params);
            },
        })
        .modal('show');

    $('.ui.large.image').on('click',function(e){
        $('#iconImageInput').click();
    });

    $('#removeIcon').on('click',function(e){
        e.preventDefault();
        section.image="";
        let img = $('.ui.centered.medium.image')[0];
        img.src = "/assets/images/image.png";

        img.onload=function(){
            $("#sectionModal").modal("refresh");
        }

    });

    $('#iconImageInput').on('change', function(e){
        var img = $('.ui.centered.medium.image')[0];

        file = e.target.files[0];
        var fileURL = URL.createObjectURL(file);
        img.src = fileURL;

        img.onload=function(){
            $("#sectionModal").modal("refresh");
        }
    });
});