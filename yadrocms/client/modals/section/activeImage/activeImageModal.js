Template.activeImageModal.onRendered(function(){
    let params = this.data.params;
    let file = null;
    let section = this.data.section;
    let language = this.data.params.language;

    $('.ui.long.modal')
        .modal({
            onApprove: function(){
                function writeSection(fileObj){
                    section.image = null;

                    if (fileObj) {
                        section.image = fileObj;
                    }

                    let query = {};
                    query.$set = {};

                    if (section.image)
                        query.$set["image"] = section.image;

                    let id = section._id;

                    if (section._id)
                        Sections.update(id, query);
                    else {
                        Sections.insert(section);
                    }

                }

                if (file) {

                    var upload = Files.insert({
                        file: file,
                        streams: 'dynamic',
                        chunkSize: 'dynamic'
                    }, true);

                    upload.on('end', function (error, fileObj) {
                        if (error) {
                            alert('Error during upload: ' + error);
                        } else {
                            writeSection(fileObj);
                        }
                    });
                }
                else
                    writeSection();
            },
            onHide: function(){
                Router.go('sections', params);
            }
        })
        .modal('show');

    $('.ui.large.image').on('click',function(e){
        $('#activeImageInput').click();
    });

    $('#activeImageInput').on('change', function(e){
        var imageContainer = $('.ui.large.image');
        var img = $('.ui.centered.medium.image')[0];

        file = e.target.files[0];
        var fileURL = URL.createObjectURL(file);
        img.src = fileURL;

        img.onload=function(){
            imageContainer.empty().append(img);
        }
    })
});