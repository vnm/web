Template.fourViewsModal.onRendered(function(){

    let params = this.data.params;

    let forntViewfile = null;
    let backViewfile = null;
    let leftViewfile = null;
    let rightViewfile = null;
    let topViewFile = null;

    let item = this.data.item;
    // item.data = {};

    $('#fourViewsModal')
        .modal({
            onVisible: function () {
                $("#fourViewsModal").modal("refresh");
            },
            onApprove: function(){
                let counter = 0;

                function checkAll(){
                    if (counter === 5){
                        if (item._id){
                            let id = item._id;
                            delete item._id;
                            Items.update(id, {$set: {data: item.data}});
                        }
                        else{
                            Items.insert(item);
                        }
                    }
                }

                if (forntViewfile){
                    var upload = Files.insert({
                        file: forntViewfile,
                        streams: 'dynamic',
                        chunkSize: 'dynamic'
                    }, true);
                    upload.on('end', function(error, fileObj){
                        counter++;
                        item.data.frontViewImage = fileObj;
                        checkAll();
                    })
                }
                else
                    counter++;

                if (backViewfile){
                    var upload = Files.insert({
                        file: backViewfile,
                        streams: 'dynamic',
                        chunkSize: 'dynamic'
                    }, true);
                    upload.on('end', function(error, fileObj){
                        counter++;
                        item.data.backViewImage = fileObj;
                        checkAll();
                    })
                }
                else
                    counter++;

                if (leftViewfile){
                    var upload = Files.insert({
                        file: leftViewfile,
                        streams: 'dynamic',
                        chunkSize: 'dynamic'
                    }, true);
                    upload.on('end', function(error, fileObj){
                        counter++;
                        item.data.leftViewImage = fileObj;
                        checkAll();
                    })
                }
                else
                    counter++;

                if (rightViewfile){
                    var upload = Files.insert({
                        file: rightViewfile,
                        streams: 'dynamic',
                        chunkSize: 'dynamic'
                    }, true);
                    upload.on('end', function(error, fileObj){
                        counter++;
                        item.data.rightViewImage = fileObj;
                        checkAll();
                    })
                }
                else counter++;

                if (topViewFile){
                    var upload = Files.insert({
                        file: topViewFile,
                        streams: 'dynamic',
                        chunkSize: 'dynamic'
                    }, true);
                    upload.on('end', function(error, fileObj){
                        counter++;
                        item.data.topViewImage = fileObj;
                        checkAll();
                    })
                }
                else counter++;
            },
            onHide: function(){
                Router.go('sections', params);
            }
        })
        .modal('show');

    $('#frontViewImage').on('click',function(e){
        $('#frontViewImageInput').click();
    });

    $('#frontViewImageInput').on('change', function(e){

        var img = $('#frontViewImage')[0];

        forntViewfile = e.target.files[0];

        var fileURL = URL.createObjectURL(forntViewfile);

        img.src = fileURL;
    });


    $('#backViewImage').on('click',function(e){
        $('#backViewImageInput').click();
    });

    $('#backViewImageInput').on('change', function(e){

        var img = $('#backViewImage')[0];

        backViewfile = e.target.files[0];

        var fileURL = URL.createObjectURL(backViewfile);

        img.src = fileURL;
    });

    $('#leftViewImage').on('click',function(e){
        $('#leftViewImageInput').click();
    });

    $('#leftViewImageInput').on('change', function(e){

        var img = $('#leftViewImage')[0];

        leftViewfile = e.target.files[0];

        var fileURL = URL.createObjectURL(leftViewfile);

        img.src = fileURL;
    });

    $('#rightViewImage').on('click',function(e){
        $('#rightViewImageInput').click();
    });

    $('#rightViewImageInput').on('change', function(e){

        var img = $('#rightViewImage')[0];

        rightViewfile = e.target.files[0];

        var fileURL = URL.createObjectURL(rightViewfile);

        img.src = fileURL;
    });

    $('#topViewImage').on('click',function(e){
        $('#topViewImageInput').click();
    });

    $('#topViewImageInput').on('change', function(e){

        var img = $('#topViewImage')[0];

        topViewFile = e.target.files[0];

        var fileURL = URL.createObjectURL(topViewFile);

        img.src = fileURL;
    });


});