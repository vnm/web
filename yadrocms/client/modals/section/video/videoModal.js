Template.videoModal.onRendered(function(){
    let params = this.data.params;
    let file = null;
    let item = this.data.item;
    let language = this.data.params.language;

    $('#videoModal')
        .modal('show')
        .modal({
            onApprove: function(){
                item.title = {};
                item.title[language]=$('#title').val();
                item.data.text = {};
                item.data.text[language] = $('#text').val();

                function saveImageItem(fileObj){
                    if (item._id){
                        if (fileObj)
                            item.data.file=fileObj;

                        let query = {};
                        query.$set = {};

                        query.$set["title."+language] = item.title[language];
                        query.$set["data.text."+language] = item.data.text[language];

                        if (item.data.file){
                            query.$set["data.file"] = item.data.file;
                        }

                        let id = item._id;
                        Items.update(id, query);
                    }
                    else{
                        item.data.file = fileObj;
                        Items.insert(item);

                    }
                }

                if (file){
                    toastr.options = {"timeOut": "999999999"};
                    let t = toastr.warning("видео загружается");
                    toastr.options = {"timeOut": "5000"};

                    var upload = Files.insert({
                        file: file,
                        streams: 'dynamic',
                        chunkSize: 'dynamic'
                    }, true);
                    upload.on('end', function(error, fileObj){
                        toastr.clear(t);
                        toastr.success('Видео загружено', 'Успех!');
                        t=null;
                        saveImageItem(fileObj);
                    })
                }
                else {
                    saveImageItem(null);
                }

            },
            onHide: function(){
                Router.go('sections', params);
            }
        });

    $('.ui.video').on('click',function(e){
        $('#videoSelect').click();
    });

    $('#videoSelect').on('change', function(e){
        var video = document.createElement('video');
        video.controls = true;

        var videoContainer = $('.ui.video');
        file = e.target.files[0];
        var fileURL = URL.createObjectURL(file);

        video.src = fileURL;
        video.load();

        video.onloadedmetadata=function(){
            var prop = video.videoHeight / video.videoWidth;
            videoContainer.css({
                height : videoContainer.width() * prop
                ,  "background-image":"none"
                ,  "background-color":"white"
            });
            videoContainer.empty().append(video);
            $('#videoModal')
                .modal('refresh')
        }
    })
});