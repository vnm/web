Template.packModal.helpers({
    title: function(){
        let lang = this.params.language;
        return this.item && this.item.title && this.item.title[lang];
    },

    files: function(){
        let lang = this.params.language;
        return this.item && this.item.data && this.item.data[lang] && this.item.data[lang].files;
    }
});

Template.packModal.onRendered(function(){
    let params = this.data.params;
    let files = null;
    let item = this.data.item;
    let language = this.data.params.language;

    $('#packModal')
        .modal({
            onVisible: function () {
                $("#packModal").modal("refresh");
            }
        })
        .modal('show')
        .modal({
            onApprove: function(){
                let counter = 0;
                let fileObjects = [];

                item.title[language] = $('#title').val();

                function checkAll(){
                    if (counter == files.length){

                        item.data[language].files = fileObjects;

                        if (item._id){

                            let query = {};
                            query.$set = {};

                            query.$set["title."+language] = item.title[language];

                            if (item.data[language].files){
                                query.$set["data."+language+".files"] = item.data[language].files;
                            }

                            let id = item._id;

                            Items.update(id, query);
                        }
                        else{
                            Items.insert(item);
                        }
                    }
                }

                if (files){
                    for (var i=0; i< files.length; i++) {
                        let upload = Files.insert({
                            file: files[i],
                            streams: 'dynamic',
                            chunkSize: 'dynamic'
                        }, true);
                        upload.on('end', function (error, fileObj) {
                            fileObjects.push(fileObj);
                            counter++;
                            checkAll();
                        })
                    }
                }
                else{
                    if (item._id){

                        let query = {};
                        query.$set = {};

                        query.$set["title."+language] = item.title[language];

                        let id = item._id;
                        delete item.id;

                        Items.update(id, query);
                    }
                    else{
                        Items.insert(item);
                    }
                }
            },
            onHide: function(){
                Router.go('sections', params);
            }
        });

    $('#selectImages').on('click',function(){
        $('#multipleInput').click();
    });

    $('#multipleInput').on('change', function(e){

        let imageContainer = $('.packofmedia');
        imageContainer.empty();

        for (var i=0; i<e.target.files.length; i++){

            let file = e.target.files[i];

            let c = jQuery('<div/>', {
                class: "column"
            }).appendTo(imageContainer);

            jQuery('<div/>', {
                class: "packimage",
                style: "background-image:url('"+  URL.createObjectURL(file) + "')"
            }).appendTo(c);
        }

        files = e.target.files;
        $("#packModal").modal("refresh");

    })
});