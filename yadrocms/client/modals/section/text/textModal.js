Template.textModal.helpers({
    isInRole: function(role){
        return true;
    },
    t_item: function(){
        let language = this.params.language;
        let a = Object.assign({}, this.item);
        a.data = a.data && a.data[language];
        return a
    },
    title: function(){
        let language = this.params.language;
        return this.item && this.item.title && this.item.title[language] || "заголовок не задан";
    }
});

Template.textModal.onRendered(function(){
    let cmsSettings = this.data.cmsSettings;

    let params = this.data.params;
    let language = this.data.params.language;

    let createImage;

    let item = this.data.item;
    let textImageData = item.data[language].image;
    let textData = item.data[language];

    let textSettings  = cmsSettings.text;

    $('#textModal')
        .modal({
            onVisible: function () {
                $("#textModal").modal("refresh");
            },
            onApprove: function() {
                let oldHtml = textData.html;
                let renderEnable = true;

                collectData();

                if (textData.html === oldHtml)
                    renderEnable = false;

                if (textImageData.file === null && createImage)
                    renderEnable = true;


                renderEnable = renderEnable && createImage;


                if (renderEnable) {
                    renderText(item, textSettings, language, function (image) {
                        Meteor.call('uploadTextImage', image, item, language)
                    });
                }
                else {
                    if (item.data[language].image.file !== null && !createImage) {
                        // Files.remove(item.data[language].image.file);
                        // item.data[language].image.file = null;
                        item["data."+language+".image.file"] = null;
                        item["data."+language+".image.height"] = item.data[language].image.height;
                        item["data."+language+".image.width"] = item.data[language].image.width;
                        item["data."+language+".image.padding"] = item.data[language].image.padding;
                        item["data."+language+".image.linehgt"] = item.data[language].image.linehgt;
                        item["data."+language+".image.scale"] = item.data[language].image.scale;
                        item["data."+language+".image.center"] = item.data[language].image.center;
                        item["data."+language+".image.font"] = item.data[language].image.font;
                        item["data."+language+".image.weight"] = item.data[language].image.weight;

                        delete item.data;
                    }

                    if (item._id) {
                        delete item._id;
                        item["title." + language] = item.title[language];
                        delete item.title;
                        Items.update(params.id, {$set: item});
                    }
                    else {
                        Items.insert(item);
                    }
                }
            },
            onHide: function(){
                tinymce.remove();
                Router.go('sections', params);
            }
        })
        .modal('show');

    let LineHeightChanged = function() {
        let lh = textImageData.linehgt;
        if (lh && lh > 0) {
            let dom = tinyMCE.activeEditor.dom;
            dom.setStyle(
                dom.select('p'),
                "line-height",
                (lh + "px")
            );
        }
    };

    let PaddingChanged = function() {
        let pd = textImageData.padding;
        let body = tinymce.activeEditor.getBody();
        $(body).css("padding", pd + "px");
    }

    let collectData = function(){
        item.title = {};
        item.title[language] = $("#title").val();

        textData.text      = tinyMCE.activeEditor.getContent({format : 'text'});
        textData.title     = $("#title").val();
        textData.html      = tinyMCE.activeEditor.getContent();

        textImageData.width = $("#renderWidth").val();
        textImageData.padding = $("#renderPadding").val();
        textImageData.scale = $("#renderScale").val();
        textImageData.linehgt = $("#renderLinehgt").val();
    }

    let isApplyWithToContainer;

    let WidthToContainer = function (value) {
        let activeEditor = tinymce.activeEditor;
        if(activeEditor) {
            let body = $(activeEditor.getBody());
            let bwidth = "";
            if(value) {
                bwidth = $("#renderWidth").val();
            }
            body.css("width", bwidth);
        }
        isApplyWithToContainer = value;
    }

    let WidthChanged = function () {
        let wd = $("#renderWidth").val();
        if(isApplyWithToContainer && wd && wd > 0) {
            let body = tinymce.activeEditor.getBody();
            $(body).css("width", wd);
        }
    }

    tinymce.init({
        selector: 'textarea',

        init_instance_callback: function (editor) {
            LineHeightChanged();
            PaddingChanged();
            $(editor.getBody()).css({
                "font-family": textSettings.default_render_fontname
                ,  "font-weight": textSettings.default_render_weight
                ,  "background": cmsSettings.textContentDefaultBackground
                ,  "margin" : 0
            });

            let fonts = cmsSettings.public.cms.fonts;
            let head = $(editor.getBody());
            _.forEach(fonts.regular, function (font) {

                head.append("<style type=\"text/css\">" +
                    "@font-face {\n" +
                    "font-family: \""+ font.fontName +"\";\n" +
                    "src: local('☺'), url('/fonts/"+ font.fontPath +"');\n" +
                    "font-weight: \"" + font.fontWeight + "\";\n" +
                    "}</style>");
            });
        },
        height      : 200,
        menubar     : false,  // removes the menubar
        statusbar   : true,
        plugins     : ['textcolor colorpicker advlist lists code'],
        toolbar1     : 'newdocument removeformat | insertfile undo redo | bold italic underline | forecolor backcolor | alignleft aligncenter alignright alignjustify | code',
        toolbar2    : 'styleselect formatselect fontselect fontsizeselect | bullist numlist outdent indent',
        fontsize_formats: textSettings.fontsize_formats,
        font_formats: textSettings.font_formats,
        style_formats: textSettings.style_formats,
        content_css : ['/merged-stylesheets.css', "/styles/tinymce.editor.css"],
    });

    let PreviewText = function (tabPath) {

        if(tabPath == "preview") {
            let activeTab = $(this);
            let container = activeTab.find(".preview");
            container.empty();

            collectData();

            renderText(item, textSettings, language, function(image){
                let previewImage = new Image();
                previewImage.onload = function () {
                    Utils.CenterImageInContainer(previewImage, container);
                }
                previewImage.src = image;
            });
        }
        else if(tabPath == "edit") {
            // console.log("edit tab");
            // return;
           // $("#textModal").marginOffsetToCenter();

            // $('#textModal')
            //     .modal({
            //         onVisible: function () {
            //             $("#textModal").modal("refresh");
            //         }
            //     });
        }
    }


    $('.item').tab({
        cache: false,
        alwaysRefresh: false,
        onLoad: PreviewText
    });

    // $(".modal").marginOffsetToCenter();


    tinymce.activeEditor.setContent(item.data[language].html || "");

    $(".ui.toggle").checkbox({
        onChecked: function(){
            createImage = true;
        },
        onUnchecked: function(){
            createImage = false;
        }
    });



    if (this.data && this.data.item && this.data.item.data && this.data.item.data[language].image && this.data.item.data[language].image.file) {
        $(".ui.toggle").checkbox("check");
    }

    $('#renderPadding').change(function(){
        textImageData.padding = $("#renderPadding").val();
        PaddingChanged();
    });

    $('#renderLinehgt').change(function(){
        textImageData.linehgt = $("#renderLinehgt").val();
        LineHeightChanged();
    });

    $('#applywidth').checkbox({
        onChecked: function() {
            WidthToContainer(true)
        },
        onUnchecked: function(){
            WidthToContainer(false)
        }
    });

    $('#renderWidth').change(function(){
        WidthChanged();
    });

});
