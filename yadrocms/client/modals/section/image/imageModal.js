Template.imageModal.helpers({
    title: function(){
        let lang = this.params.language;
        return this.item && this.item.title && this.item.title[lang];
    },
    text: function(){
        let lang = this.params.language;
        return this.item && this.item.data && this.item.data.text && this.item.data.text[lang];
    }
})
Template.imageModal.onRendered(function(){
    let params = this.data.params;
    let file = null;
    let item = this.data.item;
    let language = this.data.params.language;

    $('#imageModal')
        .modal({
            onVisible: function () {
                $("#imageModal").modal("refresh");
            }
        })
        .modal('show')
        .modal({
            onApprove: function(){

                item.title = {};
                item.title[language] = $('#title').val();
                item.data.text = {};
                item.data.text[language]= $('#text').val();

                function saveImageItem(fileObj){
                    if (item._id){
                        if (fileObj)
                            item.data.file=fileObj;

                        let query = {};
                        query.$set = {};

                        query.$set["title."+language] = item.title[language];
                        query.$set["data.text."+language] = item.data.text[language];

                        if (item.data.file){
                            query.$set["data.file"] = item.data.file;
                        }

                        let id = item._id;
                        Items.update(id, query);
                    }
                    else{
                        item.data.file = fileObj;
                        Items.insert(item);

                    }
                }

                if (file){
                    let upload = Files.insert({
                        file: file,
                        streams: 'dynamic',
                        chunkSize: 'dynamic'
                    }, true);
                    upload.on('end', function(error, fileObj){
                        saveImageItem(fileObj);
                    })
                }
                else {
                    saveImageItem(null);
                }
            },
            onHide: function(){
                Router.go('sections', params);
            }
        });

    $('.ui.large.image').on('click',function(){
        $('#fileInput').click();
    });

    $('#fileInput').on('change', function(e){
        let imageContainer = $('.ui.large.image');
        let img = new Image();

        file = e.target.files[0];
        img.src = URL.createObjectURL(file);

        img.onload=function(){
            // let prop = this.height / this.width;
            //imageContainer.css("height", imageC
            //ontainer.width() * prop);
            imageContainer.empty().append(img);
            $('#imageModal').modal('refresh');
        }
    })
});