Template.deviceItem.helpers({
    title: function() {
        let language = Template.parentData(1).params.language;
        return this.title && this.title[language] + " " +this.model;
    }
});

Template.deviceItem.events({
    //add your events here
});

Template.deviceItem.onCreated(function () {
    //add your statement here
});

Template.deviceItem.onRendered(function () {
    //add your statement here
});

Template.deviceItem.onDestroyed(function () {
    //add your statement here
});

