Template.addToRackModal.onRendered(function(){
    let params = this.data.params;
    let language = this.data.params.language;

    $.fn.form.settings.rules.position = function(value, position) {
        if (value) {
            let p = parseInt(value);
            if (p<0)
                return false;

            let device = $('#select').val();
            if (device) {
                let h = Sections.findOne(device).deviceHeight*2;
                let list = Rack.find().fetch();

                if (p+Sections.findOne(device).deviceHeight*2>rackHeight*2)
                    return false;

                for(let i=0; i<list.length; i++){
                    let dh = Sections.findOne(list[i].device).deviceHeight*2;
                    if (list[i].position>=p && list[i].position<p+h ||
                        p>=list[i].position && p<list[i].position +dh){
                        return false;
                    }
                }
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    };

    $('#rackForm').form({
        fields:{
            devices: {
                identifier: 'devices',
                rules: [{
                    type: 'empty',
                    prompt: 'укажите прибор'
                }
                ]
            },
            devicePosition: {
                identifier: 'devicePosition',
                rules: [{
                    type: 'position',
                    prompt: 'укажите позицию'
                }
                ]
            }
        }
    });

    $('#rackForm').on('submit', function() {
        if ($('#rackForm').form('is valid')) {
            let device = $('#select').val();
            let position = parseInt($('#devicePosition').val());

            Rack.insert({device: device, position: position}, function(e,r){
                if (!e){
                    toastr.success(Sections.findOne(device).title[language] + " добавлен в рэк.", "Успех");
                    $('#addToRackModal').modal('hide');
                }
                else{
                    toastr.error("Не удалось добавить прибор", "Ошибка");
                }
            });
        }
    });

    $('#addToRackModal')
        .modal({
            onApprove: function (e) {
                $('#rackForm').submit();
                return false;
            },
            onHide: function () {
                $('#devicePosition').val("");
                Router.go('rack', params);
            }
        });

    this.autorun(function(){
        if(Router.current().route.getName() === "addToRack") {
            $('#addToRackModal')
                .modal('show');
        }
    });


});