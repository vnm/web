/**
 * Created by Alexey on 08.08.2016.
 */
renderText = function(item, textSettings, language, callback){

    let textImageData = item.data[language].image;
    let html = item.data[language].html;

    let htmlContainer = $('<div>');

    let scaleIsInPixels = isNaN(textImageData.scale);

    let scale       = !scaleIsInPixels ? textImageData.scale : 1;
    let width       = textImageData.width * scale;
    let padding     = (textImageData.padding * scale) + "px";
    let lineHeight  = (textImageData.linehgt * scale) + "px";
    let font        = textSettings.default_render_fontname;
    let weight      = textSettings.default_render_weight;
    let center      = textImageData.center;

    let style = {
        "padding"      : padding,
        "width"        : width > 0 ? (width + "px") : "",
        "font-family"  : font,
        "font-size"    : scaleIsInPixels ? textImageData.scale : "1em",
        "font-weight"  : weight,
        "line-height"  : lineHeight,
        "text-align"   : center ? "center" : "left",
        "position"     : "static",
        "float"        : "left"
    };

    htmlContainer
        .css(style)
        .html(html)
        .appendTo(document.body);

    html2canvas(htmlContainer, {
        onrendered: function(canvas) {
            htmlContainer.remove();
            let imageData = canvas.toDataURL("image/png", 1);
            callback(imageData);
        }
    });
}