Template.registerHelper("timeToHuman", function(timeStamp){
    return moment(timeStamp).format("DD.MM.YYYY HH:mm:ss");
})