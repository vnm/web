Template.topMenu.helpers({
    active: function(arg){
        let routeName=Router.current().route.getName();
        if (arg === routeName)
            return "active";
        else
            return "";
    },
    enableIRLogo: function() {
        return Settings.findOne({name: "IRBranding"}) &&
            Settings.findOne({name: "IRBranding"}).enableIRLogo;
    },
    language: function(){
        return this.params.language;
    },
    languages: function(){
        return Settings.findOne({name: "languages"})
                && Settings.findOne({name: "languages"}).languages;
    }

});

Template.topMenu.events({
    'click  #refreshAllClients': function(e,t){
        e.preventDefault();
        Meteor.call('refreshClients', 'refreshAll', function(e,res){
            if (res) {
                res.forEach(function(r){
                    if (r.result === "error") {
                        toastr.error("Не удалось обновить " + r.ip, "Ошибка");
                    }
                    if (r.result === "success") {
                        toastr.success(r.ip + " успешно обновлен", "Успех");
                    }
                })
            }
        });
    }
});