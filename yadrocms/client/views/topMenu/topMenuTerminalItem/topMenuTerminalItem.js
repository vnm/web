Template.topMenuTerminalItem.helpers({
    //add you helpers here
});

Template.topMenuTerminalItem.events({
    "click a": function(e,t){
        e.preventDefault();

        Meteor.call('refreshClient', this._id, 'refreshAll', function(e,r){
            if (r) {
                if (r.result === "error") {
                    toastr.error("Не удалось обновить " + r.ip, "Ошибка");
                }
                if (r.result === "success") {
                    toastr.success(r.ip + " успешно обновлен", "Успех");
                }
            }
        });
    }
});