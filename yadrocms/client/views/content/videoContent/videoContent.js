Template.videoContent.helpers({
    title: function(){
        let lang = Template.parentData(1).params.language;
        return this.title[lang] || "название не задано";
    },
    text: function(){
        let lang = Template.parentData(1).params.language;
        return this.data.text[lang] || "текст отсутствует";
    }
})