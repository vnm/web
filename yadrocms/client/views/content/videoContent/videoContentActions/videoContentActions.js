Template.videoContentActions.helpers({
    //add you helpers here
});

Template.videoContentActions.events({
    "click #editFourViewsActions": function(e,t){
        let p = Template.parentData(1).params;
        p.id = this._id;
        Router.go("editVideo", p);
    },
    "click #removeFourViewsActions": function(e,t){
        Meteor.call('removeItem', this._id);
    }
});

Template.videoContentActions.onCreated(function () {
    //add your statement here
});

Template.videoContentActions.onRendered(function () {
    $('.dropdown').dropdown(
        {
            on          : 'hover'
            ,  action      : "hide"
            ,  allowTab    : false
            ,  transition  : 'fade down'
        });
});

Template.videoContentActions.onDestroyed(function () {
    //add your statement here
});

