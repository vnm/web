Template.packContent.helpers({
   title: function(){
      let lang = Template.parentData(1).params.language;
      return this.title[lang] || "Заголовок не задан"
   },

    files: function(){
        let lang = Template.parentData(1).params.language;
        return this.data && this.data[lang] && this.data[lang].files;
    }
});