Template.packContentActions.helpers({
    //add you helpers here
});

Template.packContentActions.events({
    "click #editPackActions": function(e,t){
        let p = Template.parentData(1).params;
        p.id = this._id;
        Router.go("editPack", p);
    },
    "click #removePackActions": function(e,t){
        Meteor.call('removeItem', this._id);
    }
});

Template.packContentActions.onCreated(function () {
    //add your statement here
});

Template.packContentActions.onRendered(function () {
    $('.dropdown').dropdown(
        {
            on          : 'hover'
            ,  action      : "hide"
            ,  allowTab    : false
            ,  transition  : 'fade down'
        });
});

Template.packContentActions.onDestroyed(function () {
    //add your statement here
});

