Template.imageContentActions.helpers({
    //add you helpers here
});

Template.imageContentActions.events({
    "click #editFourViewsActions": function(e,t){
        let p = Template.parentData(1).params;
        p.id = this._id;
        Router.go("editImage", p);
    },
    "click #removeFourViewsActions": function(e,t){
        Meteor.call('removeItem', this._id);
    }
});

Template.imageContentActions.onCreated(function () {
    //add your statement here
});

Template.imageContentActions.onRendered(function () {
    $('.dropdown').dropdown(
        {
            on          : 'hover'
            ,  action      : "hide"
            ,  allowTab    : false
            ,  transition  : 'fade down'
        });
});

Template.imageContentActions.onDestroyed(function () {
    //add your statement here
});

