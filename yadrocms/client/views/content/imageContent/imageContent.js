Template.imageContent.helpers({
    title: function(){
        let lang = Template.parentData(1).params.language;
        return this.title && this.title[lang] || "название не задано"
    },
    text: function(){
        let lang = Template.parentData(1).params.language;
        return this.data && this.data.text && this.data.text[lang] || "текст отсутствует"
    }
});