Template.textContent.helpers(
{
    language: function(){
    },

    title: function(){
        let l = Template.parentData(1).params.language;
        return this.title[l];
    },
    html: function(){
        let l = Template.parentData(1).params.language;
        return this.data && this.data[l] && this.data[l].html;
    }
  //textContentBackground: CMSSettings.public.cms.textContentDefaultBackground
});
