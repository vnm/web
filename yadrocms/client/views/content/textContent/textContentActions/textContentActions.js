Template.textContentActions.helpers({
    //add you helpers here
});

Template.textContentActions.events({
    "click #editTextContent": function(e,t){
        let p = Template.parentData(1).params;
        p.id = this._id;
        Router.go("editText", p);
    },
    "click #removeTextContent": function(e,t){
        Meteor.call('removeItem', this._id);
    }
});

Template.textContentActions.onRendered(function () {
    $('.dropdown').dropdown(
        {
            on          : 'hover'
            ,  action      : "hide"
            ,  allowTab    : false
            ,  transition  : 'fade down'
        });
});