Template.fourViewsActions.helpers({
    //add you helpers here
});

Template.fourViewsActions.events({
    "click #editFourViewsActions": function(e,t){
        let p = Template.parentData(1).params;
        p.id = this._id;
        Router.go("editFourViews", p);
    },
    "click #removeFourViewsActions": function(e,t){
        Meteor.call('removeItem', this._id);
    }
});

Template.fourViewsActions.onCreated(function () {
    //add your statement here
});

Template.fourViewsActions.onRendered(function () {
    $('.dropdown').dropdown(
        {
            on          : 'hover'
            ,  action      : "hide"
            ,  allowTab    : false
            ,  transition  : 'fade down'
        });
});

Template.fourViewsActions.onDestroyed(function () {
    //add your statement here
});

