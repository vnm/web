Template.sectionsMenuActions.helpers({
    sectionsActions: function(){
        let a = [];
        let owner = this.section && this.section._id;
        let az = Sections.find({owner: owner, type: "activeImageSection"}).count();
        let fv = Items.find({owner: owner, type: "fourViews"}).count();

        if(this.params.owner === "root") {
            a =
                [
                    new MenuActionVO("Прибор", 'inbox', 'createSection'),

                ];
        }
        else{
            if (!az && (this.section && this.section.type!=="activeImageZone")){
                a.push(new MenuActionVO("Активная картинка", "crosshairs", 'createActiveImage'));
            }
            if (!fv) {
                a.push(new MenuActionVO("Виды", 'cube', 'createFourViews'));
            }
            a.push(new MenuActionVO("Картинка", 'photo', 'createImage'));
            a.push(new MenuActionVO("Видео", 'video', 'createVideo'));
            a.push(new MenuActionVO("Текст", "newspaper", 'createText'));
            a.push(new MenuActionVO("Набор", "grid layout", 'createPack'));

        }
        return a;
    }
});

Template.sectionsMenuActions.onRendered(function(){
    $('.dropdown').dropdown(
        {
            on          : 'hover'
            ,  action      : "hide"
            ,  allowTab    : false
            ,  transition  : 'fade down'
        });
});
