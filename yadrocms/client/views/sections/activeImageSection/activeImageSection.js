Template.activeImageSection.helpers({
    activePointModeAdd: function(){
        return Session.get("activePointMode") === "add";
    },
    activePointModeRemove: function(){
        return Session.get("activePointMode") === "remove";
    },


    activeZoneList: function(){
        return Template.parentData(1).activeZoneList;
    },

    language: function(){
        let p = Session.get("point");
        return p&&p.language;
    }
});


let point = {};


Template.activeImageSection.events({
    "click #activeImage": function(e,t){

        point.x = e.offsetX/e.currentTarget.clientWidth;
        point.y = e.offsetY/e.currentTarget.clientHeight;

        let state = Session.get("activePointMode");
        if (state === "add") {
            $("#POIName").val('');
            $('#activeZoneTitleModal')
                .modal('show');
        }

    },

    "click #removePoints":function(e,t){
        Session.set("activePointMode", "remove");
    },

    "click #addPoints": function(e,t){
        Session.set("activePointMode", "add");
    },
});

Template.activeImageSection.onCreated(function () {
    //add your statement here
});

Template.activeImageSection.onRendered(function () {
    Session.set("activePointMode", "add");

    let lang = Template.parentData(1).params.language;
    let device = Template.parentData(1)
        && Template.parentData(1).section
        &&Template.parentData(1).section._id;

    let that = this;



    $('.dropdown').dropdown(
        {
            on          : 'hover'
            ,  action      : "hide"
            ,  allowTab    : false
            ,  transition  : 'fade down'
        });

    $('.ui.accordion')
        .accordion()
    ;

    if(Router.current().route.getName() === "sections") {
        $('.aztm')
            .modal({
                onApprove: function () {
                    let o = {};
                    o.owner = that.data._id;
                    o.title = {};
                    o.title[lang] = $("#POIName").val();
                    o.point = point;
                    o.device = device;
                    o.type = "activeImageZone";
                    Sections.insert(o);
                }
            });

        $('.eaztm')
            .modal({
                onApprove: function () {
                    let p = Session.get("point");

                    let o = {};
                    o.$set = {};
                    o.$set["title." + p.language] = $("#POINameEdited").val();

                    Sections.update(p.id, o);
                }
            });
    }
});

Template.activeImageSection.onDestroyed(function () {
    //add your statement here
});

