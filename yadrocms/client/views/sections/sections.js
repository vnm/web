Template.sections.helpers({
    hasContent: function(){
        if (this.sectionList.count()>0 || this.itemList.count()>0)
            return true;
        else
            return false;
    },
    isActiveZoneSection: function(){
        if (this.section && this.section.type==="activeImageZone")
            return true;
        else
            return false;
    }
});

Template.sections.events({
    //add your events here
});

Template.sections.onCreated(function () {
    //add your statement here
});

Template.sections.onRendered(function () {
    //add your statement here
});

Template.sections.onDestroyed(function () {
    //add your statement here
});

