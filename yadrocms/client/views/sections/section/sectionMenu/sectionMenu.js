Template.sectionMenu.events({
    "click #removeSection": function(){
        Meteor.call("removeSection", this._id);
    },

    "click #editSection": function(){
        let params = Template.parentData(1).params;
        params.sectionId = this._id;

        Router.go("editSection", params);
    }
});

Template.sectionMenu.onRendered(function () {
    $('.dropdown').dropdown(
        {
            on          : 'hover'
            ,  action      : "hide"
            ,  allowTab    : false
            ,  transition  : 'fade down'
        });
});
