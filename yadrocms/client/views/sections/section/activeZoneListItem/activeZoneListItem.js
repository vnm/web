Template.activeZoneListItem.helpers({
    title:function(){
        let l = Template.parentData(2).params.language;
        return this && this.title && this.title[l];
    }
});

Template.activeZoneListItem.events({
    "click a.header": function(e,t){

        e.preventDefault();

        let p = Template.parentData(2).params;
        p.owner = this._id;

        Router.go("sections", p);

    }
});

Template.activeZoneListItem.onCreated(function () {
    //add your statement here
});

Template.activeZoneListItem.onRendered(function () {
    //add your statement here
});

Template.activeZoneListItem.onDestroyed(function () {
    //add your statement here
});

