Template.section.helpers({
    title: function(){
        let l = Template.parentData(1).params.language;
        if(this && this.title && this.title[l])
            return this.title[l]
        else{
            return "название не задано"
        }
    },
    language: function(){
        return Template.parentData(1).params.language;
    },
    // activeImage: function(){
    //     if (this.device.activeImage) {
    //         return this.device.activeImage;
    //     }
    //     // else
    //     //     return "/assets/images/image.png";
    // },


    activeImage: function(){
        if (this.type === "activeImageSection")
            return true;
    }
});



Template.section.events({
    "click #editImage": function(e,t){
        let p = Template.parentData(1).params;
        p.id = this._id;

        Router.go("editActiveImage", p);
    },

    "click #removeImage": function(e,t){
        Meteor.call("removeSection", this._id);
    },

    "click #icon": function(e,t){
        let o = this._id;
        let l = Template.parentData(1).params.language;
        Router.go('sections', {owner: o, language: l});
    }
});

Template.section.onCreated(function () {
    //add your statement here
});

Template.section.onRendered(function () {

});

Template.section.onDestroyed(function () {
    //add your statement here
});

