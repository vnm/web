Template.activeButton.helpers({
    x: function(){
        return this.point.x*100;
    },
    y: function(){
        return this.point.y*100;
    },
    title: function(){
        let l = Template.parentData(2).params.language;
        return this && this.title && this.title[l];
    }
});

Template.activeButton.events({
    "click .circular": function(){
        let language = Template.parentData(2).params.language;

        let state = Session.get("activePointMode");

        if (state === "remove"){
            Meteor.call("removeSection", this._id);
        }

        if (state === "add"){
            if (this.title[language]){
                let p = Template.parentData(2).params;
                p.owner = this._id;
                Router.go("sections", p);
            }
            else{
                let point = {};
                point.id = this._id;
                point.language = language;

                Session.set("point", point);
                $("#POINameEdited").val('');
                $('#editActiveZoneTitleModal')
                    .modal('show');
            }

        }
    }
});

Template.activeButton.onCreated(function () {
    //add your statement here
});

Template.activeButton.onRendered(function () {
    $('.circular.ui.icon.button')
        .popup({
        });
    ;
});

Template.activeButton.onDestroyed(function () {
    //add your statement here
});

