Template.sectionsHeader.helpers({
    deviceTitle: function () {
        let language = this.params.language;

        if (this.params.owner!== "root") {
            if (this.section.type === "activeImageZone"){
               let device = Sections.findOne(this.section.device);
               if(device && device.title && device.title[language])
                   return device.title[language];
               else
                   return "*название не задано*";
            }
            if (this.section && this.section.owner === "root") {
                if(this.section && this.section.title && this.section.title[language])
                    return this.section.title[language];
                else
                    return "*название не задано*";
            }
        }
    },

    deviceModel: function(){
        if (this.params.owner!== "root") {
            if (this.section.type === "activeImageZone"){
                let device = Sections.findOne(this.section.device);
                return device && device.model;
            }
            if (this.section && this.section.owner === "root") {
                return this.section && this.section.model;
            }
        }
    },

    activeZoneTitle: function(){

        let language = this.params.language;

        if (this.section.type === "activeImageZone") {
            if(this.section.title[language])
                return this.section.title[language];
            else
                return "*название не задано*";
        }

    },

    bcowner: function(){
        if (this.section.type === "activeImageZone"){
            let device = Sections.findOne(this.section.device);
            return device && device._id;
        }
    }


});
