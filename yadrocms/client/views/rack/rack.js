let unit2pixel = 40;
let a = [];
rackHeight = 42;

Template.rackDevice.helpers({
    deviceHeight: function() {
        return this.deviceHeight / unit2pixel;
    },
    position: function(){
        return this.position/(unit2pixel/2);
    }
});

Template.rack.helpers({
    devicesList: function(){
        let lang = this.params.language;
        a=[];
        for (let j=0; j<this.devicesList.length; j++) {

            let o = {};
            let h = Sections.findOne(this.devicesList[j].device).deviceHeight * unit2pixel;

            o._id = this.devicesList[j]._id;
            o.device = this.devicesList[j].device;
            o.deviceHeight = h;
            o.image = Sections.findOne(this.devicesList[j].device).image;
            o.model = Sections.findOne(this.devicesList[j].device).model;
            o.position = this.devicesList[j].position*unit2pixel/2;
            o.title = Sections.findOne(this.devicesList[j].device).title[lang];

            a.push(o);
        }

        return a;
    }
});

Template.rack.events({
    'dblclick #rackContainer': function(e,t){
        Router.go('addToRack', {language: this.params.language});
    }
});

Template.rack.onRendered(function(){
    setTimeout(function() {
        $('.help').slideUp();
    }, 15000);
});

let starty=0;
let pos = 0;

Template.rackDevice.onRendered(function(){

    let that = this;
    let intersect = false;
    let id;
    let newpos;

    $(".draggable").on('dragover', function(e){e.preventDefault();});

    $(this.firstNode).on('dragstart', function(e) {

        starty = e.originalEvent.clientY;
        pos = parseInt($(e.currentTarget).css("top"));
    });

    $(this.firstNode).on('dragend', function(e) {
        if (!intersect) {
            Rack.update(id, {$set: {position: newpos / (unit2pixel / 2)}})
        }
        else {
            $(e.currentTarget).animate({
                top: pos + "px",
            }, 0);
        }
        $(e.currentTarget).css('background-color', 'rgba(255,255,255,0)');
    });

    $(this.firstNode).on('drag', function(e) {
        // e.originalEvent.dataTransfer
        // e.originalEvent.dataTransfer.setData( 'text/plain', '' );
        // document.body.style.cursor = "move";
        e.originalEvent.preventDefault();

        if (e.originalEvent.clientY === 0){
            return;
        }

        let dy = e.originalEvent.clientY - starty;
        newpos = pos + dy;

        newpos = Math.round(newpos / (unit2pixel / 2)) * (unit2pixel / 2);

        if (newpos < 0) {
            newpos = 0;
        }

        if (newpos + that.data.deviceHeight > rackHeight * unit2pixel) {
            newpos = rackHeight * unit2pixel - that.data.deviceHeight;
        }

        id = that.data._id;

        intersect = false;
        $(e.currentTarget).css('background-color', 'rgba(220,220,220,0.5)');

        for (let i = 0; i < a.length; i++) {
            if (a[i]._id === id)
                continue;
            if (a[i].position >= newpos && a[i].position < newpos + that.data.deviceHeight ||
                newpos >= a[i].position && newpos < a[i].position + a[i].deviceHeight) {
                $(e.currentTarget).css('background-color', 'red');
                intersect = true;
            }
        }

        $(e.currentTarget).animate({
            top: newpos + "px",
        }, 0);
    });
});