//rack.js

Template.rackDevice.events({
    'click .removeRackItem':function(e,t){
        e.preventDefault();
        e.stopPropagation();
        let device = this.device;
        let language = Template.parentData(1).params.language;

        Rack.remove(this._id, function(e,r){
           if (!e){
                toastr.success(Sections.findOne(device).title[language] + " удален из рэка.", "Успех");
            }
            else{
                toastr.error("Не удалось удалить прибор", "Ошибка");
            }
        });
    },

    'dblclick': function(e,t){
        e.preventDefault();
        e.stopPropagation();
    },

    'click .device': function(e,t){
        let l = Template.parentData(1).params.language;
        let d = this.device;
        Router.go('sections', {owner: d, language: l});
    }
})
