Template.rackMenuActions.helpers({
    sectionsActions: function(){
        let a = [
                    new MenuActionVO("Прибор", 'inbox', 'addToRack')
        ];


        return a;
    }
});

Template.rackMenuActions.onRendered(function(){
    $('#rackMenuActionsDropdowwn').dropdown(
        {
            on          : 'hover'
            ,  action      : "hide"
            ,  allowTab    : false
            ,  transition  : 'fade down'
        });
});
