Template.rackMenuActionsItem.events({
    "click .item": function(e,t){
        var params = Template.parentData(1).params;
        Router.go(this.route, params);
    }
})