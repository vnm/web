Template.rackHeader.helpers({
    title: function(){
        let language = this.params.language;

       if (this.params.owner !== "root"){
           if (this.section.type === "activeImageZone"){
               let device = Sections.findOne(this.section.device);
               let t = device && device.title && device.title[language];
               let m = device && device.model;

               return t + " " + m + "/зона:" + this.section.title[language];
           }
           else{
               let t = this && this.section && this.section.title && this.section.title[language];
               return "Приборы/" + t + " " + this.section.model;
           }
       }
       else{
           return "Приборы";
       }
    }
});
