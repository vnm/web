Template.menu.helpers({
    section: function(){
        if (this.params.owner !== 'root')
            return true;
        else
            return false;
    },

    owner: function(){
        var i = Sections.findOne(this.params.owner);
        if (i) {
            return i.owner;
        }
    },

    lang :function(){
        return this.params.lang;
    },

    allSubSections: function(){
        var sec = Sections.findOne(this.params.owner);
        if (sec)
            return Sections.find({owner: sec.owner}, {sort: {position:1}});
    }
});

