Template.menuSubSection.helpers({
   isActive: function(){
      if (this._id===Template.parentData(1).params.owner)
         return true;
   },

   'title': function(){
      let lang = Template.parentData(1).params.lang;
      if (this.data[lang] && this.data[lang].title)
         return this.data[lang].title;
      else
         return "*не задано* [" + this.name + "]";
   },
   'lang': function(){
      return Template.parentData(1).params.lang;
   }
})