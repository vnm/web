Template.menuSection.helpers({
    'popupContent': function(){
        return "<img src='/file/" + this[lang].logo.fileId + "'/>";
    },
    'title': function(){
        let lang = Template.parentData(1).params.lang;
        if (this.data[lang] && this.data[lang].title)
            return this.data[lang].title;
        else
            return "*не задано* [" + this.name + "]";
    },
    'file': function(){
        let lang = Template.parentData(1).params.lang;
        if (this.data[lang] && this.data[lang].file)
            return this.data[lang].file;
    }
});

Template.menuSection.events({
    'click div.item':function(e,t){
        let params = Template.parentData(1).params;
        params.page = 0;
        params.owner = this._id;

        Router.go("sections", params);
    }
});

Template.menuSection.onRendered(function(){
    $(this.firstNode).find(".right.floated").popup();
});