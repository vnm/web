Template.settings.helpers({
    showTable: function() {
        return this.terminals.count();
    },
    currentLanguage: function(){
        return Settings.findOne({name: "languages"}).currentLanguage;
    },
    languages: function(){
        return Settings.findOne({name: "languages"}).languages;
    },
    selectedValue: function(v){
        if(v===Settings.findOne({name: "languages"}).currentLanguage){
            return "selected";
        }
    }
});

Template.settings.events({
    "click .addIP": function(e,t){
        let ip = $(e.target.parentElement.children[0]).val();

        if (ip.length>7){
            Terminals.insert({ip: ip});
            $(e.target.parentElement.children[0]).val('');
        }
    },

    "click .applyLangs":function(e,t){

        let l = $('.applang').val();
        let id = Settings.findOne({name: "languages"})._id;
        Settings.update(id, {$set: {currentLanguage: l}}, function(e,r){
            if (!e){
                Meteor.call('refreshClients', 'resfreshLang', function(e,res){
                    if (res) {
                        res.forEach(function(r){
                            if (r.result === "error") {
                                toastr.error("Не удалось обновить " + r.ip, "Ошибка");
                            }
                            if (r.result === "success") {
                                toastr.success(r.ip + " успешно обновлен", "Успех");
                            }
                        })
                    }
                });
            }
        });


    }
});

Template.terminalRow.events({
    "click .removeIP": function(){
        Meteor.call('removeTerminal', this._id);
    }
})