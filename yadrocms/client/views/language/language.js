Template.language.helpers({
    languages: function(){
        return Settings.findOne({name: "languages"}) &&
            Settings.findOne({name: "languages"}).languages;
    }
});

Template.language.events({
    "click .item": function(e,t){
        let lang = e.currentTarget.innerHTML;
        let params = t.data.params;
        params.language = lang;

        let route = Router.current().route.getName();

        Router.go(route, params);
    }
});

Template.language.onCreated(function () {
    //add your statement here
});

Template.language.onRendered(function () {
    this.$('.dropdown').dropdown(
        {
            on          : 'hover'
            ,  action      : "hide"
            ,  allowTab    : false
            ,  transition  : 'fade down'
        });
});

Template.language.onDestroyed(function () {
    //add your statement here
});

