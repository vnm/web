Statistic = new Meteor.Collection('statistic');

if (Meteor.isServer){
    Statistic.allow({
        'update': function () {
            if (Meteor.userId())
                return true;
        },
        'remove': function(){
            if (Meteor.userId())
                return true;
        },
        'insert': function(){
            if (Meteor.userId())
                return true;
        },


    });
}