sberUsers = new Meteor.Collection('sberUsers');

if (Meteor.isServer){
    sberUsers.allow({
        'update': function () {
            if (Meteor.userId())
                return true;
        },
        'remove': function(){
            if (Meteor.userId())
                return true;
        },

    });
}