/**
 * Created by Alexey on 29.04.2016.
 */
State = new Meteor.Collection('state');

if (Meteor.isServer){
    State.allow({
        'update': function () {
            if (Meteor.userId())
                return true;
        },
        'remove': function(){
            if (Meteor.userId())
                return true;
        },

    });
}