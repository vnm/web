/**
 * Created by Alexey on 18.04.2016.
 */
terminals = new Meteor.Collection('terminals');

if (Meteor.isServer){
    terminals.allow({
        'update': function () {
            if (Meteor.userId())
                return true;
        },
        'remove': function(){
            if (Meteor.userId())
                return true;
        },
        'insert': function(){
            if (Meteor.userId())
                return true;
        },


    });
}