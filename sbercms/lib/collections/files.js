var path = process.env.STORE;
console.log("Store path " + path);
Files = new FS.Collection("files", {
    stores: [new FS.Store.FileSystem("files", {path: path})]
});

Files.allow({
    'insert': function () {
        return true;
    },
    download: function () {
        return true;
    },
    remove: function(){
        return true;
    },
    update: function(){
        return true;
    }
});
