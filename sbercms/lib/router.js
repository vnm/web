
/**
 * Created by Alexey on 12.04.2016.
 */

//import { Router } from 'meteor/iron:router';
Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading'
});

Router.onBeforeAction(function() {
    if (Meteor.isClient) {
        if (!Meteor.userId()) {
            this.redirect('login');
        } else {
            this.next();
        }
    }

    if (Meteor.isServer){
        this.next();
    }
});

Router.route('home', {
    path: '/',
    action: function(){
        console.log('here');
        Router.go('mainTable', {page: 0})
    }
})
Router.route('mainTable', {
    path: '/main/:page/:keyword?',
    waitOn: function(){
        return [Meteor.subscribe('state'), Meteor.subscribe('sberUsers', this.params.keyword, this.params.page), Meteor.subscribe('files')];
    }
});

Router.route('controlPanel', {
    path: '/control',
    subscriptions: function(){
        return [Meteor.subscribe('terminals')];
    }
});

Router.route('statisticPanel', {
    path: '/statistic/:page?/:terminal?/dates/:fromDate?/:toDate?/view/:view?/keyword/:keyword?',
    waitOn: function(){
        var page = this.params.page;
        if (page === undefined)
            page = 0;

        var view = this.params.view;
        if (view === undefined || view === "journal"){
            return [Meteor.subscribe('state'), Meteor.subscribe('statistic', page, this.params.terminal, this.params.fromDate, this.params.toDate, this.params.keyword), Meteor.subscribe('terminals')];
        }
        if (view === "byLogin"){
            return [Meteor.subscribe('statUsers', page, this.params.terminal, this.params.fromDate, this.params.toDate), Meteor.subscribe('state'), Meteor.subscribe('terminals')];
        }

    }
})

Router.route('login',{
    path: '/login',
    layoutTemplate: "loginLayout"
});

Router.route('logout',{
    path: '/logout',
    action: function() {
        Meteor.logout(function (err) {
            if (err)
                console.log('Logout failed, reason: ' + err);
            else {
                console.log('Logout ok');
            }
        });
    }
});

Router.route('/api/users', { where: 'server' })
    .get(function () {

        let _users = sberUsers.find({dontsync: false},{fields: {login: 1, fileId: 1, name: 1, description: 1, department: 1, division:1, room:1, intPhone:1, phone:1}}).fetch();
        let users = [];

        for (let i=0; i< _users.length; i++){
            let user =_users[i];
            if (user && user.fileId && user.fileId.length>0)
                user.filePath = Files.findOne(user.fileId).url();
            users.push(user);
        }

        this.response.writeHead(200, {'Content-Type': 'text/html'});
        this.response.end(JSON.stringify(users));
    })
