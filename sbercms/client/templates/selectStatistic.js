Template.selectStatistic.helpers({
    selected: function(value){
        var ssq = Router.current().params.terminal;

        if (ssq && ssq === value)
            return 'selected';
        else
            return "";
    },
    selectedView: function(value){
        var ssq = Router.current().params.view;

        if (ssq && ssq === value)
            return 'selected';
        else
            return "";
    },
    terminals: function(){
        return terminals.find();
    },

    from: function(){
        var q = Router.current().params.fromDate;
        if (q){
            return moment(parseInt(q)).format("HH:mm DD.MM.YYYY");
        }
    },

    to: function(){
        var q = Router.current().params.toDate;
        if (q){
            return moment(parseInt(q)).format("HH:mm DD.MM.YYYY");
        }
    }
});

Template.selectStatistic.events({
    'click button.showStat': function(){

        var fromDate;
        var toDate;

        var terminal = $('.terminalSelect').val();

        if ($('#statFromdate').data("DateTimePicker").date()!== null)
            fromDate = $('#statFromdate').data("DateTimePicker").date().unix()*1000;

        if ($('#statTodate').data("DateTimePicker").date()!==null)
            toDate = $('#statTodate').data("DateTimePicker").date().unix()*1000;

        var view = $('.viewSelect').val();

        Router.go(Router.current().route.getName(), {page:0, terminal: terminal, fromDate: fromDate, toDate: toDate, view: view});
    },

    'click button.downloadSelected': function(e,t){
        //https://atmospherejs.com/lfergon/exportcsv

        var nameFile = 'statistic(' + moment().format("DD.MM HH:mm") + ').csv';

        var statSearchQuery = {};
        statSearchQuery.terminal = Router.current().params.terminal;
        statSearchQuery.fromDate = Router.current().params.fromDate;
        statSearchQuery.toDate = Router.current().params.toDate;
        statSearchQuery.view = Router.current().params.view;

        Meteor.call('downloadCSV', statSearchQuery, function(err, fileContent) {
            if(fileContent){
                var blob = new Blob([fileContent], {type: "text/plain;charset=utf-8"});
                saveAs(blob, nameFile);
            }
        });
    },


})
Template.selectStatistic.rendered = function(){
    this.$('#statFromdate').datetimepicker({format: 'HH:mm DD.MM.YYYY'});
    this.$('#statTodate').datetimepicker({format: 'HH:mm DD.MM.YYYY'});
};

