Template.statPagination.helpers({
    pageNum: function(){
        return this+1;
    },
    currentRoute: function(){
        return Router.current().route.getName();
    },
    terminal: function(){
        return Router.current().params.terminal;
    },
    fromDate: function(){
        return Router.current().params.fromDate;
    },
    toDate: function(){
        return Router.current().params.toDate;
    },
    isActive: function(){
        if (Router.current().params.page == this)
            return "active";
        else
            return "";
    },
    view: function(){
        return Router.current().params.view;
    },
    keyword: function(){
        return Router.current().params.keyword;
    }
})