Template.controlPanel.helpers({
    terminals: function(){
        return terminals.find();
    },
    'suMail': function(){
        return Meteor.users.findOne({username: 'admin'}).profile.mail;
    }
});

Template.controlPanel.events({
    'click a.addTerminal': function(){
        var ip = prompt("Please enter IP");
        if(ip.length > 8){
            terminals.insert({ip: ip});
        }
    },

    'click a.refreshAll': function(){
        Meteor.call('refreshAll');
    },

    'click button.changesumail': function(){
        var mail = $('#sumail').val();
        Meteor.call('changeMail', 'admin', mail);
        $('#sumail').val('');
    },

    'click button.changeupwd': function(){
        var pwd = $('#userpwd').val();
        var pwd1 = $('#userpwd1').val();

        if (pwd === pwd1) {
            if (pwd.length>2) {
                Meteor.call('changePwd', 'admin', pwd);
                $('#userpwd').val('');
                $('#userpwd1').val('');
            }
            else{
                alert("Минимальная длина 3 символа")
            }
        }
        else {
            alert("Пароли не совпадают");
        }
    },
});

Template.controlPanel.onCreated(function () {
    //add your statement here
});

Template.controlPanel.onRendered(function () {
    //add your statement here
});

Template.controlPanel.onDestroyed(function () {
    //add your statement here
});

