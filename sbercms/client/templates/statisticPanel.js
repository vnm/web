

Template.statisticPanel.helpers({
    statistic: function(){
        return Statistic.find({},{sort:{Timestamp: -1}});
    },

    pages: function(){
        var view = Router.current().params.view;
        if (view === undefined || view ==="journal") {
            if (State.find()) {
                var pagesNum = State.findOne().usersCount;
                var usersOnPage = State.findOne().usersOnPage;

                var a = [];
                for (var i = 0; i < Math.ceil(pagesNum / usersOnPage); i++) {
                    a.push(i);
                }
                return a;
            }
        }
        if (view === "byLogin"){
            var keyword = Router.current().params.keyword;

            var s = {};
            if (keyword) {
                s.$or=[];

                var a = {};
                a.login = {};
                a.login.$regex = "(" + keyword + ")";
                a.login.$options =  'i';

                s.$or.push(a);

                var b = {};
                b.name = {};
                b.name.$regex = "(" + keyword + ")";
                b.name.$options =  'i';

                s.$or.push(b);
            }


            var pagesNum = sberUsers.find(s).count();

            var usersOnPage = State.findOne().usersOnPage;

            var a = [];
            for (var i = 0; i < Math.ceil(pagesNum / usersOnPage); i++) {
                a.push(i);
            }
            return a;
        }
    },

    zero: function() {
        var view = Router.current().params.view;
        if (view === undefined || view ==="journal") {
            if (Statistic.find().count() === 0)
                return true;
        }
        else
            return false;
    },

    currentRoute: function(){
        return Router.current().route.getName();
    },

    terminal: function(){
        return Router.current().params.terminal;
    },
    fromDate: function(){
        return Router.current().params.fromDate;
    },
    toDate: function(){
        return Router.current().params.toDate;
    },
    maxpage: function(){
        if (State.findOne()) {
            var pagesNum = State.findOne().count;
            var usersOnPage = State.findOne().usersOnPage;
            return Math.ceil(pagesNum / usersOnPage) - 1;
        }
    },
    view: function(){
        return Router.current().params.view;
    },

    isJournalView: function(){
        if (Router.current().params.view === undefined || Router.current().params.view === "journal")
            return true;
        else
            return false;
    },

    Calculated: function(){
        var usersOnPage = State.findOne().usersOnPage;
        var page = Router.current().params.page;
        var keyword = Router.current().params.keyword;

        var s = {};
        if (keyword) {
            s.$or=[];

            var a = {};
            a.login = {};
            a.login.$regex = "(" + keyword + ")";
            a.login.$options =  'i';

            s.$or.push(a);

            var b = {};
            b.name = {};
            b.name.$regex = "(" + keyword + ")";
            b.name.$options =  'i';

            s.$or.push(b);
        }

        return sberUsers.find(s, {skip: page*usersOnPage, limit: usersOnPage, sort: {type0: -1, type1: -1, login: 1}});
    }
});

Template.statisticPanel.events({
    //add your events here
});

Template.statisticPanel.onCreated(function () {
    //add your statement here
});

Template.statisticPanel.onRendered(function () {
    //add your statement here
});

Template.statisticPanel.onDestroyed(function () {
    //add your statement here
});

