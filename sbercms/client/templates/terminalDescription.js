Template.terminalDescription.helpers({
    'panelClass': function(){
        if (this.status === 'online')
            return 'panel-success';
        if (this.status === 'offline')
            return 'panel-warning';
        if (this.status === 'softwareisdown')
            return 'panel-danger';

        return 'panel-default';
    },

    'status': function(){
        if (this.status)
            return this.status;
        else
            return "n/a";
    },

    'updatedAt' : function(){
        var ts = terminals.findOne(this._id).updatedAt;
        if (ts)
            return moment(parseInt(ts)).format('DD-MM-YY;HH:mm');
        else
            return "нет данных"
    },


    disabled: function(){
        if (this.status !=='online')
            return 'disabled';
        else
            return "";
    },
});

Template.terminalDescription.events({
    'click button.deleteTerminal': function(){
        terminals.remove(this._id);
    },
    'click button.refresh': function(){
        var id = this._id;
        Meteor.call('refreshClient', id);
    },
});

Template.terminalDescription.onCreated(function () {
    //add your statement here
});

Template.terminalDescription.onRendered(function () {
    //add your statement here
});

Template.terminalDescription.onDestroyed(function () {
    //add your statement here
});

