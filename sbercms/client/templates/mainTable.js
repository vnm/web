Template.mainTable.helpers({
    sberUsers: function(){
        return sberUsers.find({},{sort: {name: 1}});
    },

    currentRoute: function(){
        return Router.current().route.getName();
    },

    pages: function(){

        if (State.findOne()) {

            var usersCount = State.findOne().usersCount;
            var usersOnPage = State.findOne().usersOnPage;

            console.log(usersCount);
            console.log(usersOnPage);

            var a = [];
            for (var i = 0; i < Math.ceil(usersCount / usersOnPage); i++) {
                a.push(i);
            }
            return a;
        }
    },

    maxpage: function(){
        if (State.findOne()) {
            var pagesNum = State.findOne().count;
            var usersOnPage = State.findOne().usersOnPage;
            return Math.ceil(pagesNum / usersOnPage) - 1;
        }
    },

    keyword: function(){
        return Router.current().params.keyword;
    },

    hideProgress: function(){
        if (State.findOne()){
            if (State.findOne().refreshing)
                return "";
            else
                return "hidden";
        }
    }
});

Template.mainTable.events({
    'click .refreshFromCSV': function(e,t) {
        e.preventDefault();
        console.log("refresh request")
        Meteor.call('refreshFromCSV', function(e,r){
            console.log('return')
            console.log(r.result);
            //if (r)
                //$(".progress").slideUp();
        });

        //$(".progress").slideDown();
    },

    'change #avatarSelect': function(e,t){

        var id = Session.get('clickedId');
        var file = e.currentTarget.files[0];
        if (file.size> 300000){
            alert('Аватар не более 300Кб')
            return;
        }

        var fileId = sberUsers.findOne(id).fileId;
        Files.remove(fileId);

        Files.insert(file, function (err, fileObj) {
            if (err){
                console.log('Uploading file error, ' + err);
            }
            else
            {
                sberUsers.update(id, {$set: {'fileId': fileObj._id}})
            }
        });
    }

});

Template.mainTable.onCreated(function () {
    //add your statement here
});

Template.mainTable.onRendered(function () {
    //if (sberUsersCount.findOne()){
    //    if (sberUsersCount.findOne().refreshing)
    //        $(".progress").slideDown();
    //}
    //
    //var q = sberUsersCount.find();
    //var h = query.observeChanges({
    //    changed: function(){
    //        if (sberUsersCount.findOne()){
    //            if (sberUsersCount.findOne().refreshing)
    //                $(".progress").slideDown();
    //            else
    //                $(".progress").slideUp();
    //        }
    ////    }
    //})
});

Template.mainTable.onDestroyed(function () {
    //add your statement here
});

