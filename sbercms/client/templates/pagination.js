Template.pagination.helpers({
    pageNum: function(){
        return this+1;
    },
    currentRoute: function(){
        return Router.current().route.getName();
    },
    keyword: function(){
        return Router.current().params.keyword;
    },
    isActive: function(){
        if (Router.current().params.page == this)
            return "active";
        else
            return "";
    }
})

Template.pagination.events({
    //add your events here
});

Template.pagination.onCreated(function () {
    //add your statement here
});

Template.pagination.onRendered(function () {
    //add your statement here
});

Template.pagination.onDestroyed(function () {
    //add your statement here
});

