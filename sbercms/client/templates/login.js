Template.login.helpers({
    //add you helpers here
});

Template.login.events({
    'submit .form-signin': function(e ,t){
        e.preventDefault();

        var name = t.find('#inputLogin').value;
        var password = t.find('#inputPassword').value;

        Meteor.loginWithPassword(name, password, function(err){
            if (err)
                console.log('Login failed, reason: ' + err);
            else {
                console.log("Login ok");
                Router.go('mainTable', {page: 0});
            }
        })
    }
})

Template.login.onCreated(function () {
    //add your statement here
});

Template.login.onRendered(function () {
    //add your statement here
});

Template.login.onDestroyed(function () {
    //add your statement here
});

