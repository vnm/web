Template.statTableRow.helpers({
    time: function(){
        return moment(this.Timestamp).format('DD-MM-YY;HH:mm');
    },
    message: function(){
        if (this.Type == 0){
            return "Выбран в поиске"
        }
        if (this.Type == 1) {
            return "Построен маршрут"
        }
    }
});

Template.statTableRow.events({
    //add your events here
});

Template.statTableRow.onCreated(function () {
    //add your statement here
});

Template.statTableRow.onRendered(function () {
    //add your statement here
});

Template.statTableRow.onDestroyed(function () {
    //add your statement here
});

