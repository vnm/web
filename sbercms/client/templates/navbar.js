Template.navbar.helpers({
    keyword: function(){
        return Router.current().params.keyword;
    },

    disabled: function(){
        if (Router.current().route.getName() === 'mainTable')
            return "";
        if (Router.current().route.getName() === 'statisticPanel')
            return "";
        return "disabled";
    }
})
Template.navbar.events({
    'change .searchField': function(e,t){
        var params = Router.current().params;
        params.page = 0;
        params.keyword = e.target.value;


        Router.go(Router.current().route.getName(), params);
        e.preventDefault();
    },

    'click .clearSearch': function(e,t){
        var params = Router.current().params;
        params.page = 0;
        params.keyword = "";
        Router.go(Router.current().route.getName(), params);
    },

    'keydown .searchField': function(e,t){
        if (e.keyCode == 27) {
            Router.go(Router.current().route.getName(), {page: 0});
            $(e.target).blur();
        }
    }
});