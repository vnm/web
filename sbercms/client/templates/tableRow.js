Template.tableRow.helpers({
    refreshDisable: function(){
        if (this.refreshDisable)
            return "checked"
        else
            return ""
    },

    popoverContent: function(){
        var record = Files.findOne(this.fileId);
        if (record)
            return "<img src='"+record.url()+"'/>";

    },
    //add you helpers here
});

Template.tableRow.events({
    'change .refreshDisable': function(e,t){
        if (e.target.checked === this.refreshDisable)
            return;
        var id = this._id;
        sberUsers.update(id, {$set: {"refreshDisable": e.target.checked}}, function(err){
            if(err){
                console.log('Ошибка изменения refreshDisable: ' + err);
            }
            else{
                console.log('refreshDisable изменен: ' + e.target.checked);
            }
        })
    },

    'blur td': function(e,t){

        var newString = e.currentTarget.textContent;
        var column = e.currentTarget.className;

        if (newString == this[column]) {
            return;
        }

        //e.target.textContent = "";
        var id = this._id;

        var query = {}
        query['$set'] = {}
        query.$set[column] = newString;


        sberUsers.update(id, query, function(err){
            if(err){
                console.log('Ошибка изменения cellNumber: ' + err);
            }
            else{
                console.log(column + ' изменен: ' + newString);
            }
        });
    },

    'click .avatarTd': function(){
        Session.set('clickedId', this._id);
        $('#avatarSelect').click();
    },

    'keydown td': function(e,t){
        if(e.keyCode === 13){
            e.preventDefault();
            $(e.target).blur();
        }

        if (e.keyCode === 27) {
            e.preventDefault();
            var column = e.currentTarget.className;

            if (this[column])
                e.target.textContent = this[column];
            else
                e.target.textContent = "";
            $(e.target).blur();
        }
    },


});

Template.tableRow.onCreated(function () {
    //add your statement here
});

Template.tableRow.onRendered(function () {
    $(this.firstNode).find('.popoverAncor').popover();
});

Template.tableRow.onDestroyed(function () {
    //add your statement here
});

