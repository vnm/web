var terminalsPoolDelay = 30000;
var ping = Npm.require('ping');
var Fiber = Npm.require('fibers');

PoolClients = function(){
    Meteor.setInterval(function(){
        var _terminals = terminals.find().fetch();

        _terminals.forEach(function(terminal){

            var id = terminal._id;
            var host = terminal.ip;

            HTTP.get("http://"+host+":8081/log", {}, function(e,r){
                if (e) {
                    ping.promise.probe(host)
                        .then(function (res) {
                            if (res.alive) {
                                Fiber(function () {
                                    var s = terminals.findOne(id).status;
                                    terminals.update(id, {$set: {status: 'softwareisdown'}})
                                    if (s === 'online')
                                        Meteor.call('sendEmail', 'Terminal ' + host + ' status: software is down')
                                }).run();
                            }
                            else {
                                Fiber(function () {
                                    var s = terminals.findOne(id).status;
                                    terminals.update(id, {$set: {status: 'offline'}})
                                    if (s === 'online' || s === 'softwareisdown')
                                        Meteor.call('sendEmail', 'Terminal ' + host + ' status: terminal is offline')
                            }).run();
                        }
                    });
                }

                else {
                    HTTP.get("http://"+host+":8081/state", {}, function(e,r){
                        if (!e){
                            terminals.update(id, {$set: {status: 'online', updatedAt: r.content}})
                        }
                    });


                    var lastStatRecord = Statistic.findOne({terminal: host}, {sort: {Timestamp: -1}}) &&
                        Statistic.findOne({terminal: host}, {sort: {Timestamp: -1}}).Timestamp;

                    if (lastStatRecord === undefined)
                        lastStatRecord = 0;

                    var statArray = [];
                    try {
                        statArray = JSON.parse(r.content);
                    }
                    catch(e){
                        console.log(e)
                    }

                    for (var i = 0; i< statArray.length; i++){
                        if (statArray[i].Timestamp > lastStatRecord){
                            if (statArray[i].Login.length>0) {
                                statArray[i].client = host;
                                statArray[i].Name = sberUsers.findOne({'login': statArray[i].Login}).name;
                                Statistic.insert(statArray[i]);
                            }
                        }
                    }

                    lastStatRecord = Statistic.findOne({client: host}, {sort: {Timestamp: -1}}) &&
                        Statistic.findOne({client: host}, {sort: {Timestamp: -1}}).Timestamp;

                    if (lastStatRecord === undefined)
                        lastStatRecord = 0;


                    HTTP.post("http://"+host+":8081/log", {params: {'timestamp': lastStatRecord}});

                    Statistic.remove({Timestamp: {$lte: new Date().getTime()-1000*3600*24*90}});
                }
           })
        });

    }, terminalsPoolDelay);
}