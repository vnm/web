Meteor.publish("terminals", function(){
    return terminals.find();
})

Meteor.publish('statistic', function(page, terminal, fromDate, toDate, keyword){
    var searchQuery = {};
    var usersOnPage = State.findOne().usersOnPage;

    if (terminal){
        if (terminal.length>7)
            searchQuery.client = terminal;
    }

    if (!fromDate)
        fromDate = new Date().getTime()-1000*3600*24;

    searchQuery.Timestamp = {};
    searchQuery.Timestamp.$gte = parseInt(fromDate);

    if (toDate)
        searchQuery.Timestamp.$lte = parseInt(toDate);

    if (keyword) {
        searchQuery.$or=[];

        var a = {};
        a.Login = {};
        a.Login.$regex = "(" + keyword + ")";
        a.Login.$options =  'i';

        searchQuery.$or.push(a);

        var b = {};
        b.Name = {};
        b.Name.$regex = "(" + keyword + ")";
        b.Name.$options =  'i';

        searchQuery.$or.push(b);

    }

    var coursor =  Statistic.find(searchQuery, {skip: page*usersOnPage, limit: usersOnPage, sort: {Timestamp: -1}});
    var usersCount = coursor.count();

    var id = State.findOne()._id;
    State.update(id, {$set: {usersCount: usersCount}});

    return coursor;
})

Meteor.publishTransformed('statUsers', function(page, terminal, fromDate, toDate) {
    var searchQuery = {};

    if (terminal){
        if (terminal.length>7)
            searchQuery.client = terminal;
    }

    if (!fromDate)
        fromDate = new Date().getTime()-1000*3600*24;

    searchQuery.Timestamp = {};
    searchQuery.Timestamp.$gte = parseInt(fromDate);

    if (toDate)
        searchQuery.Timestamp.$lte = parseInt(toDate);


    return sberUsers.find({}, {fields: {login: 1, name: 1}}).serverTransform({
        // we extending the document with the custom property 'commentsCount'
        type0: function(user) {
            var sq = searchQuery;
            sq.Login = user.login;
            sq.Type = 0;

            return Statistic.find(sq).count();
        },

        type1: function(user) {
            var sq = searchQuery;
            sq.Login = user.login;
            sq.Type = 1;
            return Statistic.find(sq).count();
        }
    });
});

Meteor.publish('sberUsers', function(keyword, page){

    var usersOnPage = State.findOne().usersOnPage;

    if (this.userId) {
        if(!keyword)
            keyword = ""
        var coursor = sberUsers.find({
            dontsync: false,
            $or:[
                {
                    name: {
                        '$regex': "(" + keyword + ")",
                        '$options': 'i'
                    }
                },
                {
                    room: {
                        '$regex': "(" + keyword + ")",
                        '$options': 'i'
                    }
                }
            ]},
            {
                skip: page * usersOnPage,
                limit: usersOnPage,
                sort: {name: 1}
            });

        var usersCount = coursor.count();
        var id = State.findOne()._id;
        State.update(id, {$set: {usersCount: usersCount}});

        return coursor;
    }
});

if (Meteor. isServer){
    Meteor.publish("files", function(){
        return Files.find(); //publish all
        });

    Meteor.publish("state", function(){
            return State.find();
        });
}


