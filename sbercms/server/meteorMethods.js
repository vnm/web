/**
 * Created by Alexey on 13.04.2016.
 */

var usersPath = process.env.USERS;
var from = process.env.FROM;
var fs = Npm.require('fs');

Meteor.methods({
    "refreshFromCSV": function(){
        this.unblock();
        console.log("start load from csv");

        var id = State.findOne()._id;
        State.update(id, {$set: {refreshing: true}});

        if (this.userId) {
            var files = fs.readdirSync(usersPath);
            for (var i = 0; i < files.length; i++) {
                if (files[i].endsWith(".csv")) {
                    var usersCSVArray = Papa.parse(fs.readFileSync(usersPath + "\\" + files[i], 'utf-8')).data;
                    var usersDB = sberUsers.find().fetch();

                    for (var j = 0; j < usersDB.length; j++) {
                        if (!usersDB[j].refreshDisable) {
                            sberUsers.remove(usersDB[j]._id);
                            Files.remove(usersDB[j].fileId);
                        }
                        else{
                            if(usersDB[j].dontsync === undefined){
                                sberUsers.update(usersDB[j]._id, {$set: {dontsync: false}}); //fix after POI added
                            }
                        }
                    }

                    for (var k = 1; k < usersCSVArray.length; k++) {
                        var userRow = usersCSVArray[k];
                        if (userRow.length<10)
                            continue;

                        if(userRow[3].length===0)
                            continue;

                        var login = userRow[0];
                        var userFromDb = sberUsers.findOne({login: login});

                        if (userFromDb) {
                            if (userFromDb.refreshDisable)
                                continue;
                        }

                        var fileObj = null;
                        var filePath = usersPath + "\\" + login + ".jpg";

                        if (fs.statSync(filePath) && fs.statSync(filePath).size>0) {
                            var data = fs.readFileSync(filePath);
                            var newFile = new FS.File();
                            newFile.attachData(data, {type: 'image/jpeg'});
                            newFile.name(login + ".jpg");
                            fileObj = Files.insert(newFile);
                        }

                        var fileId = "";
                        if (fileObj && fileObj._id)
                            fileId = fileObj._id;

                        sberUsers.insert({
                            refreshDisable: false,
                            dontsync: false,
                            login: login,
                            fileId: fileId,
                            name: userRow[1] + " " + userRow[2] + " " + userRow[3],
                            description: userRow[4],
                            department: userRow[5],
                            division: userRow[6],
                            room: userRow[7],
                            intPhone: userRow[8],
                            phone: userRow[9]
                        });
                    }

                    sberUsers.remove({login: "Shop"});
                    sberUsers.insert({
                        refreshDisable: false,
                        dontsync: true,
                        login: "Shop",
                        fileId: "",
                        name: "Магазин",
                        description: "",
                        department: "",
                        division: "",
                        room: "А299",
                        intPhone: "",
                        phone: ""
                    });

                    sberUsers.remove({login: "Restaurant"});
                    sberUsers.insert({
                        refreshDisable: false,
                        dontsync: true,
                        login: "Restaurant",
                        fileId: "",
                        name: "Ресторан",
                        description: "",
                        department: "",
                        division: "",
                        room: "A352",
                        intPhone: "",
                        phone: ""
                    });

                    sberUsers.remove({login: "Conference"});
                    sberUsers.insert({
                        refreshDisable: false,
                        dontsync: true,
                        login: "Conference",
                        fileId: "",
                        name: "Конференц-зал",
                        description: "",
                        department: "",
                        division: "",
                        room: "АТ2А",
                        intPhone: "",
                        phone: ""
                    });


                    State.update(id, {$set: {refreshing: false}});

                    console.log('end parsing csv');
                    return {result: true};
                }
            }
        }
    },

    "getMainTablePagesNum": function(){
        return 1000;
    },

    "refreshClient": function(id){
        var ip = terminals.findOne(id).ip;

        d = {'command': 'refresh'};
        HTTP.post("http://"+ip+":8080/api/managment", {params: d}, function(e,r){
            console.log('refresh client : '+ ip);
        })

    },

    "refreshAll": function(){
        var _terminals = terminals.find().fetch();
        _terminals.forEach(function(terminal){
            Meteor.call('refreshClient', terminal._id);
        })
    },

    "downloadCSV":function(v){
        var terminal = v.terminal;
        var fromDate = v.fromDate;
        var toDate = v.toDate;
        var view = v.view;

        if (view === undefined || view === "journal") {
            console.log("journal");

            var searchQuery = {};

            if (terminal) {
                if (terminal.length > 7)
                    searchQuery.client = terminal;
            }

            if (!fromDate)
                fromDate = new Date().getTime() - 1000 * 3600 * 24;

            searchQuery.Timestamp = {};
            searchQuery.Timestamp.$gte = parseInt(fromDate);

            if (toDate)
                searchQuery.Timestamp.$lte = parseInt(toDate);


            var collection = Statistic.find(searchQuery).fetch();

            for (var i = 0; i < collection.length; i++) {
                var item = {};
                item.Time = moment(collection[i].Timestamp).format("DD-MM-YY HH:mm");
                item.Terminal = collection[i].client;
                item.Login = collection[i].Login;

                if (collection[i].Name)
                    item.Name = collection[i].Name;
                else
                    item.Name = "";

                if (collection[i].Type = 0)
                    item.Message = "Выбран в поиске"
                if (collection[i].Type = 1)
                    item.Message = "Построен маршрут"
                collection[i] = item;
            }

            return exportcsv.exportToCSV(collection, true, ";");
        }

        if (view === "byLogin"){
            console.log("byLogin");

            var searchQuery = {};

            if (terminal){
                if (terminal.length>7)
                    searchQuery.client = terminal;
            }

            if (!fromDate)
                fromDate = new Date().getTime()-1000*3600*24;

            searchQuery.Timestamp = {};
            searchQuery.Timestamp.$gte = parseInt(fromDate);

            if (toDate)
                searchQuery.Timestamp.$lte = parseInt(toDate);

            var a = sberUsers.find().fetch();

            for (var i=0; i< a.length; i++){
                var item = {};
                item.Login = a[i].login;
                item.Name = a[i].name;
                var sq = searchQuery;
                sq.Login = item.Login;
                sq.Type=0;
                item.Selected = Statistic.find(sq).count();
                sq.Type=1;
                item.Routed = Statistic.find(sq).count();

                a[i]= item;
            }

            function compare(a1,b1) {
                if (a1.Selected > b1.Selected)
                    return -1;
                else if (a1.Selected < b1.Selected)
                    return 1;
                else
                    return 0;
            }

            a.sort(compare);

            return exportcsv.exportToCSV(a, true, ";");
        }
    },

    "sendEmail" :function(message){
        var mail = Meteor.users.findOne({username: 'admin'}).profile.mail;
        if (mail.length>0){
            Email.send({
                to: mail,
                from: from,
                subject: 'navigation warning',
                text: message
            });
        }
    },
    'changeMail' : function(user, mail){
        if (this.userId){
            var userId = Meteor.users.findOne({username: user})._id;
            Meteor.users.update(userId, {$set: {'profile.mail': mail}});
        }

    },

    'changePwd' : function(user, pwd){
        if (this.userId){
            var userId = Meteor.users.findOne({username: user})._id;
            Accounts.setPassword(userId, pwd);
        }
    },
})